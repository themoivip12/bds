<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Alert', 'fw' ),
	'description' => __( 'Add an Alert', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);
