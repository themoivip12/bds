<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'message' => array(
		'label' => __( 'Message', 'fw' ),
		'desc'  => __( 'Notification message', 'fw' ),
		'type'  => 'text',
		'value' => __( 'Message!', 'fw' ),
	),
	'type'   => array(
		'label'   => __( 'Type', 'fw' ),
		'desc'    => __( 'Notification type', 'fw' ),
		'type'    => 'select',
		'choices' => array(
			'primary'    => __( 'Primary', 'fw' ),
			'secondary'    => __( 'Secondary', 'fw' ),
			'success' => __( 'Success', 'fw' ),
			'info'    => __( 'Info', 'fw' ),
			'warning' => __( 'Warning', 'fw' ),
			'danger'  => __( 'Danger', 'fw' ),
			'dark'    => __( 'Dark', 'fw' ),
			'light'    => __( 'Light', 'fw' ),
		)
	),
);