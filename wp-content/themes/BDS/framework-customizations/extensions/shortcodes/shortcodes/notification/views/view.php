<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>
<div class="alert alert-<?php echo esc_attr($atts['type']); ?>">
	<?php echo $atts['message']; ?>
</div>