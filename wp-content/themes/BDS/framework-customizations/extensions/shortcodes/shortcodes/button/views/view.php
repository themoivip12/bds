<?php if (!defined('FW')) die( 'Forbidden' ); ?>
<?php $style_class = !empty($atts['style']) ? " btn-{$atts['style']}" : ''; ?>
<?php $size_class = !empty($atts['size']) ? " btn-{$atts['size']}" : ''; ?>
<?php $fullwidth_class = ( isset( $atts['is_fullwidth'] ) && $atts['is_fullwidth'] ) ? ' btn-block' : ''; ?>
<?php $custom_class = !empty($atts['class']) ? " {$atts['class']}" : ''; ?>
<a href="<?php echo esc_attr($atts['link']) ?>" target="<?php echo esc_attr($atts['target']) ?>" class="btn<?php echo esc_attr($style_class); ?><?php echo esc_attr($size_class); ?><?php echo esc_attr($fullwidth_class); ?><?php echo esc_attr($custom_class); ?>"<?php echo ( isset( $atts['is_smooth_scroll'] ) && $atts['is_smooth_scroll'] ) ? ' data-toggle="smooth-scroll"' : '' ?>>
	<?php echo $atts['label']; ?>
</a>
