<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'label'  => array(
		'label' => __( 'Button Label', 'fw' ),
		'desc'  => __( 'This is the text that appears on your button', 'fw' ),
		'type'  => 'text',
		'value' => 'Submit'
	),
	'link'   => array(
		'label' => __( 'Button Link', 'fw' ),
		'desc'  => __( 'Where should your button link to', 'fw' ),
		'type'  => 'text',
		'value' => '#'
	),
	'target' => array(
		'type'  => 'switch',
		'label'   => __( 'Open Link in New Window', 'fw' ),
		'desc'    => __( 'Select here if you want to open the linked page in a new window', 'fw' ),
		'right-choice' => array(
			'value' => '_blank',
			'label' => __('Yes', 'fw'),
		),
		'left-choice' => array(
			'value' => '_self',
			'label' => __('No', 'fw'),
		),
	),
	'style'  => array(
		'label'   => __( 'Button Style', 'fw' ),
		'desc'    => __( 'Choose a style for your button', 'fw' ),
		'type'    => 'select',
		'choices' => array(
			'primary'      => __('Primary', 'fw'),
			'secondary' => __( 'Secondary', 'fw' ),
			'success'  => __( 'Success', 'fw' ),
			'danger' => __( 'Danger', 'fw' ),
			'warning'   => __( 'Warning', 'fw' ),
			'info' => __( 'Info', 'fw' ),
			'light'  => __( 'Light', 'fw' ),
			'dark' => __( 'Dark', 'fw' ),
			'white' => __( 'White', 'fw' ),
			'link'   => __( 'Link', 'fw' ),
			'outline-primary'      => __('Outline Primary', 'fw'),
			'outline-secondary' => __( 'Outline Secondary', 'fw' ),
			'outline-success'  => __( 'Outline Success', 'fw' ),
			'outline-danger' => __( 'Outline Danger', 'fw' ),
			'outline-warning'   => __( 'Outline Warning', 'fw' ),
			'outline-info' => __( 'Outline Info', 'fw' ),
			'outline-light'  => __( 'Outline Light', 'fw' ),
			'outline-dark' => __( 'Outline Dark', 'fw' ),
			'outline-white' => __( 'Outline White', 'fw' ),
			'outline-link'   => __( 'Outline Link', 'fw' ),
		)
	),
	'size'  => array(
		'label'   => __( 'Button Size', 'fw' ),
		'desc'    => __( 'Choose a size for your button', 'fw' ),
		'type'    => 'select',
		'choices' => array(
			'lg'      => __('Large', 'fw'),
			'' => __( 'Medium', 'fw' ),
			'sm'  => __( 'Small', 'fw' ),
		)
	),
	'is_fullwidth' => array(
		'label'        => __('Full Width', 'fw'),
		'type'         => 'switch',
	),
	'is_smooth_scroll' => array(
		'label'        => __('Smooth Scroll', 'fw'),
		'type'         => 'switch',
	),
	'class' => array(
		'label' => __('Custom Class', 'fw'),
		'type'  => 'text',
	),
);
