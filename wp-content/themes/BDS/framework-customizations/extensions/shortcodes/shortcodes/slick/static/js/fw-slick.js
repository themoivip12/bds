
(function ($) {
	'use strict';
	$('.fw-slick-carousel').slick();
	$('.fw-slick-nav').slick({ asNavFor: '.fw-slick-carousel' });
})(jQuery);
