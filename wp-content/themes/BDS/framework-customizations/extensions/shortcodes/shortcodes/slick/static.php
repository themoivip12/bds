<?php if (!defined('FW')) die('Forbidden');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/slick');
wp_enqueue_style(
	'fw-shortcode-slick',
	$uri . '/static/css/slick.css'
);

wp_enqueue_style(
	'fw-shortcode-fw-slick-theme',
	$uri . '/static/css/slick-theme.css'
);

wp_enqueue_style(
	'fw-shortcode-fw-slick',
	$uri . '/static/css/fw-slick.css'
);

wp_enqueue_script(
	'fw-shortcode-slick-js',
	$uri . '/static/js/slick.min.js',
	'jquery',
	'',
	true
);
wp_enqueue_script(
	'fw-shortcode-fw-slick-js',
	$uri . '/static/js/fw-slick.js',
	array( 'jquery', 'fw-shortcode-slick-js' ),
	'',
	true
);
