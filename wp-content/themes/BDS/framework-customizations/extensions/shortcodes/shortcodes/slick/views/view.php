<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
?>
<div class="<?php echo (isset( $atts['wrapper_class'] ) && $atts['wrapper_class']) ? $atts['wrapper_class'] : 'fw-slick-carousel-wrapper'; ?>">
	<div class="<?php echo (isset( $atts['row_class'] ) && $atts['row_class']) ? $atts['row_class'] : 'row justify-content-lg-between align-items-lg-center'; ?>">
		<?php if (isset( $atts['show_nav'] ) && $atts['show_nav']) : ?>
		<div class="<?php echo (isset( $atts['nav_col_class'] ) && $atts['nav_col_class']) ? $atts['nav_col_class'] : 'col-lg-5'; ?>">
			<div class="fw-slick-nav" data-slick='<?php echo (isset( $atts['nav_options'] ) && $atts['nav_options']) ? $atts['nav_options'] : '{"arrows": true, "slidesToShow": 3, "slidesToScroll": 3, "adaptiveHeight": true, "vertical": true, "verticalSwiping": true, "focusOnSelect": true}'; ?>'>
			<?php foreach ( fw_akg( 'slides', $atts, array() ) as $tab ) : ?>
				<div><?php echo do_shortcode( $tab['slide_nav_content'] ); ?></div>
			<?php endforeach; ?>
			</div>
		</div>
		<?php endif; ?>
		<div class="<?php echo (isset( $atts['carousel_col_class'] ) && $atts['carousel_col_class']) ? $atts['carousel_col_class'] : 'col-lg-6'; ?>">
			<div class="fw-slick-carousel" data-slick='<?php echo (isset( $atts['carousel_options'] ) && $atts['carousel_options']) ? $atts['carousel_options'] : '{"arrows": false, "fade": true}'; ?>'>
			<?php foreach ( fw_akg( 'slides', $atts, array() ) as $tab ) : ?>
				<div><?php echo do_shortcode( $tab['slide_content'] ); ?></div>
			<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
