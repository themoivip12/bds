<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Slick Carousel', 'fw' ),
	'description' => __( 'Add a Slick Carousel', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);
