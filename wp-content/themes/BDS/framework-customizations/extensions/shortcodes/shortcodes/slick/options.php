<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'slides' => array(
		'type'          => 'addable-popup',
		'label'         => __( 'Slides', 'fw' ),
		'popup-title'   => __( 'Add/Edit Slide', 'fw' ),
		'desc'          => __( 'Create your slides', 'fw' ),
		'template'      => '{{=slide_title}}',
		'popup-options' => array(
			'slide_title'   => array(
				'type'  => 'text',
				'label' => __('Title', 'fw')
			),
			'slide_nav_content' => array(
				'type'  => 'textarea',
				'label' => __('Nav Content', 'fw')
			),
			'slide_content' => array(
				'type'  => 'textarea',
				'label' => __('Content', 'fw')
			),
		)
	),
	'wrapper_class' => array(
		'type'    => 'text',
		'label'   => __('Wrapper Class', 'fw'),
		'value'   => 'fw-slick-carousel-wrapper'
	),
	'row_class' => array(
		'type'    => 'text',
		'label'   => __('Row Class', 'fw'),
		'value'   => 'row justify-content-lg-between align-items-lg-center'
	),
	'carousel_col_class' => array(
		'type'    => 'text',
		'label'   => __('Carousel Col Class', 'fw'),
		'value'   => 'col-lg-6'
	),
	'carousel_options' => array(
		'type'    => 'text',
		'label'   => __('Carousel Options', 'fw'),
		'value'   => '{"arrows": false, "fade": true}'
	),
	'show_nav' => array(
		'type'    => 'switch',
		'value'   => true,
		'label'   => __('Show Nav?', 'fw'),
	),
	'nav_col_class' => array(
		'type'    => 'text',
		'label'   => __('Nav Col Class', 'fw'),
		'value'   => 'col-lg-5'
	),
	'nav_options' => array(
		'type'    => 'text',
		'label'   => __('Nav Options', 'fw'),
		'value'   => '{"arrows": true, "slidesToShow": 3, "slidesToScroll": 3, "adaptiveHeight": true, "vertical": true, "verticalSwiping": true, "focusOnSelect": true}'
	),
);
