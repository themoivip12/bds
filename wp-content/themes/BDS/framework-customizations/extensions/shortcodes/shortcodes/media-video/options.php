<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'url'    => array(
		'type'  => 'text',
		'label' => __( 'Insert Video URL', 'fw' ),
		'desc'  => __( 'Insert Video URL to embed this video', 'fw' )
	),
	'width'  => array(
		'type'  => 'text',
		'label' => __( 'Video Width', 'fw' ),
		'desc'  => __( 'Enter a value for the width', 'fw' ),
		'value' => 1920
	),
	'height' => array(
		'type'  => 'text',
		'label' => __( 'Video Height', 'fw' ),
		'desc'  => __( 'Enter a value for the height', 'fw' ),
		'value' => 1080
	),
	'class'    => array(
		'type'  => 'text',
		'label' => __( 'Custom Class', 'fw' ),
		'value' => 'embed-responsive embed-responsive-16by9 rounded shadow mb-6'
	),
);
