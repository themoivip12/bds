<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
?>
<?php $heading = "<{$atts['heading']} class='{$atts['class']}'>{$atts['title']}</{$atts['heading']}>"; ?>
<?php echo $heading; ?>
