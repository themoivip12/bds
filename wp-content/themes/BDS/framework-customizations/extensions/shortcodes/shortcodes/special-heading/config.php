<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Heading', 'fw'),
	'description'   => __('Add a Heading', 'fw'),
	'tab'           => __('Content Elements', 'fw'),
);
