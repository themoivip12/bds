<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */
$pack = $atts['icon']['pack'];
$icon = $atts['icon'][$pack]['icon'];
$image = fw_get_template_customizations_directory('/extensions/shortcodes/shortcodes/icon/static/img/icons/') . $pack . '/' . $icon . '.svg';
?>
<span class="icon<?php echo (!empty($atts['size'])) ? ' ' . $atts['size'] : ''; ?><?php echo (!empty($atts['color'])) ? ' ' . $atts['color'] : ''; ?><?php echo (!empty($atts['class'])) ? ' ' . $atts['class'] : ''; ?>">
	<?php echo file_get_contents( $image ); ?>
</span>
