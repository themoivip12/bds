<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/icon/static/img/icons/');
$options = array(
	'icon'       => array(
		'type' => 'multi-picker',
		'picker' => array(
			'pack' => array(
				'label'   => __('Pack', 'fw'),
				'type'    => 'select', // or 'short-select'
				'choices' => array(
					'feather'  => __('Feather', 'fw'),
					'duotone-icons' => __('Duotone Icons', 'fw'),
					'pe-icon-7-stroke' => __('Pe Icon 7 Stroke', 'fw'),
				),
			),
		),
		'choices' => array(
			'feather' => array(
				'icon' => array(
					'type'  => 'image-picker',
					'label' => __('Icon', 'fw'),
					'choices' => array(
						'activity' => array(
							'small' => array(
								'src' => $uri . 'feather/activity.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/activity.svg',
								'height' => 128
							)
						),
						'airplay' => array(
							'small' => array(
								'src' => $uri . 'feather/airplay.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/airplay.svg',
								'height' => 128
							)
						),
						'alert-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/alert-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/alert-circle.svg',
								'height' => 128
							)
						),
						'alert-octagon' => array(
							'small' => array(
								'src' => $uri . 'feather/alert-octagon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/alert-octagon.svg',
								'height' => 128
							)
						),
						'alert-triangle' => array(
							'small' => array(
								'src' => $uri . 'feather/alert-triangle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/alert-triangle.svg',
								'height' => 128
							)
						),
						'align-center' => array(
							'small' => array(
								'src' => $uri . 'feather/align-center.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/align-center.svg',
								'height' => 128
							)
						),
						'align-justify' => array(
							'small' => array(
								'src' => $uri . 'feather/align-justify.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/align-justify.svg',
								'height' => 128
							)
						),
						'align-left' => array(
							'small' => array(
								'src' => $uri . 'feather/align-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/align-left.svg',
								'height' => 128
							)
						),
						'align-right' => array(
							'small' => array(
								'src' => $uri . 'feather/align-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/align-right.svg',
								'height' => 128
							)
						),
						'anchor' => array(
							'small' => array(
								'src' => $uri . 'feather/anchor.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/anchor.svg',
								'height' => 128
							)
						),
						'aperture' => array(
							'small' => array(
								'src' => $uri . 'feather/aperture.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/aperture.svg',
								'height' => 128
							)
						),
						'archive' => array(
							'small' => array(
								'src' => $uri . 'feather/archive.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/archive.svg',
								'height' => 128
							)
						),
						'arrow-down-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-down-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-down-circle.svg',
								'height' => 128
							)
						),
						'arrow-down-left' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-down-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-down-left.svg',
								'height' => 128
							)
						),
						'arrow-down-right' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-down-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-down-right.svg',
								'height' => 128
							)
						),
						'arrow-down' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-down.svg',
								'height' => 128
							)
						),
						'arrow-left-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-left-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-left-circle.svg',
								'height' => 128
							)
						),
						'arrow-left' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-left.svg',
								'height' => 128
							)
						),
						'arrow-right-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-right-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-right-circle.svg',
								'height' => 128
							)
						),
						'arrow-right' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-right.svg',
								'height' => 128
							)
						),
						'arrow-up-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-up-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-up-circle.svg',
								'height' => 128
							)
						),
						'arrow-up-left' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-up-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-up-left.svg',
								'height' => 128
							)
						),
						'arrow-up-right' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-up-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-up-right.svg',
								'height' => 128
							)
						),
						'arrow-up' => array(
							'small' => array(
								'src' => $uri . 'feather/arrow-up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/arrow-up.svg',
								'height' => 128
							)
						),
						'at-sign' => array(
							'small' => array(
								'src' => $uri . 'feather/at-sign.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/at-sign.svg',
								'height' => 128
							)
						),
						'award' => array(
							'small' => array(
								'src' => $uri . 'feather/award.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/award.svg',
								'height' => 128
							)
						),
						'bar-chart-2' => array(
							'small' => array(
								'src' => $uri . 'feather/bar-chart-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/bar-chart-2.svg',
								'height' => 128
							)
						),
						'bar-chart' => array(
							'small' => array(
								'src' => $uri . 'feather/bar-chart.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/bar-chart.svg',
								'height' => 128
							)
						),
						'battery-charging' => array(
							'small' => array(
								'src' => $uri . 'feather/battery-charging.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/battery-charging.svg',
								'height' => 128
							)
						),
						'battery' => array(
							'small' => array(
								'src' => $uri . 'feather/battery.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/battery.svg',
								'height' => 128
							)
						),
						'bell-off' => array(
							'small' => array(
								'src' => $uri . 'feather/bell-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/bell-off.svg',
								'height' => 128
							)
						),
						'bell' => array(
							'small' => array(
								'src' => $uri . 'feather/bell.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/bell.svg',
								'height' => 128
							)
						),
						'bluetooth' => array(
							'small' => array(
								'src' => $uri . 'feather/bluetooth.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/bluetooth.svg',
								'height' => 128
							)
						),
						'bold' => array(
							'small' => array(
								'src' => $uri . 'feather/bold.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/bold.svg',
								'height' => 128
							)
						),
						'book-open' => array(
							'small' => array(
								'src' => $uri . 'feather/book-open.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/book-open.svg',
								'height' => 128
							)
						),
						'book' => array(
							'small' => array(
								'src' => $uri . 'feather/book.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/book.svg',
								'height' => 128
							)
						),
						'bookmark' => array(
							'small' => array(
								'src' => $uri . 'feather/bookmark.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/bookmark.svg',
								'height' => 128
							)
						),
						'box' => array(
							'small' => array(
								'src' => $uri . 'feather/box.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/box.svg',
								'height' => 128
							)
						),
						'briefcase' => array(
							'small' => array(
								'src' => $uri . 'feather/briefcase.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/briefcase.svg',
								'height' => 128
							)
						),
						'calendar' => array(
							'small' => array(
								'src' => $uri . 'feather/calendar.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/calendar.svg',
								'height' => 128
							)
						),
						'camera-off' => array(
							'small' => array(
								'src' => $uri . 'feather/camera-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/camera-off.svg',
								'height' => 128
							)
						),
						'camera' => array(
							'small' => array(
								'src' => $uri . 'feather/camera.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/camera.svg',
								'height' => 128
							)
						),
						'cast' => array(
							'small' => array(
								'src' => $uri . 'feather/cast.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/cast.svg',
								'height' => 128
							)
						),
						'check-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/check-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/check-circle.svg',
								'height' => 128
							)
						),
						'check-square' => array(
							'small' => array(
								'src' => $uri . 'feather/check-square.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/check-square.svg',
								'height' => 128
							)
						),
						'check' => array(
							'small' => array(
								'src' => $uri . 'feather/check.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/check.svg',
								'height' => 128
							)
						),
						'chevron-down' => array(
							'small' => array(
								'src' => $uri . 'feather/chevron-down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chevron-down.svg',
								'height' => 128
							)
						),
						'chevron-left' => array(
							'small' => array(
								'src' => $uri . 'feather/chevron-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chevron-left.svg',
								'height' => 128
							)
						),
						'chevron-right' => array(
							'small' => array(
								'src' => $uri . 'feather/chevron-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chevron-right.svg',
								'height' => 128
							)
						),
						'chevron-up' => array(
							'small' => array(
								'src' => $uri . 'feather/chevron-up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chevron-up.svg',
								'height' => 128
							)
						),
						'chevrons-down' => array(
							'small' => array(
								'src' => $uri . 'feather/chevrons-down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chevrons-down.svg',
								'height' => 128
							)
						),
						'chevrons-left' => array(
							'small' => array(
								'src' => $uri . 'feather/chevrons-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chevrons-left.svg',
								'height' => 128
							)
						),
						'chevrons-right' => array(
							'small' => array(
								'src' => $uri . 'feather/chevrons-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chevrons-right.svg',
								'height' => 128
							)
						),
						'chevrons-up' => array(
							'small' => array(
								'src' => $uri . 'feather/chevrons-up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chevrons-up.svg',
								'height' => 128
							)
						),
						'chrome' => array(
							'small' => array(
								'src' => $uri . 'feather/chrome.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/chrome.svg',
								'height' => 128
							)
						),
						'circle' => array(
							'small' => array(
								'src' => $uri . 'feather/circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/circle.svg',
								'height' => 128
							)
						),
						'clipboard' => array(
							'small' => array(
								'src' => $uri . 'feather/clipboard.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/clipboard.svg',
								'height' => 128
							)
						),
						'clock' => array(
							'small' => array(
								'src' => $uri . 'feather/clock.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/clock.svg',
								'height' => 128
							)
						),
						'cloud-drizzle' => array(
							'small' => array(
								'src' => $uri . 'feather/cloud-drizzle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/cloud-drizzle.svg',
								'height' => 128
							)
						),
						'cloud-lightning' => array(
							'small' => array(
								'src' => $uri . 'feather/cloud-lightning.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/cloud-lightning.svg',
								'height' => 128
							)
						),
						'cloud-off' => array(
							'small' => array(
								'src' => $uri . 'feather/cloud-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/cloud-off.svg',
								'height' => 128
							)
						),
						'cloud-rain' => array(
							'small' => array(
								'src' => $uri . 'feather/cloud-rain.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/cloud-rain.svg',
								'height' => 128
							)
						),
						'cloud-snow' => array(
							'small' => array(
								'src' => $uri . 'feather/cloud-snow.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/cloud-snow.svg',
								'height' => 128
							)
						),
						'cloud' => array(
							'small' => array(
								'src' => $uri . 'feather/cloud.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/cloud.svg',
								'height' => 128
							)
						),
						'code' => array(
							'small' => array(
								'src' => $uri . 'feather/code.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/code.svg',
								'height' => 128
							)
						),
						'codepen' => array(
							'small' => array(
								'src' => $uri . 'feather/codepen.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/codepen.svg',
								'height' => 128
							)
						),
						'codesandbox' => array(
							'small' => array(
								'src' => $uri . 'feather/codesandbox.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/codesandbox.svg',
								'height' => 128
							)
						),
						'coffee' => array(
							'small' => array(
								'src' => $uri . 'feather/coffee.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/coffee.svg',
								'height' => 128
							)
						),
						'columns' => array(
							'small' => array(
								'src' => $uri . 'feather/columns.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/columns.svg',
								'height' => 128
							)
						),
						'command' => array(
							'small' => array(
								'src' => $uri . 'feather/command.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/command.svg',
								'height' => 128
							)
						),
						'compass' => array(
							'small' => array(
								'src' => $uri . 'feather/compass.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/compass.svg',
								'height' => 128
							)
						),
						'copy' => array(
							'small' => array(
								'src' => $uri . 'feather/copy.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/copy.svg',
								'height' => 128
							)
						),
						'corner-down-left' => array(
							'small' => array(
								'src' => $uri . 'feather/corner-down-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/corner-down-left.svg',
								'height' => 128
							)
						),
						'corner-down-right' => array(
							'small' => array(
								'src' => $uri . 'feather/corner-down-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/corner-down-right.svg',
								'height' => 128
							)
						),
						'corner-left-down' => array(
							'small' => array(
								'src' => $uri . 'feather/corner-left-down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/corner-left-down.svg',
								'height' => 128
							)
						),
						'corner-left-up' => array(
							'small' => array(
								'src' => $uri . 'feather/corner-left-up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/corner-left-up.svg',
								'height' => 128
							)
						),
						'corner-right-down' => array(
							'small' => array(
								'src' => $uri . 'feather/corner-right-down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/corner-right-down.svg',
								'height' => 128
							)
						),
						'corner-right-up' => array(
							'small' => array(
								'src' => $uri . 'feather/corner-right-up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/corner-right-up.svg',
								'height' => 128
							)
						),
						'corner-up-left' => array(
							'small' => array(
								'src' => $uri . 'feather/corner-up-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/corner-up-left.svg',
								'height' => 128
							)
						),
						'corner-up-right' => array(
							'small' => array(
								'src' => $uri . 'feather/corner-up-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/corner-up-right.svg',
								'height' => 128
							)
						),
						'cpu' => array(
							'small' => array(
								'src' => $uri . 'feather/cpu.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/cpu.svg',
								'height' => 128
							)
						),
						'credit-card' => array(
							'small' => array(
								'src' => $uri . 'feather/credit-card.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/credit-card.svg',
								'height' => 128
							)
						),
						'crop' => array(
							'small' => array(
								'src' => $uri . 'feather/crop.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/crop.svg',
								'height' => 128
							)
						),
						'crosshair' => array(
							'small' => array(
								'src' => $uri . 'feather/crosshair.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/crosshair.svg',
								'height' => 128
							)
						),
						'database' => array(
							'small' => array(
								'src' => $uri . 'feather/database.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/database.svg',
								'height' => 128
							)
						),
						'delete' => array(
							'small' => array(
								'src' => $uri . 'feather/delete.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/delete.svg',
								'height' => 128
							)
						),
						'disc' => array(
							'small' => array(
								'src' => $uri . 'feather/disc.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/disc.svg',
								'height' => 128
							)
						),
						'dollar-sign' => array(
							'small' => array(
								'src' => $uri . 'feather/dollar-sign.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/dollar-sign.svg',
								'height' => 128
							)
						),
						'download-cloud' => array(
							'small' => array(
								'src' => $uri . 'feather/download-cloud.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/download-cloud.svg',
								'height' => 128
							)
						),
						'download' => array(
							'small' => array(
								'src' => $uri . 'feather/download.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/download.svg',
								'height' => 128
							)
						),
						'droplet' => array(
							'small' => array(
								'src' => $uri . 'feather/droplet.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/droplet.svg',
								'height' => 128
							)
						),
						'edit-2' => array(
							'small' => array(
								'src' => $uri . 'feather/edit-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/edit-2.svg',
								'height' => 128
							)
						),
						'edit-3' => array(
							'small' => array(
								'src' => $uri . 'feather/edit-3.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/edit-3.svg',
								'height' => 128
							)
						),
						'edit' => array(
							'small' => array(
								'src' => $uri . 'feather/edit.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/edit.svg',
								'height' => 128
							)
						),
						'external-link' => array(
							'small' => array(
								'src' => $uri . 'feather/external-link.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/external-link.svg',
								'height' => 128
							)
						),
						'eye-off' => array(
							'small' => array(
								'src' => $uri . 'feather/eye-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/eye-off.svg',
								'height' => 128
							)
						),
						'eye' => array(
							'small' => array(
								'src' => $uri . 'feather/eye.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/eye.svg',
								'height' => 128
							)
						),
						'facebook' => array(
							'small' => array(
								'src' => $uri . 'feather/facebook.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/facebook.svg',
								'height' => 128
							)
						),
						'fast-forward' => array(
							'small' => array(
								'src' => $uri . 'feather/fast-forward.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/fast-forward.svg',
								'height' => 128
							)
						),
						'feather' => array(
							'small' => array(
								'src' => $uri . 'feather/feather.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/feather.svg',
								'height' => 128
							)
						),
						'figma' => array(
							'small' => array(
								'src' => $uri . 'feather/figma.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/figma.svg',
								'height' => 128
							)
						),
						'file-minus' => array(
							'small' => array(
								'src' => $uri . 'feather/file-minus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/file-minus.svg',
								'height' => 128
							)
						),
						'file-plus' => array(
							'small' => array(
								'src' => $uri . 'feather/file-plus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/file-plus.svg',
								'height' => 128
							)
						),
						'file-text' => array(
							'small' => array(
								'src' => $uri . 'feather/file-text.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/file-text.svg',
								'height' => 128
							)
						),
						'file' => array(
							'small' => array(
								'src' => $uri . 'feather/file.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/file.svg',
								'height' => 128
							)
						),
						'film' => array(
							'small' => array(
								'src' => $uri . 'feather/film.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/film.svg',
								'height' => 128
							)
						),
						'filter' => array(
							'small' => array(
								'src' => $uri . 'feather/filter.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/filter.svg',
								'height' => 128
							)
						),
						'flag' => array(
							'small' => array(
								'src' => $uri . 'feather/flag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/flag.svg',
								'height' => 128
							)
						),
						'folder-minus' => array(
							'small' => array(
								'src' => $uri . 'feather/folder-minus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/folder-minus.svg',
								'height' => 128
							)
						),
						'folder-plus' => array(
							'small' => array(
								'src' => $uri . 'feather/folder-plus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/folder-plus.svg',
								'height' => 128
							)
						),
						'folder' => array(
							'small' => array(
								'src' => $uri . 'feather/folder.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/folder.svg',
								'height' => 128
							)
						),
						'framer' => array(
							'small' => array(
								'src' => $uri . 'feather/framer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/framer.svg',
								'height' => 128
							)
						),
						'frown' => array(
							'small' => array(
								'src' => $uri . 'feather/frown.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/frown.svg',
								'height' => 128
							)
						),
						'gift' => array(
							'small' => array(
								'src' => $uri . 'feather/gift.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/gift.svg',
								'height' => 128
							)
						),
						'git-branch' => array(
							'small' => array(
								'src' => $uri . 'feather/git-branch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/git-branch.svg',
								'height' => 128
							)
						),
						'git-commit' => array(
							'small' => array(
								'src' => $uri . 'feather/git-commit.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/git-commit.svg',
								'height' => 128
							)
						),
						'git-merge' => array(
							'small' => array(
								'src' => $uri . 'feather/git-merge.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/git-merge.svg',
								'height' => 128
							)
						),
						'git-pull-request' => array(
							'small' => array(
								'src' => $uri . 'feather/git-pull-request.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/git-pull-request.svg',
								'height' => 128
							)
						),
						'github' => array(
							'small' => array(
								'src' => $uri . 'feather/github.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/github.svg',
								'height' => 128
							)
						),
						'gitlab' => array(
							'small' => array(
								'src' => $uri . 'feather/gitlab.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/gitlab.svg',
								'height' => 128
							)
						),
						'globe' => array(
							'small' => array(
								'src' => $uri . 'feather/globe.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/globe.svg',
								'height' => 128
							)
						),
						'grid' => array(
							'small' => array(
								'src' => $uri . 'feather/grid.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/grid.svg',
								'height' => 128
							)
						),
						'hard-drive' => array(
							'small' => array(
								'src' => $uri . 'feather/hard-drive.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/hard-drive.svg',
								'height' => 128
							)
						),
						'hash' => array(
							'small' => array(
								'src' => $uri . 'feather/hash.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/hash.svg',
								'height' => 128
							)
						),
						'headphones' => array(
							'small' => array(
								'src' => $uri . 'feather/headphones.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/headphones.svg',
								'height' => 128
							)
						),
						'heart' => array(
							'small' => array(
								'src' => $uri . 'feather/heart.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/heart.svg',
								'height' => 128
							)
						),
						'help-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/help-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/help-circle.svg',
								'height' => 128
							)
						),
						'hexagon' => array(
							'small' => array(
								'src' => $uri . 'feather/hexagon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/hexagon.svg',
								'height' => 128
							)
						),
						'home' => array(
							'small' => array(
								'src' => $uri . 'feather/home.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/home.svg',
								'height' => 128
							)
						),
						'image' => array(
							'small' => array(
								'src' => $uri . 'feather/image.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/image.svg',
								'height' => 128
							)
						),
						'inbox' => array(
							'small' => array(
								'src' => $uri . 'feather/inbox.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/inbox.svg',
								'height' => 128
							)
						),
						'info' => array(
							'small' => array(
								'src' => $uri . 'feather/info.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/info.svg',
								'height' => 128
							)
						),
						'instagram' => array(
							'small' => array(
								'src' => $uri . 'feather/instagram.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/instagram.svg',
								'height' => 128
							)
						),
						'italic' => array(
							'small' => array(
								'src' => $uri . 'feather/italic.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/italic.svg',
								'height' => 128
							)
						),
						'key' => array(
							'small' => array(
								'src' => $uri . 'feather/key.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/key.svg',
								'height' => 128
							)
						),
						'layers' => array(
							'small' => array(
								'src' => $uri . 'feather/layers.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/layers.svg',
								'height' => 128
							)
						),
						'layout' => array(
							'small' => array(
								'src' => $uri . 'feather/layout.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/layout.svg',
								'height' => 128
							)
						),
						'life-buoy' => array(
							'small' => array(
								'src' => $uri . 'feather/life-buoy.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/life-buoy.svg',
								'height' => 128
							)
						),
						'link-2' => array(
							'small' => array(
								'src' => $uri . 'feather/link-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/link-2.svg',
								'height' => 128
							)
						),
						'link' => array(
							'small' => array(
								'src' => $uri . 'feather/link.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/link.svg',
								'height' => 128
							)
						),
						'linkedin' => array(
							'small' => array(
								'src' => $uri . 'feather/linkedin.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/linkedin.svg',
								'height' => 128
							)
						),
						'list' => array(
							'small' => array(
								'src' => $uri . 'feather/list.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/list.svg',
								'height' => 128
							)
						),
						'loader' => array(
							'small' => array(
								'src' => $uri . 'feather/loader.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/loader.svg',
								'height' => 128
							)
						),
						'lock' => array(
							'small' => array(
								'src' => $uri . 'feather/lock.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/lock.svg',
								'height' => 128
							)
						),
						'log-in' => array(
							'small' => array(
								'src' => $uri . 'feather/log-in.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/log-in.svg',
								'height' => 128
							)
						),
						'log-out' => array(
							'small' => array(
								'src' => $uri . 'feather/log-out.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/log-out.svg',
								'height' => 128
							)
						),
						'mail' => array(
							'small' => array(
								'src' => $uri . 'feather/mail.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/mail.svg',
								'height' => 128
							)
						),
						'map-pin' => array(
							'small' => array(
								'src' => $uri . 'feather/map-pin.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/map-pin.svg',
								'height' => 128
							)
						),
						'map' => array(
							'small' => array(
								'src' => $uri . 'feather/map.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/map.svg',
								'height' => 128
							)
						),
						'maximize-2' => array(
							'small' => array(
								'src' => $uri . 'feather/maximize-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/maximize-2.svg',
								'height' => 128
							)
						),
						'maximize' => array(
							'small' => array(
								'src' => $uri . 'feather/maximize.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/maximize.svg',
								'height' => 128
							)
						),
						'meh' => array(
							'small' => array(
								'src' => $uri . 'feather/meh.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/meh.svg',
								'height' => 128
							)
						),
						'menu' => array(
							'small' => array(
								'src' => $uri . 'feather/menu.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/menu.svg',
								'height' => 128
							)
						),
						'message-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/message-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/message-circle.svg',
								'height' => 128
							)
						),
						'message-square' => array(
							'small' => array(
								'src' => $uri . 'feather/message-square.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/message-square.svg',
								'height' => 128
							)
						),
						'mic-off' => array(
							'small' => array(
								'src' => $uri . 'feather/mic-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/mic-off.svg',
								'height' => 128
							)
						),
						'mic' => array(
							'small' => array(
								'src' => $uri . 'feather/mic.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/mic.svg',
								'height' => 128
							)
						),
						'minimize-2' => array(
							'small' => array(
								'src' => $uri . 'feather/minimize-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/minimize-2.svg',
								'height' => 128
							)
						),
						'minimize' => array(
							'small' => array(
								'src' => $uri . 'feather/minimize.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/minimize.svg',
								'height' => 128
							)
						),
						'minus-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/minus-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/minus-circle.svg',
								'height' => 128
							)
						),
						'minus-square' => array(
							'small' => array(
								'src' => $uri . 'feather/minus-square.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/minus-square.svg',
								'height' => 128
							)
						),
						'minus' => array(
							'small' => array(
								'src' => $uri . 'feather/minus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/minus.svg',
								'height' => 128
							)
						),
						'monitor' => array(
							'small' => array(
								'src' => $uri . 'feather/monitor.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/monitor.svg',
								'height' => 128
							)
						),
						'moon' => array(
							'small' => array(
								'src' => $uri . 'feather/moon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/moon.svg',
								'height' => 128
							)
						),
						'more-horizontal' => array(
							'small' => array(
								'src' => $uri . 'feather/more-horizontal.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/more-horizontal.svg',
								'height' => 128
							)
						),
						'more-vertical' => array(
							'small' => array(
								'src' => $uri . 'feather/more-vertical.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/more-vertical.svg',
								'height' => 128
							)
						),
						'mouse-pointer' => array(
							'small' => array(
								'src' => $uri . 'feather/mouse-pointer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/mouse-pointer.svg',
								'height' => 128
							)
						),
						'move' => array(
							'small' => array(
								'src' => $uri . 'feather/move.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/move.svg',
								'height' => 128
							)
						),
						'music' => array(
							'small' => array(
								'src' => $uri . 'feather/music.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/music.svg',
								'height' => 128
							)
						),
						'navigation-2' => array(
							'small' => array(
								'src' => $uri . 'feather/navigation-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/navigation-2.svg',
								'height' => 128
							)
						),
						'navigation' => array(
							'small' => array(
								'src' => $uri . 'feather/navigation.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/navigation.svg',
								'height' => 128
							)
						),
						'octagon' => array(
							'small' => array(
								'src' => $uri . 'feather/octagon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/octagon.svg',
								'height' => 128
							)
						),
						'package' => array(
							'small' => array(
								'src' => $uri . 'feather/package.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/package.svg',
								'height' => 128
							)
						),
						'paperclip' => array(
							'small' => array(
								'src' => $uri . 'feather/paperclip.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/paperclip.svg',
								'height' => 128
							)
						),
						'pause-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/pause-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/pause-circle.svg',
								'height' => 128
							)
						),
						'pause' => array(
							'small' => array(
								'src' => $uri . 'feather/pause.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/pause.svg',
								'height' => 128
							)
						),
						'pen-tool' => array(
							'small' => array(
								'src' => $uri . 'feather/pen-tool.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/pen-tool.svg',
								'height' => 128
							)
						),
						'percent' => array(
							'small' => array(
								'src' => $uri . 'feather/percent.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/percent.svg',
								'height' => 128
							)
						),
						'phone-call' => array(
							'small' => array(
								'src' => $uri . 'feather/phone-call.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/phone-call.svg',
								'height' => 128
							)
						),
						'phone-forwarded' => array(
							'small' => array(
								'src' => $uri . 'feather/phone-forwarded.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/phone-forwarded.svg',
								'height' => 128
							)
						),
						'phone-incoming' => array(
							'small' => array(
								'src' => $uri . 'feather/phone-incoming.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/phone-incoming.svg',
								'height' => 128
							)
						),
						'phone-missed' => array(
							'small' => array(
								'src' => $uri . 'feather/phone-missed.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/phone-missed.svg',
								'height' => 128
							)
						),
						'phone-off' => array(
							'small' => array(
								'src' => $uri . 'feather/phone-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/phone-off.svg',
								'height' => 128
							)
						),
						'phone-outgoing' => array(
							'small' => array(
								'src' => $uri . 'feather/phone-outgoing.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/phone-outgoing.svg',
								'height' => 128
							)
						),
						'phone' => array(
							'small' => array(
								'src' => $uri . 'feather/phone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/phone.svg',
								'height' => 128
							)
						),
						'pie-chart' => array(
							'small' => array(
								'src' => $uri . 'feather/pie-chart.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/pie-chart.svg',
								'height' => 128
							)
						),
						'play-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/play-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/play-circle.svg',
								'height' => 128
							)
						),
						'play' => array(
							'small' => array(
								'src' => $uri . 'feather/play.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/play.svg',
								'height' => 128
							)
						),
						'plus-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/plus-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/plus-circle.svg',
								'height' => 128
							)
						),
						'plus-square' => array(
							'small' => array(
								'src' => $uri . 'feather/plus-square.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/plus-square.svg',
								'height' => 128
							)
						),
						'plus' => array(
							'small' => array(
								'src' => $uri . 'feather/plus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/plus.svg',
								'height' => 128
							)
						),
						'pocket' => array(
							'small' => array(
								'src' => $uri . 'feather/pocket.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/pocket.svg',
								'height' => 128
							)
						),
						'power' => array(
							'small' => array(
								'src' => $uri . 'feather/power.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/power.svg',
								'height' => 128
							)
						),
						'printer' => array(
							'small' => array(
								'src' => $uri . 'feather/printer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/printer.svg',
								'height' => 128
							)
						),
						'radio' => array(
							'small' => array(
								'src' => $uri . 'feather/radio.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/radio.svg',
								'height' => 128
							)
						),
						'refresh-ccw' => array(
							'small' => array(
								'src' => $uri . 'feather/refresh-ccw.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/refresh-ccw.svg',
								'height' => 128
							)
						),
						'refresh-cw' => array(
							'small' => array(
								'src' => $uri . 'feather/refresh-cw.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/refresh-cw.svg',
								'height' => 128
							)
						),
						'repeat' => array(
							'small' => array(
								'src' => $uri . 'feather/repeat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/repeat.svg',
								'height' => 128
							)
						),
						'rewind' => array(
							'small' => array(
								'src' => $uri . 'feather/rewind.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/rewind.svg',
								'height' => 128
							)
						),
						'rotate-ccw' => array(
							'small' => array(
								'src' => $uri . 'feather/rotate-ccw.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/rotate-ccw.svg',
								'height' => 128
							)
						),
						'rotate-cw' => array(
							'small' => array(
								'src' => $uri . 'feather/rotate-cw.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/rotate-cw.svg',
								'height' => 128
							)
						),
						'rss' => array(
							'small' => array(
								'src' => $uri . 'feather/rss.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/rss.svg',
								'height' => 128
							)
						),
						'save' => array(
							'small' => array(
								'src' => $uri . 'feather/save.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/save.svg',
								'height' => 128
							)
						),
						'scissors' => array(
							'small' => array(
								'src' => $uri . 'feather/scissors.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/scissors.svg',
								'height' => 128
							)
						),
						'search' => array(
							'small' => array(
								'src' => $uri . 'feather/search.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/search.svg',
								'height' => 128
							)
						),
						'send' => array(
							'small' => array(
								'src' => $uri . 'feather/send.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/send.svg',
								'height' => 128
							)
						),
						'server' => array(
							'small' => array(
								'src' => $uri . 'feather/server.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/server.svg',
								'height' => 128
							)
						),
						'settings' => array(
							'small' => array(
								'src' => $uri . 'feather/settings.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/settings.svg',
								'height' => 128
							)
						),
						'share-2' => array(
							'small' => array(
								'src' => $uri . 'feather/share-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/share-2.svg',
								'height' => 128
							)
						),
						'share' => array(
							'small' => array(
								'src' => $uri . 'feather/share.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/share.svg',
								'height' => 128
							)
						),
						'shield-off' => array(
							'small' => array(
								'src' => $uri . 'feather/shield-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/shield-off.svg',
								'height' => 128
							)
						),
						'shield' => array(
							'small' => array(
								'src' => $uri . 'feather/shield.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/shield.svg',
								'height' => 128
							)
						),
						'shopping-bag' => array(
							'small' => array(
								'src' => $uri . 'feather/shopping-bag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/shopping-bag.svg',
								'height' => 128
							)
						),
						'shopping-cart' => array(
							'small' => array(
								'src' => $uri . 'feather/shopping-cart.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/shopping-cart.svg',
								'height' => 128
							)
						),
						'shuffle' => array(
							'small' => array(
								'src' => $uri . 'feather/shuffle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/shuffle.svg',
								'height' => 128
							)
						),
						'sidebar' => array(
							'small' => array(
								'src' => $uri . 'feather/sidebar.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/sidebar.svg',
								'height' => 128
							)
						),
						'skip-back' => array(
							'small' => array(
								'src' => $uri . 'feather/skip-back.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/skip-back.svg',
								'height' => 128
							)
						),
						'skip-forward' => array(
							'small' => array(
								'src' => $uri . 'feather/skip-forward.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/skip-forward.svg',
								'height' => 128
							)
						),
						'slack' => array(
							'small' => array(
								'src' => $uri . 'feather/slack.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/slack.svg',
								'height' => 128
							)
						),
						'slash' => array(
							'small' => array(
								'src' => $uri . 'feather/slash.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/slash.svg',
								'height' => 128
							)
						),
						'sliders' => array(
							'small' => array(
								'src' => $uri . 'feather/sliders.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/sliders.svg',
								'height' => 128
							)
						),
						'smartphone' => array(
							'small' => array(
								'src' => $uri . 'feather/smartphone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/smartphone.svg',
								'height' => 128
							)
						),
						'smile' => array(
							'small' => array(
								'src' => $uri . 'feather/smile.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/smile.svg',
								'height' => 128
							)
						),
						'speaker' => array(
							'small' => array(
								'src' => $uri . 'feather/speaker.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/speaker.svg',
								'height' => 128
							)
						),
						'square' => array(
							'small' => array(
								'src' => $uri . 'feather/square.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/square.svg',
								'height' => 128
							)
						),
						'star' => array(
							'small' => array(
								'src' => $uri . 'feather/star.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/star.svg',
								'height' => 128
							)
						),
						'stop-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/stop-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/stop-circle.svg',
								'height' => 128
							)
						),
						'sun' => array(
							'small' => array(
								'src' => $uri . 'feather/sun.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/sun.svg',
								'height' => 128
							)
						),
						'sunrise' => array(
							'small' => array(
								'src' => $uri . 'feather/sunrise.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/sunrise.svg',
								'height' => 128
							)
						),
						'sunset' => array(
							'small' => array(
								'src' => $uri . 'feather/sunset.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/sunset.svg',
								'height' => 128
							)
						),
						'tablet' => array(
							'small' => array(
								'src' => $uri . 'feather/tablet.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/tablet.svg',
								'height' => 128
							)
						),
						'tag' => array(
							'small' => array(
								'src' => $uri . 'feather/tag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/tag.svg',
								'height' => 128
							)
						),
						'target' => array(
							'small' => array(
								'src' => $uri . 'feather/target.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/target.svg',
								'height' => 128
							)
						),
						'terminal' => array(
							'small' => array(
								'src' => $uri . 'feather/terminal.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/terminal.svg',
								'height' => 128
							)
						),
						'thermometer' => array(
							'small' => array(
								'src' => $uri . 'feather/thermometer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/thermometer.svg',
								'height' => 128
							)
						),
						'thumbs-down' => array(
							'small' => array(
								'src' => $uri . 'feather/thumbs-down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/thumbs-down.svg',
								'height' => 128
							)
						),
						'thumbs-up' => array(
							'small' => array(
								'src' => $uri . 'feather/thumbs-up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/thumbs-up.svg',
								'height' => 128
							)
						),
						'toggle-left' => array(
							'small' => array(
								'src' => $uri . 'feather/toggle-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/toggle-left.svg',
								'height' => 128
							)
						),
						'toggle-right' => array(
							'small' => array(
								'src' => $uri . 'feather/toggle-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/toggle-right.svg',
								'height' => 128
							)
						),
						'tool' => array(
							'small' => array(
								'src' => $uri . 'feather/tool.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/tool.svg',
								'height' => 128
							)
						),
						'trash-2' => array(
							'small' => array(
								'src' => $uri . 'feather/trash-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/trash-2.svg',
								'height' => 128
							)
						),
						'trash' => array(
							'small' => array(
								'src' => $uri . 'feather/trash.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/trash.svg',
								'height' => 128
							)
						),
						'trello' => array(
							'small' => array(
								'src' => $uri . 'feather/trello.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/trello.svg',
								'height' => 128
							)
						),
						'trending-down' => array(
							'small' => array(
								'src' => $uri . 'feather/trending-down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/trending-down.svg',
								'height' => 128
							)
						),
						'trending-up' => array(
							'small' => array(
								'src' => $uri . 'feather/trending-up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/trending-up.svg',
								'height' => 128
							)
						),
						'triangle' => array(
							'small' => array(
								'src' => $uri . 'feather/triangle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/triangle.svg',
								'height' => 128
							)
						),
						'truck' => array(
							'small' => array(
								'src' => $uri . 'feather/truck.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/truck.svg',
								'height' => 128
							)
						),
						'tv' => array(
							'small' => array(
								'src' => $uri . 'feather/tv.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/tv.svg',
								'height' => 128
							)
						),
						'twitch' => array(
							'small' => array(
								'src' => $uri . 'feather/twitch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/twitch.svg',
								'height' => 128
							)
						),
						'twitter' => array(
							'small' => array(
								'src' => $uri . 'feather/twitter.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/twitter.svg',
								'height' => 128
							)
						),
						'type' => array(
							'small' => array(
								'src' => $uri . 'feather/type.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/type.svg',
								'height' => 128
							)
						),
						'umbrella' => array(
							'small' => array(
								'src' => $uri . 'feather/umbrella.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/umbrella.svg',
								'height' => 128
							)
						),
						'underline' => array(
							'small' => array(
								'src' => $uri . 'feather/underline.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/underline.svg',
								'height' => 128
							)
						),
						'unlock' => array(
							'small' => array(
								'src' => $uri . 'feather/unlock.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/unlock.svg',
								'height' => 128
							)
						),
						'upload-cloud' => array(
							'small' => array(
								'src' => $uri . 'feather/upload-cloud.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/upload-cloud.svg',
								'height' => 128
							)
						),
						'upload' => array(
							'small' => array(
								'src' => $uri . 'feather/upload.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/upload.svg',
								'height' => 128
							)
						),
						'user-check' => array(
							'small' => array(
								'src' => $uri . 'feather/user-check.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/user-check.svg',
								'height' => 128
							)
						),
						'user-minus' => array(
							'small' => array(
								'src' => $uri . 'feather/user-minus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/user-minus.svg',
								'height' => 128
							)
						),
						'user-plus' => array(
							'small' => array(
								'src' => $uri . 'feather/user-plus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/user-plus.svg',
								'height' => 128
							)
						),
						'user-x' => array(
							'small' => array(
								'src' => $uri . 'feather/user-x.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/user-x.svg',
								'height' => 128
							)
						),
						'user' => array(
							'small' => array(
								'src' => $uri . 'feather/user.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/user.svg',
								'height' => 128
							)
						),
						'users' => array(
							'small' => array(
								'src' => $uri . 'feather/users.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/users.svg',
								'height' => 128
							)
						),
						'video-off' => array(
							'small' => array(
								'src' => $uri . 'feather/video-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/video-off.svg',
								'height' => 128
							)
						),
						'video' => array(
							'small' => array(
								'src' => $uri . 'feather/video.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/video.svg',
								'height' => 128
							)
						),
						'voicemail' => array(
							'small' => array(
								'src' => $uri . 'feather/voicemail.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/voicemail.svg',
								'height' => 128
							)
						),
						'volume-1' => array(
							'small' => array(
								'src' => $uri . 'feather/volume-1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/volume-1.svg',
								'height' => 128
							)
						),
						'volume-2' => array(
							'small' => array(
								'src' => $uri . 'feather/volume-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/volume-2.svg',
								'height' => 128
							)
						),
						'volume-x' => array(
							'small' => array(
								'src' => $uri . 'feather/volume-x.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/volume-x.svg',
								'height' => 128
							)
						),
						'volume' => array(
							'small' => array(
								'src' => $uri . 'feather/volume.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/volume.svg',
								'height' => 128
							)
						),
						'watch' => array(
							'small' => array(
								'src' => $uri . 'feather/watch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/watch.svg',
								'height' => 128
							)
						),
						'wifi-off' => array(
							'small' => array(
								'src' => $uri . 'feather/wifi-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/wifi-off.svg',
								'height' => 128
							)
						),
						'wifi' => array(
							'small' => array(
								'src' => $uri . 'feather/wifi.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/wifi.svg',
								'height' => 128
							)
						),
						'wind' => array(
							'small' => array(
								'src' => $uri . 'feather/wind.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/wind.svg',
								'height' => 128
							)
						),
						'x-circle' => array(
							'small' => array(
								'src' => $uri . 'feather/x-circle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/x-circle.svg',
								'height' => 128
							)
						),
						'x-octagon' => array(
							'small' => array(
								'src' => $uri . 'feather/x-octagon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/x-octagon.svg',
								'height' => 128
							)
						),
						'x-square' => array(
							'small' => array(
								'src' => $uri . 'feather/x-square.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/x-square.svg',
								'height' => 128
							)
						),
						'x' => array(
							'small' => array(
								'src' => $uri . 'feather/x.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/x.svg',
								'height' => 128
							)
						),
						'youtube' => array(
							'small' => array(
								'src' => $uri . 'feather/youtube.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/youtube.svg',
								'height' => 128
							)
						),
						'zap-off' => array(
							'small' => array(
								'src' => $uri . 'feather/zap-off.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/zap-off.svg',
								'height' => 128
							)
						),
						'zap' => array(
							'small' => array(
								'src' => $uri . 'feather/zap.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/zap.svg',
								'height' => 128
							)
						),
						'zoom-in' => array(
							'small' => array(
								'src' => $uri . 'feather/zoom-in.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/zoom-in.svg',
								'height' => 128
							)
						),
						'zoom-out' => array(
							'small' => array(
								'src' => $uri . 'feather/zoom-out.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'feather/zoom-out.svg',
								'height' => 128
							)
						),
					),
				),
			),
			'duotone-icons' => array(
				'icon' => array(
					'type' => 'image-picker',
					'label' => __('Icon', 'fw'),
					'choices' => array(
						'Brassiere' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Brassiere.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Brassiere.svg',
								'height' => 128
							)
						),
						'Briefcase' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Briefcase.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Briefcase.svg',
								'height' => 128
							)
						),
						'Cap' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cap.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cap.svg',
								'height' => 128
							)
						),
						'Crown' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Crown.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Crown.svg',
								'height' => 128
							)
						),
						'Dress' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Dress.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Dress.svg',
								'height' => 128
							)
						),
						'Hanger' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Hanger.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Hanger.svg',
								'height' => 128
							)
						),
						'Hat' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Hat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Hat.svg',
								'height' => 128
							)
						),
						'Panties' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Panties.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Panties.svg',
								'height' => 128
							)
						),
						'Shirt' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shirt.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shirt.svg',
								'height' => 128
							)
						),
						'Shoes' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shoes.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shoes.svg',
								'height' => 128
							)
						),
						'Shorts' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shorts.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shorts.svg',
								'height' => 128
							)
						),
						'Sneakers' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sneakers.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sneakers.svg',
								'height' => 128
							)
						),
						'Socks' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Socks.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Socks.svg',
								'height' => 128
							)
						),
						'Sun-glasses' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sun-glasses.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sun-glasses.svg',
								'height' => 128
							)
						),
						'T-Shirt' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/T-Shirt.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/T-Shirt.svg',
								'height' => 128
							)
						),
						'Tie' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Tie.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Tie.svg',
								'height' => 128
							)
						),
						'Backspace' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Backspace.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Backspace.svg',
								'height' => 128
							)
						),
						'CMD' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/CMD.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/CMD.svg',
								'height' => 128
							)
						),
						'Code' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Code.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Code.svg',
								'height' => 128
							)
						),
						'Commit' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Commit.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Commit.svg',
								'height' => 128
							)
						),
						'Compiling' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Compiling.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Compiling.svg',
								'height' => 128
							)
						),
						'Control' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Control.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Control.svg',
								'height' => 128
							)
						),
						'Done-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Done-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Done-circle.svg',
								'height' => 128
							)
						),
						'Error-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Error-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Error-circle.svg',
								'height' => 128
							)
						),
						'Git 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Git-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Git-1.svg',
								'height' => 128
							)
						),
						'Git 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Git-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Git-2.svg',
								'height' => 128
							)
						),
						'Git 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Git-3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Git-3.svg',
								'height' => 128
							)
						),
						'Git 4' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Git-4.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Git-4.svg',
								'height' => 128
							)
						),
						'Github' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Github.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Github.svg',
								'height' => 128
							)
						),
						'Info-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Info-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Info-circle.svg',
								'height' => 128
							)
						),
						'Left-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Left-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Left-circle.svg',
								'height' => 128
							)
						),
						'Loading' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Loading.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Loading.svg',
								'height' => 128
							)
						),
						'Lock-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Lock-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Lock-circle.svg',
								'height' => 128
							)
						),
						'Lock-overturning' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Lock-overturning.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Lock-overturning.svg',
								'height' => 128
							)
						),
						'Minus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Minus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Minus.svg',
								'height' => 128
							)
						),
						'Option' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Option.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Option.svg',
								'height' => 128
							)
						),
						'Plus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Plus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Plus.svg',
								'height' => 128
							)
						),
						'Puzzle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Puzzle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Puzzle.svg',
								'height' => 128
							)
						),
						'Question-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Question-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Question-circle.svg',
								'height' => 128
							)
						),
						'Right-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Right-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Right-circle.svg',
								'height' => 128
							)
						),
						'Settings 4' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Settings 4.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Settings 4.svg',
								'height' => 128
							)
						),
						'Shift' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shift.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shift.svg',
								'height' => 128
							)
						),
						'Spy' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Spy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Spy.svg',
								'height' => 128
							)
						),
						'Stop' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Stop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Stop.svg',
								'height' => 128
							)
						),
						'Terminal' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Terminal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Terminal.svg',
								'height' => 128
							)
						),
						'Thunder-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Thunder-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Thunder-circle.svg',
								'height' => 128
							)
						),
						'Time-schedule' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Time-schedule.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Time-schedule.svg',
								'height' => 128
							)
						),
						'Warning-1-circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Warning-1-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Warning-1-circle.svg',
								'height' => 128
							)
						),
						'Warning-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Warning-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Warning-2.svg',
								'height' => 128
							)
						),
						'Active-call' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Active-call.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Active-call.svg',
								'height' => 128
							)
						),
						'Add-user' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Add-user.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Add-user.svg',
								'height' => 128
							)
						),
						'Address-card' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Address-card.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Address-card.svg',
								'height' => 128
							)
						),
						'Adress-book 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Adress-book 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Adress-book 1.svg',
								'height' => 128
							)
						),
						'Adress-book 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Adress-book 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Adress-book 2.svg',
								'height' => 128
							)
						),
						'Archive' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Archive.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Archive.svg',
								'height' => 128
							)
						),
						'Call 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Call 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Call 1.svg',
								'height' => 128
							)
						),
						'Call' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Call.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Call.svg',
								'height' => 128
							)
						),
						'Chat 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat 1.svg',
								'height' => 128
							)
						),
						'Chat 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat 2.svg',
								'height' => 128
							)
						),
						'Chat 4' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat 4.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat 4.svg',
								'height' => 128
							)
						),
						'Chat 5' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat 5.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat 5.svg',
								'height' => 128
							)
						),
						'Chat 6' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat 6.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat 6.svg',
								'height' => 128
							)
						),
						'Chat-check' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat-check.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat-check.svg',
								'height' => 128
							)
						),
						'Chat-error' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat-error.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat-error.svg',
								'height' => 128
							)
						),
						'Chat-locked' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat-locked.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat-locked.svg',
								'height' => 128
							)
						),
						'Chat-smile' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chat-smile.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chat-smile.svg',
								'height' => 128
							)
						),
						'Clipboard-check' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Clipboard-check.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Clipboard-check.svg',
								'height' => 128
							)
						),
						'Clipboard-list' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Clipboard-list.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Clipboard-list.svg',
								'height' => 128
							)
						),
						'Contact 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Contact 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Contact 1.svg',
								'height' => 128
							)
						),
						'Delete-user' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Delete-user.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Delete-user.svg',
								'height' => 128
							)
						),
						'Dial-numbers' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Dial-numbers.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Dial-numbers.svg',
								'height' => 128
							)
						),
						'Flag' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Flag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Flag.svg',
								'height' => 128
							)
						),
						'Forward' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Forward.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Forward.svg',
								'height' => 128
							)
						),
						'Group-chat' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Group-chat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Group-chat.svg',
								'height' => 128
							)
						),
						'Group' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Group.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Group.svg',
								'height' => 128
							)
						),
						'Incoming-box' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Incoming-box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Incoming-box.svg',
								'height' => 128
							)
						),
						'Incoming-call' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Incoming-call.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Incoming-call.svg',
								'height' => 128
							)
						),
						'Incoming-mail' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Incoming-mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Incoming-mail.svg',
								'height' => 128
							)
						),
						'Mail-@' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-@.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-@.svg',
								'height' => 128
							)
						),
						'Mail-attachment' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-attachment.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-attachment.svg',
								'height' => 128
							)
						),
						'Mail-box' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-box.svg',
								'height' => 128
							)
						),
						'Mail-error' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-error.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-error.svg',
								'height' => 128
							)
						),
						'Mail-heart' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-heart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-heart.svg',
								'height' => 128
							)
						),
						'Mail-locked' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-locked.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-locked.svg',
								'height' => 128
							)
						),
						'Mail-notification' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-notification.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-notification.svg',
								'height' => 128
							)
						),
						'Mail-opened' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-opened.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-opened.svg',
								'height' => 128
							)
						),
						'Mail-unocked' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail-unocked.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail-unocked.svg',
								'height' => 128
							)
						),
						'Mail' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mail.svg',
								'height' => 128
							)
						),
						'Missed-call' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Missed-call.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Missed-call.svg',
								'height' => 128
							)
						),
						'Outgoing-box' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Outgoing-box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Outgoing-box.svg',
								'height' => 128
							)
						),
						'Outgoing-call' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Outgoing-call.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Outgoing-call.svg',
								'height' => 128
							)
						),
						'Outgoing-mail' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Outgoing-mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Outgoing-mail.svg',
								'height' => 128
							)
						),
						'RSS' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/RSS.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/RSS.svg',
								'height' => 128
							)
						),
						'Readed-mail' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Readed-mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Readed-mail.svg',
								'height' => 128
							)
						),
						'Reply-all' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Reply-all.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Reply-all.svg',
								'height' => 128
							)
						),
						'Reply' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Reply.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Reply.svg',
								'height' => 128
							)
						),
						'Right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Right.svg',
								'height' => 128
							)
						),
						'Safe-chat' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Safe-chat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Safe-chat.svg',
								'height' => 128
							)
						),
						'Send' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Send.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Send.svg',
								'height' => 128
							)
						),
						'Sending mail' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sending mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sending mail.svg',
								'height' => 128
							)
						),
						'Sending' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sending.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sending.svg',
								'height' => 128
							)
						),
						'Share' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Share.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Share.svg',
								'height' => 128
							)
						),
						'Shield-thunder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shield-thunder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shield-thunder.svg',
								'height' => 128
							)
						),
						'Shield-user' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shield-user.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shield-user.svg',
								'height' => 128
							)
						),
						'Snoozed-mail' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Snoozed-mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Snoozed-mail.svg',
								'height' => 128
							)
						),
						'Spam' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Spam.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Spam.svg',
								'height' => 128
							)
						),
						'Thumbtack' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Thumbtack.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Thumbtack.svg',
								'height' => 128
							)
						),
						'Urgent-mail' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Urgent-mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Urgent-mail.svg',
								'height' => 128
							)
						),
						'Write' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Write.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Write.svg',
								'height' => 128
							)
						),
						'Baking-glove' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Baking-glove.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Baking-glove.svg',
								'height' => 128
							)
						),
						'Bowl' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bowl.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bowl.svg',
								'height' => 128
							)
						),
						'Chef' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chef.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chef.svg',
								'height' => 128
							)
						),
						'Cooking-book' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cooking-book.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cooking-book.svg',
								'height' => 128
							)
						),
						'Cooking-pot' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cooking-pot.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cooking-pot.svg',
								'height' => 128
							)
						),
						'Cutting board' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cutting board.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cutting board.svg',
								'height' => 128
							)
						),
						'Dinner' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Dinner.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Dinner.svg',
								'height' => 128
							)
						),
						'Dish' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Dish.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Dish.svg',
								'height' => 128
							)
						),
						'Dishes' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Dishes.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Dishes.svg',
								'height' => 128
							)
						),
						'Fork-spoon-knife' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fork-spoon-knife.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fork-spoon-knife.svg',
								'height' => 128
							)
						),
						'Fork-spoon' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fork-spoon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fork-spoon.svg',
								'height' => 128
							)
						),
						'Fork' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fork.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fork.svg',
								'height' => 128
							)
						),
						'Frying-pan' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Frying-pan.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Frying-pan.svg',
								'height' => 128
							)
						),
						'Grater' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Grater.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Grater.svg',
								'height' => 128
							)
						),
						'Kitchen-scale' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Kitchen-scale.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Kitchen-scale.svg',
								'height' => 128
							)
						),
						'Knife 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Knife 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Knife 1.svg',
								'height' => 128
							)
						),
						'Knife 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Knife 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Knife 2.svg',
								'height' => 128
							)
						),
						'Knife&fork 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Knife&fork 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Knife&fork 1.svg',
								'height' => 128
							)
						),
						'Knife&fork 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Knife&fork 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Knife&fork 2.svg',
								'height' => 128
							)
						),
						'Ladle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Ladle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Ladle.svg',
								'height' => 128
							)
						),
						'Rolling-pin' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Rolling-pin.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Rolling-pin.svg',
								'height' => 128
							)
						),
						'Saucepan' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Saucepan.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Saucepan.svg',
								'height' => 128
							)
						),
						'Shovel' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shovel.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shovel.svg',
								'height' => 128
							)
						),
						'Sieve' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sieve.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sieve.svg',
								'height' => 128
							)
						),
						'Spoon' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Spoon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Spoon.svg',
								'height' => 128
							)
						),
						'Adjust' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Adjust.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Adjust.svg',
								'height' => 128
							)
						),
						'Anchor-center-down' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-center-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-center-down.svg',
								'height' => 128
							)
						),
						'Anchor-center-up' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-center-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-center-up.svg',
								'height' => 128
							)
						),
						'Anchor-center' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-center.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-center.svg',
								'height' => 128
							)
						),
						'Anchor-left-down' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-left-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-left-down.svg',
								'height' => 128
							)
						),
						'Anchor-left-up' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-left-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-left-up.svg',
								'height' => 128
							)
						),
						'Anchor-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-left.svg',
								'height' => 128
							)
						),
						'Anchor-right-down' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-right-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-right-down.svg',
								'height' => 128
							)
						),
						'Anchor-right-up' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-right-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-right-up.svg',
								'height' => 128
							)
						),
						'Anchor-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Anchor-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Anchor-right.svg',
								'height' => 128
							)
						),
						'Arrows' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrows.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrows.svg',
								'height' => 128
							)
						),
						'Bezier-curve' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bezier-curve.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bezier-curve.svg',
								'height' => 128
							)
						),
						'Border' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Border.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Border.svg',
								'height' => 128
							)
						),
						'Brush' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Brush.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Brush.svg',
								'height' => 128
							)
						),
						'Bucket' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bucket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bucket.svg',
								'height' => 128
							)
						),
						'Cap-1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cap-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cap-1.svg',
								'height' => 128
							)
						),
						'Cap-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cap-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cap-2.svg',
								'height' => 128
							)
						),
						'Cap-3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cap-3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cap-3.svg',
								'height' => 128
							)
						),
						'Circle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Circle.svg',
								'height' => 128
							)
						),
						'Color-profile' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Color-profile.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Color-profile.svg',
								'height' => 128
							)
						),
						'Color' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Color.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Color.svg',
								'height' => 128
							)
						),
						'Component' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Component.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Component.svg',
								'height' => 128
							)
						),
						'Crop' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Crop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Crop.svg',
								'height' => 128
							)
						),
						'Difference' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Difference.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Difference.svg',
								'height' => 128
							)
						),
						'Edit' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Edit.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Edit.svg',
								'height' => 128
							)
						),
						'Eraser' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Eraser.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Eraser.svg',
								'height' => 128
							)
						),
						'Flatten' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Flatten.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Flatten.svg',
								'height' => 128
							)
						),
						'Flip-horizontal' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Flip-horizontal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Flip-horizontal.svg',
								'height' => 128
							)
						),
						'Flip-vertical' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Flip-vertical.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Flip-vertical.svg',
								'height' => 128
							)
						),
						'Horizontal' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Horizontal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Horizontal.svg',
								'height' => 128
							)
						),
						'Image' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Image.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Image.svg',
								'height' => 128
							)
						),
						'Interselect' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Interselect.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Interselect.svg',
								'height' => 128
							)
						),
						'Join-1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Join-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Join-1.svg',
								'height' => 128
							)
						),
						'Join-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Join-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Join-2.svg',
								'height' => 128
							)
						),
						'Join-3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Join-3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Join-3.svg',
								'height' => 128
							)
						),
						'Layers' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layers.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layers.svg',
								'height' => 128
							)
						),
						'Line' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Line.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Line.svg',
								'height' => 128
							)
						),
						'Magic' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Magic.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Magic.svg',
								'height' => 128
							)
						),
						'Mask' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mask.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mask.svg',
								'height' => 128
							)
						),
						'Patch' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Patch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Patch.svg',
								'height' => 128
							)
						),
						'Pen&ruller' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pen&ruller.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pen&ruller.svg',
								'height' => 128
							)
						),
						'Pen-tool-vector' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pen-tool-vector.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pen-tool-vector.svg',
								'height' => 128
							)
						),
						'Pencil' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pencil.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pencil.svg',
								'height' => 128
							)
						),
						'Picker' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Picker.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Picker.svg',
								'height' => 128
							)
						),
						'Pixels' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pixels.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pixels.svg',
								'height' => 128
							)
						),
						'Polygon' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Polygon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Polygon.svg',
								'height' => 128
							)
						),
						'Position' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Position.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Position.svg',
								'height' => 128
							)
						),
						'Rectangle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Rectangle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Rectangle.svg',
								'height' => 128
							)
						),
						'Saturation' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Saturation.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Saturation.svg',
								'height' => 128
							)
						),
						'Select' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Select.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Select.svg',
								'height' => 128
							)
						),
						'Sketch' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sketch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sketch.svg',
								'height' => 128
							)
						),
						'Stamp' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Stamp.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Stamp.svg',
								'height' => 128
							)
						),
						'Substract' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Substract.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Substract.svg',
								'height' => 128
							)
						),
						'Target' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Target.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Target.svg',
								'height' => 128
							)
						),
						'Triangle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Triangle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Triangle.svg',
								'height' => 128
							)
						),
						'Union' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Union.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Union.svg',
								'height' => 128
							)
						),
						'Vertical' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Vertical.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Vertical.svg',
								'height' => 128
							)
						),
						'Zoom minus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Zoom minus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Zoom minus.svg',
								'height' => 128
							)
						),
						'Zoom plus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Zoom plus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Zoom plus.svg',
								'height' => 128
							)
						),
						'Airpods' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Airpods.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Airpods.svg',
								'height' => 128
							)
						),
						'Android' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Android.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Android.svg',
								'height' => 128
							)
						),
						'Apple-Watch' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Apple-Watch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Apple-Watch.svg',
								'height' => 128
							)
						),
						'Battery-charging' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Battery-charging.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Battery-charging.svg',
								'height' => 128
							)
						),
						'Battery-empty' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Battery-empty.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Battery-empty.svg',
								'height' => 128
							)
						),
						'Battery-full' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Battery-full.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Battery-full.svg',
								'height' => 128
							)
						),
						'Battery-half' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Battery-half.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Battery-half.svg',
								'height' => 128
							)
						),
						'Bluetooth' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bluetooth.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bluetooth.svg',
								'height' => 128
							)
						),
						'CPU 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/CPU 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/CPU 1.svg',
								'height' => 128
							)
						),
						'CPU 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/CPU 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/CPU 2.svg',
								'height' => 128
							)
						),
						'Camera' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Camera.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Camera.svg',
								'height' => 128
							)
						),
						'Cardboard-vr' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cardboard-vr.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cardboard-vr.svg',
								'height' => 128
							)
						),
						'Cassete' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cassete.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cassete.svg',
								'height' => 128
							)
						),
						'Diagnostics' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Diagnostics.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Diagnostics.svg',
								'height' => 128
							)
						),
						'Display 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Display 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Display 1.svg',
								'height' => 128
							)
						),
						'Display 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Display 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Display 2.svg',
								'height' => 128
							)
						),
						'Display 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Display 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Display 3.svg',
								'height' => 128
							)
						),
						'Gameboy' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Gameboy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Gameboy.svg',
								'height' => 128
							)
						),
						'Gamepad 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Gamepad 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Gamepad 1.svg',
								'height' => 128
							)
						),
						'Gamepad 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Gamepad 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Gamepad 2.svg',
								'height' => 128
							)
						),
						'Generator' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Generator.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Generator.svg',
								'height' => 128
							)
						),
						'Hard-drive' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Hard-drive.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Hard-drive.svg',
								'height' => 128
							)
						),
						'Headphones' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Headphones.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Headphones.svg',
								'height' => 128
							)
						),
						'Homepod' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Homepod.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Homepod.svg',
								'height' => 128
							)
						),
						'Keyboard' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Keyboard.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Keyboard.svg',
								'height' => 128
							)
						),
						'LTE 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/LTE 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/LTE 1.svg',
								'height' => 128
							)
						),
						'LTE 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/LTE 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/LTE 2.svg',
								'height' => 128
							)
						),
						'Laptop-macbook' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Laptop-macbook.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Laptop-macbook.svg',
								'height' => 128
							)
						),
						'Laptop' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Laptop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Laptop.svg',
								'height' => 128
							)
						),
						'Mic' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mic.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mic.svg',
								'height' => 128
							)
						),
						'Midi' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Midi.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Midi.svg',
								'height' => 128
							)
						),
						'Mouse' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mouse.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mouse.svg',
								'height' => 128
							)
						),
						'Phone' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Phone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Phone.svg',
								'height' => 128
							)
						),
						'Printer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Printer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Printer.svg',
								'height' => 128
							)
						),
						'Radio' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Radio.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Radio.svg',
								'height' => 128
							)
						),
						'Router 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Router 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Router 1.svg',
								'height' => 128
							)
						),
						'Router 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Router 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Router 2.svg',
								'height' => 128
							)
						),
						'SD-card' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/SD-card.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/SD-card.svg',
								'height' => 128
							)
						),
						'Server' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Server.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Server.svg',
								'height' => 128
							)
						),
						'Speaker' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Speaker.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Speaker.svg',
								'height' => 128
							)
						),
						'TV 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/TV 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/TV 1.svg',
								'height' => 128
							)
						),
						'TV 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/TV 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/TV 2.svg',
								'height' => 128
							)
						),
						'Tablet' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Tablet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Tablet.svg',
								'height' => 128
							)
						),
						'USB' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/USB.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/USB.svg',
								'height' => 128
							)
						),
						'Usb-storage' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Usb-storage.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Usb-storage.svg',
								'height' => 128
							)
						),
						'Video-camera' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Video-camera.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Video-camera.svg',
								'height' => 128
							)
						),
						'Watch 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Watch 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Watch 1.svg',
								'height' => 128
							)
						),
						'Watch 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Watch 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Watch 2.svg',
								'height' => 128
							)
						),
						'Wi-fi' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wi-fi.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wi-fi.svg',
								'height' => 128
							)
						),
						'iMac' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/iMac.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/iMac.svg',
								'height' => 128
							)
						),
						'iPhone-X' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/iPhone-X.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/iPhone-X.svg',
								'height' => 128
							)
						),
						'iPhone-back' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/iPhone-back.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/iPhone-back.svg',
								'height' => 128
							)
						),
						'iPhone-x-back' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/iPhone-x-back.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/iPhone-x-back.svg',
								'height' => 128
							)
						),
						'Air-conditioning' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Air-conditioning.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Air-conditioning.svg',
								'height' => 128
							)
						),
						'Blender' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Blender.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Blender.svg',
								'height' => 128
							)
						),
						'Fan' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fan.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fan.svg',
								'height' => 128
							)
						),
						'Fridge' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fridge.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fridge.svg',
								'height' => 128
							)
						),
						'Gas-stove' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Gas-stove.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Gas-stove.svg',
								'height' => 128
							)
						),
						'Hair-dryer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Hair-dryer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Hair-dryer.svg',
								'height' => 128
							)
						),
						'Highvoltage' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Highvoltage.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Highvoltage.svg',
								'height' => 128
							)
						),
						'Iron' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Iron.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Iron.svg',
								'height' => 128
							)
						),
						'Kettle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Kettle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Kettle.svg',
								'height' => 128
							)
						),
						'Mixer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mixer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mixer.svg',
								'height' => 128
							)
						),
						'Outlet' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Outlet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Outlet.svg',
								'height' => 128
							)
						),
						'Range-hood' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Range-hood.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Range-hood.svg',
								'height' => 128
							)
						),
						'Shutdown' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shutdown.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shutdown.svg',
								'height' => 128
							)
						),
						'Socket-eu' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Socket-eu.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Socket-eu.svg',
								'height' => 128
							)
						),
						'Socket-us' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Socket-us.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Socket-us.svg',
								'height' => 128
							)
						),
						'Washer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Washer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Washer.svg',
								'height' => 128
							)
						),
						'Cloud-download' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloud-download.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloud-download.svg',
								'height' => 128
							)
						),
						'Cloud-upload' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloud-upload.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloud-upload.svg',
								'height' => 128
							)
						),
						'Compilation' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Compilation.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Compilation.svg',
								'height' => 128
							)
						),
						'Compiled-file' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Compiled-file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Compiled-file.svg',
								'height' => 128
							)
						),
						'Deleted-file' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Deleted-file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Deleted-file.svg',
								'height' => 128
							)
						),
						'Deleted-folder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Deleted-folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Deleted-folder.svg',
								'height' => 128
							)
						),
						'Download' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Download.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Download.svg',
								'height' => 128
							)
						),
						'Downloaded file' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Downloaded file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Downloaded file.svg',
								'height' => 128
							)
						),
						'Downloads-folder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Downloads-folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Downloads-folder.svg',
								'height' => 128
							)
						),
						'Export' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Export.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Export.svg',
								'height' => 128
							)
						),
						'File-cloud' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/File-cloud.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/File-cloud.svg',
								'height' => 128
							)
						),
						'File-done' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/File-done.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/File-done.svg',
								'height' => 128
							)
						),
						'File-minus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/File-minus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/File-minus.svg',
								'height' => 128
							)
						),
						'File-plus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/File-plus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/File-plus.svg',
								'height' => 128
							)
						),
						'File' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/File.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/File.svg',
								'height' => 128
							)
						),
						'Folder-check' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-check.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-check.svg',
								'height' => 128
							)
						),
						'Folder-cloud' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-cloud.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-cloud.svg',
								'height' => 128
							)
						),
						'Folder-error' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-error.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-error.svg',
								'height' => 128
							)
						),
						'Folder-heart' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-heart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-heart.svg',
								'height' => 128
							)
						),
						'Folder-minus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-minus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-minus.svg',
								'height' => 128
							)
						),
						'Folder-plus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-plus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-plus.svg',
								'height' => 128
							)
						),
						'Folder-solid' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-solid.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-solid.svg',
								'height' => 128
							)
						),
						'Folder-star' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-star.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-star.svg',
								'height' => 128
							)
						),
						'Folder-thunder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder-thunder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder-thunder.svg',
								'height' => 128
							)
						),
						'Folder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder.svg',
								'height' => 128
							)
						),
						'Group-folders' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Group-folders.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Group-folders.svg',
								'height' => 128
							)
						),
						'Import' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Import.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Import.svg',
								'height' => 128
							)
						),
						'Locked-folder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Locked-folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Locked-folder.svg',
								'height' => 128
							)
						),
						'Media-folder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Media-folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Media-folder.svg',
								'height' => 128
							)
						),
						'Media' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Media.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Media.svg',
								'height' => 128
							)
						),
						'Music' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Music.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Music.svg',
								'height' => 128
							)
						),
						'Pictures 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pictures 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pictures 1.svg',
								'height' => 128
							)
						),
						'Pictures 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pictures 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pictures 2.svg',
								'height' => 128
							)
						),
						'Protected-file' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Protected-file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Protected-file.svg',
								'height' => 128
							)
						),
						'Selected-file' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Selected-file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Selected-file.svg',
								'height' => 128
							)
						),
						'Share' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Share.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Share.svg',
								'height' => 128
							)
						),
						'Upload-folder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Upload-folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Upload-folder.svg',
								'height' => 128
							)
						),
						'Upload' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Upload.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Upload.svg',
								'height' => 128
							)
						),
						'Uploaded-file' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Uploaded-file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Uploaded-file.svg',
								'height' => 128
							)
						),
						'User-folder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/User-folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/User-folder.svg',
								'height' => 128
							)
						),
						'Beer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Beer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Beer.svg',
								'height' => 128
							)
						),
						'Bottle 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bottle 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bottle 1.svg',
								'height' => 128
							)
						),
						'Bottle 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bottle 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bottle 2.svg',
								'height' => 128
							)
						),
						'Bread' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bread.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bread.svg',
								'height' => 128
							)
						),
						'Bucket' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bucket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bucket.svg',
								'height' => 128
							)
						),
						'Burger' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Burger.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Burger.svg',
								'height' => 128
							)
						),
						'Cake' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cake.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cake.svg',
								'height' => 128
							)
						),
						'Carrot' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Carrot.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Carrot.svg',
								'height' => 128
							)
						),
						'Cheese' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cheese.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cheese.svg',
								'height' => 128
							)
						),
						'Chicken' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chicken.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chicken.svg',
								'height' => 128
							)
						),
						'Coffee 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Coffee 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Coffee 1.svg',
								'height' => 128
							)
						),
						'Coffee 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Coffee 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Coffee 2.svg',
								'height' => 128
							)
						),
						'Cookie' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cookie.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cookie.svg',
								'height' => 128
							)
						),
						'Dinner' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Dinner.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Dinner.svg',
								'height' => 128
							)
						),
						'Fish' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fish.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fish.svg',
								'height' => 128
							)
						),
						'French Bread' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/French Bread.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/French Bread.svg',
								'height' => 128
							)
						),
						'Glass-martini' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Glass-martini.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Glass-martini.svg',
								'height' => 128
							)
						),
						'Ice-cream 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Ice-cream 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Ice-cream 1.svg',
								'height' => 128
							)
						),
						'Ice-cream 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Ice-cream 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Ice-cream 2.svg',
								'height' => 128
							)
						),
						'Miso-soup' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Miso-soup.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Miso-soup.svg',
								'height' => 128
							)
						),
						'Orange' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Orange.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Orange.svg',
								'height' => 128
							)
						),
						'Pizza' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pizza.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pizza.svg',
								'height' => 128
							)
						),
						'Sushi' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sushi.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sushi.svg',
								'height' => 128
							)
						),
						'Two-bottles' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Two-bottles.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Two-bottles.svg',
								'height' => 128
							)
						),
						'Wine' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wine.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wine.svg',
								'height' => 128
							)
						),
						'Attachment 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Attachment 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Attachment 1.svg',
								'height' => 128
							)
						),
						'Attachment 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Attachment 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Attachment 2.svg',
								'height' => 128
							)
						),
						'Binocular' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Binocular.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Binocular.svg',
								'height' => 128
							)
						),
						'Bookmark' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bookmark.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bookmark.svg',
								'height' => 128
							)
						),
						'Clip' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Clip.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Clip.svg',
								'height' => 128
							)
						),
						'Clipboard' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Clipboard.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Clipboard.svg',
								'height' => 128
							)
						),
						'Cursor' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cursor.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cursor.svg',
								'height' => 128
							)
						),
						'Dislike' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Dislike.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Dislike.svg',
								'height' => 128
							)
						),
						'Duplicate' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Duplicate.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Duplicate.svg',
								'height' => 128
							)
						),
						'Edit' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Edit.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Edit.svg',
								'height' => 128
							)
						),
						'Expand-arrows' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Expand-arrows.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Expand-arrows.svg',
								'height' => 128
							)
						),
						'Fire' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fire.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fire.svg',
								'height' => 128
							)
						),
						'Folder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Folder.svg',
								'height' => 128
							)
						),
						'Half-heart' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Half-heart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Half-heart.svg',
								'height' => 128
							)
						),
						'Half-star' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Half-star.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Half-star.svg',
								'height' => 128
							)
						),
						'Heart' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Heart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Heart.svg',
								'height' => 128
							)
						),
						'Hidden' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Hidden.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Hidden.svg',
								'height' => 128
							)
						),
						'Like' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Like.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Like.svg',
								'height' => 128
							)
						),
						'Lock' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Lock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Lock.svg',
								'height' => 128
							)
						),
						'Notification 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Notification 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Notification 2.svg',
								'height' => 128
							)
						),
						'Notifications 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Notifications 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Notifications 1.svg',
								'height' => 128
							)
						),
						'Other 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Other 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Other 1.svg',
								'height' => 128
							)
						),
						'Other 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Other 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Other 2.svg',
								'height' => 128
							)
						),
						'Sad' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sad.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sad.svg',
								'height' => 128
							)
						),
						'Save' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Save.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Save.svg',
								'height' => 128
							)
						),
						'Scale' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Scale.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Scale.svg',
								'height' => 128
							)
						),
						'Scissors' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Scissors.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Scissors.svg',
								'height' => 128
							)
						),
						'Search' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Search.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Search.svg',
								'height' => 128
							)
						),
						'Settings 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Settings 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Settings 3.svg',
								'height' => 128
							)
						),
						'Settings-1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Settings-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Settings-1.svg',
								'height' => 128
							)
						),
						'Settings-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Settings-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Settings-2.svg',
								'height' => 128
							)
						),
						'Shield-check' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shield-check.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shield-check.svg',
								'height' => 128
							)
						),
						'Shield-disabled' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shield-disabled.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shield-disabled.svg',
								'height' => 128
							)
						),
						'Shield-protected' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shield-protected.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shield-protected.svg',
								'height' => 128
							)
						),
						'Size' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Size.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Size.svg',
								'height' => 128
							)
						),
						'Smile' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Smile.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Smile.svg',
								'height' => 128
							)
						),
						'Star' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Star.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Star.svg',
								'height' => 128
							)
						),
						'Thunder-move' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Thunder-move.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Thunder-move.svg',
								'height' => 128
							)
						),
						'Thunder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Thunder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Thunder.svg',
								'height' => 128
							)
						),
						'Trash' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Trash.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Trash.svg',
								'height' => 128
							)
						),
						'Unlock' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Unlock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Unlock.svg',
								'height' => 128
							)
						),
						'Update' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Update.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Update.svg',
								'height' => 128
							)
						),
						'User' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/User.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/User.svg',
								'height' => 128
							)
						),
						'Visible' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Visible.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Visible.svg',
								'height' => 128
							)
						),
						'Air-ballon' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Air-ballon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Air-ballon.svg',
								'height' => 128
							)
						),
						'Alarm-clock' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Alarm-clock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Alarm-clock.svg',
								'height' => 128
							)
						),
						'Armchair' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Armchair.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Armchair.svg',
								'height' => 128
							)
						),
						'Bag-chair' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bag-chair.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bag-chair.svg',
								'height' => 128
							)
						),
						'Bath' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bath.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bath.svg',
								'height' => 128
							)
						),
						'Bed' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bed.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bed.svg',
								'height' => 128
							)
						),
						'Book-open' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Book-open.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Book-open.svg',
								'height' => 128
							)
						),
						'Book' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Book.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Book.svg',
								'height' => 128
							)
						),
						'Box' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Box.svg',
								'height' => 128
							)
						),
						'Broom' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Broom.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Broom.svg',
								'height' => 128
							)
						),
						'Building' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Building.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Building.svg',
								'height' => 128
							)
						),
						'Bulb 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bulb 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bulb 1.svg',
								'height' => 128
							)
						),
						'Bulb 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bulb 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bulb 2.svg',
								'height' => 128
							)
						),
						'Chair 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chair 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chair 1.svg',
								'height' => 128
							)
						),
						'Chair 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chair 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chair 2.svg',
								'height' => 128
							)
						),
						'Clock' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Clock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Clock.svg',
								'height' => 128
							)
						),
						'Commode 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Commode 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Commode 1.svg',
								'height' => 128
							)
						),
						'Commode 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Commode 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Commode 2.svg',
								'height' => 128
							)
						),
						'Couch' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Couch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Couch.svg',
								'height' => 128
							)
						),
						'Cupboard' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cupboard.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cupboard.svg',
								'height' => 128
							)
						),
						'Curtains' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Curtains.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Curtains.svg',
								'height' => 128
							)
						),
						'Deer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Deer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Deer.svg',
								'height' => 128
							)
						),
						'Door-open' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Door-open.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Door-open.svg',
								'height' => 128
							)
						),
						'Earth' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Earth.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Earth.svg',
								'height' => 128
							)
						),
						'Fireplace' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fireplace.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fireplace.svg',
								'height' => 128
							)
						),
						'Flashlight' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Flashlight.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Flashlight.svg',
								'height' => 128
							)
						),
						'Flower 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Flower 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Flower 1.svg',
								'height' => 128
							)
						),
						'Flower 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Flower 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Flower 2.svg',
								'height' => 128
							)
						),
						'Flower 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Flower 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Flower 3.svg',
								'height' => 128
							)
						),
						'Globe' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Globe.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Globe.svg',
								'height' => 128
							)
						),
						'Home-heart' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Home-heart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Home-heart.svg',
								'height' => 128
							)
						),
						'Home' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Home.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Home.svg',
								'height' => 128
							)
						),
						'Key' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Key.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Key.svg',
								'height' => 128
							)
						),
						'Ladder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Ladder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Ladder.svg',
								'height' => 128
							)
						),
						'Lamp 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Lamp 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Lamp 1.svg',
								'height' => 128
							)
						),
						'Lamp 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Lamp 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Lamp 2.svg',
								'height' => 128
							)
						),
						'Library' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Library.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Library.svg',
								'height' => 128
							)
						),
						'Mailbox' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mailbox.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mailbox.svg',
								'height' => 128
							)
						),
						'Mirror' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mirror.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mirror.svg',
								'height' => 128
							)
						),
						'Picture' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Picture.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Picture.svg',
								'height' => 128
							)
						),
						'Ruller' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Ruller.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Ruller.svg',
								'height' => 128
							)
						),
						'Stairs' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Stairs.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Stairs.svg',
								'height' => 128
							)
						),
						'Timer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Timer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Timer.svg',
								'height' => 128
							)
						),
						'Toilet' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Toilet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Toilet.svg',
								'height' => 128
							)
						),
						'Towel' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Towel.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Towel.svg',
								'height' => 128
							)
						),
						'Trash' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Trash.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Trash.svg',
								'height' => 128
							)
						),
						'Water-mixer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Water-mixer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Water-mixer.svg',
								'height' => 128
							)
						),
						'Weight 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Weight 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Weight 1.svg',
								'height' => 128
							)
						),
						'Weight 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Weight 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Weight 2.svg',
								'height' => 128
							)
						),
						'Wood 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wood 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wood 1.svg',
								'height' => 128
							)
						),
						'Wood 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wood 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wood 2.svg',
								'height' => 128
							)
						),
						'Wood-horse' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wood-horse.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wood-horse.svg',
								'height' => 128
							)
						),
						'Layout-3d' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-3d.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-3d.svg',
								'height' => 128
							)
						),
						'Layout-4-blocks' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-4-blocks.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-4-blocks.svg',
								'height' => 128
							)
						),
						'Layout-arrange' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-arrange.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-arrange.svg',
								'height' => 128
							)
						),
						'Layout-grid' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-grid.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-grid.svg',
								'height' => 128
							)
						),
						'Layout-horizontal' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-horizontal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-horizontal.svg',
								'height' => 128
							)
						),
						'Layout-left-panel-1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-left-panel-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-left-panel-1.svg',
								'height' => 128
							)
						),
						'Layout-left-panel-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-left-panel-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-left-panel-2.svg',
								'height' => 128
							)
						),
						'Layout-right-panel-1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-right-panel-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-right-panel-1.svg',
								'height' => 128
							)
						),
						'Layout-right-panel-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-right-panel-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-right-panel-2.svg',
								'height' => 128
							)
						),
						'Layout-top-panel-1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-1.svg',
								'height' => 128
							)
						),
						'Layout-top-panel-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-2.svg',
								'height' => 128
							)
						),
						'Layout-top-panel-3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-3.svg',
								'height' => 128
							)
						),
						'Layout-top-panel-4' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-4.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-4.svg',
								'height' => 128
							)
						),
						'Layout-top-panel-5' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-5.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-5.svg',
								'height' => 128
							)
						),
						'Layout-top-panel-6' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-6.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-top-panel-6.svg',
								'height' => 128
							)
						),
						'Layout-vertical' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Layout-vertical.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Layout-vertical.svg',
								'height' => 128
							)
						),
						'Compass' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Compass.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Compass.svg',
								'height' => 128
							)
						),
						'Direction 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Direction 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Direction 1.svg',
								'height' => 128
							)
						),
						'Direction 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Direction 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Direction 2.svg',
								'height' => 128
							)
						),
						'Location-arrow' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Location-arrow.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Location-arrow.svg',
								'height' => 128
							)
						),
						'Marker 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Marker 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Marker 1.svg',
								'height' => 128
							)
						),
						'Marker 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Marker 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Marker 2.svg',
								'height' => 128
							)
						),
						'Position' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Position.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Position.svg',
								'height' => 128
							)
						),
						'Add-music' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Add-music.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Add-music.svg',
								'height' => 128
							)
						),
						'Airplay-video' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Airplay-video.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Airplay-video.svg',
								'height' => 128
							)
						),
						'Airplay' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Airplay.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Airplay.svg',
								'height' => 128
							)
						),
						'Back' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Back.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Back.svg',
								'height' => 128
							)
						),
						'Backward' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Backward.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Backward.svg',
								'height' => 128
							)
						),
						'CD' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/CD.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/CD.svg',
								'height' => 128
							)
						),
						'DVD' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/DVD.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/DVD.svg',
								'height' => 128
							)
						),
						'Eject' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Eject.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Eject.svg',
								'height' => 128
							)
						),
						'Equalizer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Equalizer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Equalizer.svg',
								'height' => 128
							)
						),
						'Forward' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Forward.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Forward.svg',
								'height' => 128
							)
						),
						'Media-library 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Media-library 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Media-library 1.svg',
								'height' => 128
							)
						),
						'Media-library 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Media-library 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Media-library 2.svg',
								'height' => 128
							)
						),
						'Media-library 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Media-library 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Media-library 3.svg',
								'height' => 128
							)
						),
						'Movie-Lane  2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Movie-Lane  2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Movie-Lane  2.svg',
								'height' => 128
							)
						),
						'Movie-lane 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Movie-lane 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Movie-lane 1.svg',
								'height' => 128
							)
						),
						'Music-cloud' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Music-cloud.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Music-cloud.svg',
								'height' => 128
							)
						),
						'Music-note' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Music-note.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Music-note.svg',
								'height' => 128
							)
						),
						'Music' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Music.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Music.svg',
								'height' => 128
							)
						),
						'Mute' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Mute.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Mute.svg',
								'height' => 128
							)
						),
						'Next' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Next.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Next.svg',
								'height' => 128
							)
						),
						'Pause' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pause.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pause.svg',
								'height' => 128
							)
						),
						'Play' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Play.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Play.svg',
								'height' => 128
							)
						),
						'Playlist 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Playlist 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Playlist 1.svg',
								'height' => 128
							)
						),
						'Playlist 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Playlist 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Playlist 2.svg',
								'height' => 128
							)
						),
						'Rec' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Rec.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Rec.svg',
								'height' => 128
							)
						),
						'Repeat-one' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Repeat-one.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Repeat-one.svg',
								'height' => 128
							)
						),
						'Repeat' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Repeat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Repeat.svg',
								'height' => 128
							)
						),
						'Shuffle' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shuffle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shuffle.svg',
								'height' => 128
							)
						),
						'Volume-down' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Volume-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Volume-down.svg',
								'height' => 128
							)
						),
						'Volume-full' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Volume-full.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Volume-full.svg',
								'height' => 128
							)
						),
						'Volume-half' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Volume-half.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Volume-half.svg',
								'height' => 128
							)
						),
						'Volume-up' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Volume-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Volume-up.svg',
								'height' => 128
							)
						),
						'Vynil' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Vynil.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Vynil.svg',
								'height' => 128
							)
						),
						'Youtube' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Youtube.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Youtube.svg',
								'height' => 128
							)
						),
						'Angle-double-down' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle-double-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle-double-down.svg',
								'height' => 128
							)
						),
						'Angle-double-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle-double-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle-double-left.svg',
								'height' => 128
							)
						),
						'Angle-double-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle-double-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle-double-right.svg',
								'height' => 128
							)
						),
						'Angle-double-up' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle-double-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle-double-up.svg',
								'height' => 128
							)
						),
						'Angle-down' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle-down.svg',
								'height' => 128
							)
						),
						'Angle-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle-left.svg',
								'height' => 128
							)
						),
						'Angle-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle-right.svg',
								'height' => 128
							)
						),
						'Angle-up' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle-up.svg',
								'height' => 128
							)
						),
						'Arrow-down' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-down.svg',
								'height' => 128
							)
						),
						'Arrow-from-bottom' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-from-bottom.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-from-bottom.svg',
								'height' => 128
							)
						),
						'Arrow-from-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-from-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-from-left.svg',
								'height' => 128
							)
						),
						'Arrow-from-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-from-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-from-right.svg',
								'height' => 128
							)
						),
						'Arrow-from-top' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-from-top.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-from-top.svg',
								'height' => 128
							)
						),
						'Arrow-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-left.svg',
								'height' => 128
							)
						),
						'Arrow-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-right.svg',
								'height' => 128
							)
						),
						'Arrow-to-bottom' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-to-bottom.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-to-bottom.svg',
								'height' => 128
							)
						),
						'Arrow-to-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-to-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-to-left.svg',
								'height' => 128
							)
						),
						'Arrow-to-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-to-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-to-right.svg',
								'height' => 128
							)
						),
						'Arrow-to-up' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-to-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-to-up.svg',
								'height' => 128
							)
						),
						'Arrow-up' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrow-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrow-up.svg',
								'height' => 128
							)
						),
						'Arrows-h' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrows-h.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrows-h.svg',
								'height' => 128
							)
						),
						'Arrows-v' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Arrows-v.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Arrows-v.svg',
								'height' => 128
							)
						),
						'Check' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Check.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Check.svg',
								'height' => 128
							)
						),
						'Close' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Close.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Close.svg',
								'height' => 128
							)
						),
						'Double-check' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Double-check.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Double-check.svg',
								'height' => 128
							)
						),
						'Down-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Down-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Down-2.svg',
								'height' => 128
							)
						),
						'Down-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Down-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Down-left.svg',
								'height' => 128
							)
						),
						'Down-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Down-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Down-right.svg',
								'height' => 128
							)
						),
						'Exchange' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Exchange.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Exchange.svg',
								'height' => 128
							)
						),
						'Left 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Left 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Left 3.svg',
								'height' => 128
							)
						),
						'Left-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Left-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Left-2.svg',
								'height' => 128
							)
						),
						'Minus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Minus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Minus.svg',
								'height' => 128
							)
						),
						'Plus' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Plus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Plus.svg',
								'height' => 128
							)
						),
						'Right 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Right 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Right 3.svg',
								'height' => 128
							)
						),
						'Right-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Right-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Right-2.svg',
								'height' => 128
							)
						),
						'Route' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Route.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Route.svg',
								'height' => 128
							)
						),
						'Sign-in' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sign-in.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sign-in.svg',
								'height' => 128
							)
						),
						'Sign-out' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sign-out.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sign-out.svg',
								'height' => 128
							)
						),
						'Up-2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Up-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Up-2.svg',
								'height' => 128
							)
						),
						'Up-down' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Up-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Up-down.svg',
								'height' => 128
							)
						),
						'Up-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Up-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Up-left.svg',
								'height' => 128
							)
						),
						'Up-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Up-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Up-right.svg',
								'height' => 128
							)
						),
						'Waiting' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Waiting.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Waiting.svg',
								'height' => 128
							)
						),
						'ATM' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/ATM.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/ATM.svg',
								'height' => 128
							)
						),
						'Bag 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bag 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bag 1.svg',
								'height' => 128
							)
						),
						'Bag 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bag 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bag 2.svg',
								'height' => 128
							)
						),
						'Barcode-read' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Barcode-read.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Barcode-read.svg',
								'height' => 128
							)
						),
						'Barcode-scan' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Barcode-scan.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Barcode-scan.svg',
								'height' => 128
							)
						),
						'Barcode' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Barcode.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Barcode.svg',
								'height' => 128
							)
						),
						'Bitcoin' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bitcoin.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bitcoin.svg',
								'height' => 128
							)
						),
						'Box 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Box 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Box 1.svg',
								'height' => 128
							)
						),
						'Box 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Box 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Box 2.svg',
								'height' => 128
							)
						),
						'Box 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Box 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Box 3.svg',
								'height' => 128
							)
						),
						'Calculator' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Calculator.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Calculator.svg',
								'height' => 128
							)
						),
						'Cart 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cart 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cart 1.svg',
								'height' => 128
							)
						),
						'Cart 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cart 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cart 2.svg',
								'height' => 128
							)
						),
						'Cart 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cart 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cart 3.svg',
								'height' => 128
							)
						),
						'Chart-bar 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chart-bar 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chart-bar 1.svg',
								'height' => 128
							)
						),
						'Chart-bar 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chart-bar 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chart-bar 2.svg',
								'height' => 128
							)
						),
						'Chart-bar 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chart-bar 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chart-bar 3.svg',
								'height' => 128
							)
						),
						'Chart-line 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chart-line 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chart-line 1.svg',
								'height' => 128
							)
						),
						'Chart-line 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chart-line 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chart-line 2.svg',
								'height' => 128
							)
						),
						'Chart-pie' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Chart-pie.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Chart-pie.svg',
								'height' => 128
							)
						),
						'Credit-card' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Credit-card.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Credit-card.svg',
								'height' => 128
							)
						),
						'Dollar' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Dollar.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Dollar.svg',
								'height' => 128
							)
						),
						'Euro' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Euro.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Euro.svg',
								'height' => 128
							)
						),
						'Gift' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Gift.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Gift.svg',
								'height' => 128
							)
						),
						'Loader' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Loader.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Loader.svg',
								'height' => 128
							)
						),
						'MC' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/MC.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/MC.svg',
								'height' => 128
							)
						),
						'Money' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Money.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Money.svg',
								'height' => 128
							)
						),
						'Pound' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pound.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pound.svg',
								'height' => 128
							)
						),
						'Price  1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Price  1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Price  1.svg',
								'height' => 128
							)
						),
						'Price  2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Price  2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Price  2.svg',
								'height' => 128
							)
						),
						'Rouble' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Rouble.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Rouble.svg',
								'height' => 128
							)
						),
						'Safe' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Safe.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Safe.svg',
								'height' => 128
							)
						),
						'Sale 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sale 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sale 1.svg',
								'height' => 128
							)
						),
						'Sale 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sale 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sale 2.svg',
								'height' => 128
							)
						),
						'Settings' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Settings.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Settings.svg',
								'height' => 128
							)
						),
						'Sort 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sort 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sort 1.svg',
								'height' => 128
							)
						),
						'Sort 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sort 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sort 2.svg',
								'height' => 128
							)
						),
						'Sort 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sort 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sort 3.svg',
								'height' => 128
							)
						),
						'Ticket' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Ticket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Ticket.svg',
								'height' => 128
							)
						),
						'Wallet 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wallet 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wallet 2.svg',
								'height' => 128
							)
						),
						'Wallet 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wallet 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wallet 3.svg',
								'height' => 128
							)
						),
						'Wallet' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wallet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wallet.svg',
								'height' => 128
							)
						),
						'Align-auto' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Align-auto.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Align-auto.svg',
								'height' => 128
							)
						),
						'Align-center' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Align-center.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Align-center.svg',
								'height' => 128
							)
						),
						'Align-justify' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Align-justify.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Align-justify.svg',
								'height' => 128
							)
						),
						'Align-left' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Align-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Align-left.svg',
								'height' => 128
							)
						),
						'Align-right' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Align-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Align-right.svg',
								'height' => 128
							)
						),
						'Article' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Article.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Article.svg',
								'height' => 128
							)
						),
						'Bold' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bold.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bold.svg',
								'height' => 128
							)
						),
						'Bullet-list' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Bullet-list.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Bullet-list.svg',
								'height' => 128
							)
						),
						'Code' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Code.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Code.svg',
								'height' => 128
							)
						),
						'Edit-text' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Edit-text.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Edit-text.svg',
								'height' => 128
							)
						),
						'Filter' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Filter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Filter.svg',
								'height' => 128
							)
						),
						'Font' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Font.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Font.svg',
								'height' => 128
							)
						),
						'H1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/H1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/H1.svg',
								'height' => 128
							)
						),
						'H2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/H2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/H2.svg',
								'height' => 128
							)
						),
						'Itallic' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Itallic.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Itallic.svg',
								'height' => 128
							)
						),
						'Menu' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Menu.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Menu.svg',
								'height' => 128
							)
						),
						'Paragraph' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Paragraph.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Paragraph.svg',
								'height' => 128
							)
						),
						'Quote 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Quote 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Quote 1.svg',
								'height' => 128
							)
						),
						'Quote 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Quote 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Quote 2.svg',
								'height' => 128
							)
						),
						'Redo' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Redo.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Redo.svg',
								'height' => 128
							)
						),
						'Strikethrough' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Strikethrough.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Strikethrough.svg',
								'height' => 128
							)
						),
						'Text-height' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Text-height.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Text-height.svg',
								'height' => 128
							)
						),
						'Text-width' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Text-width.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Text-width.svg',
								'height' => 128
							)
						),
						'Text' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Text.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Text.svg',
								'height' => 128
							)
						),
						'Underline' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Underline.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Underline.svg',
								'height' => 128
							)
						),
						'Undo' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Undo.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Undo.svg',
								'height' => 128
							)
						),
						'Angle Grinder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Angle Grinder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Angle Grinder.svg',
								'height' => 128
							)
						),
						'Axe' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Axe.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Axe.svg',
								'height' => 128
							)
						),
						'Brush' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Brush.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Brush.svg',
								'height' => 128
							)
						),
						'Compass' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Compass.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Compass.svg',
								'height' => 128
							)
						),
						'Hummer 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Hummer 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Hummer 2.svg',
								'height' => 128
							)
						),
						'Hummer' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Hummer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Hummer.svg',
								'height' => 128
							)
						),
						'Pantone' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Pantone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Pantone.svg',
								'height' => 128
							)
						),
						'Road-Cone' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Road-Cone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Road-Cone.svg',
								'height' => 128
							)
						),
						'Roller' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Roller.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Roller.svg',
								'height' => 128
							)
						),
						'Roulette' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Roulette.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Roulette.svg',
								'height' => 128
							)
						),
						'Screwdriver' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Screwdriver.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Screwdriver.svg',
								'height' => 128
							)
						),
						'Shovel' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Shovel.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Shovel.svg',
								'height' => 128
							)
						),
						'Spatula' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Spatula.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Spatula.svg',
								'height' => 128
							)
						),
						'Swiss-knife' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Swiss-knife.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Swiss-knife.svg',
								'height' => 128
							)
						),
						'Tools' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Tools.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Tools.svg',
								'height' => 128
							)
						),
						'Celcium' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Celcium.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Celcium.svg',
								'height' => 128
							)
						),
						'Cloud 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloud 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloud 1.svg',
								'height' => 128
							)
						),
						'Cloud 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloud 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloud 2.svg',
								'height' => 128
							)
						),
						'Cloud-fog' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloud-fog.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloud-fog.svg',
								'height' => 128
							)
						),
						'Cloud-sun' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloud-sun.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloud-sun.svg',
								'height' => 128
							)
						),
						'Cloud-wind' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloud-wind.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloud-wind.svg',
								'height' => 128
							)
						),
						'Cloudy-night' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloudy-night.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloudy-night.svg',
								'height' => 128
							)
						),
						'Cloudy' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Cloudy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Cloudy.svg',
								'height' => 128
							)
						),
						'Day-rain' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Day-rain.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Day-rain.svg',
								'height' => 128
							)
						),
						'Fahrenheit' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fahrenheit.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fahrenheit.svg',
								'height' => 128
							)
						),
						'Fog' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Fog.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Fog.svg',
								'height' => 128
							)
						),
						'Moon' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Moon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Moon.svg',
								'height' => 128
							)
						),
						'Night-fog' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Night-fog.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Night-fog.svg',
								'height' => 128
							)
						),
						'Night-rain' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Night-rain.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Night-rain.svg',
								'height' => 128
							)
						),
						'Rain 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Rain 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Rain 1.svg',
								'height' => 128
							)
						),
						'Rain 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Rain 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Rain 2.svg',
								'height' => 128
							)
						),
						'Rain 5' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Rain 5.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Rain 5.svg',
								'height' => 128
							)
						),
						'Rainbow' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Rainbow.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Rainbow.svg',
								'height' => 128
							)
						),
						'Snow 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Snow 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Snow 1.svg',
								'height' => 128
							)
						),
						'Snow 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Snow 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Snow 2.svg',
								'height' => 128
							)
						),
						'Snow 3' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Snow 3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Snow 3.svg',
								'height' => 128
							)
						),
						'Snow' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Snow.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Snow.svg',
								'height' => 128
							)
						),
						'Storm' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Storm.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Storm.svg',
								'height' => 128
							)
						),
						'Sun-fog' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sun-fog.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sun-fog.svg',
								'height' => 128
							)
						),
						'Sun' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Sun.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Sun.svg',
								'height' => 128
							)
						),
						'Suset 1' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Suset 1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Suset 1.svg',
								'height' => 128
							)
						),
						'Suset 2' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Suset 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Suset 2.svg',
								'height' => 128
							)
						),
						'Temperature-empty' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Temperature-empty.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Temperature-empty.svg',
								'height' => 128
							)
						),
						'Temperature-full' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Temperature-full.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Temperature-full.svg',
								'height' => 128
							)
						),
						'Temperature-half' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Temperature-half.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Temperature-half.svg',
								'height' => 128
							)
						),
						'Thunder-night' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Thunder-night.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Thunder-night.svg',
								'height' => 128
							)
						),
						'Thunder' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Thunder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Thunder.svg',
								'height' => 128
							)
						),
						'Umbrella' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Umbrella.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Umbrella.svg',
								'height' => 128
							)
						),
						'Wind' => array(
							'small' => array(
								'src' => $uri . 'duotone-icons/Wind.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'duotone-icons/Wind.svg',
								'height' => 128
							)
						),
					)
				)
			),
			'pe-icon-7-stroke' => array(
				'icon' => array(
					'type' => 'image-picker',
					'label' => __('Icon', 'fw'),
					'choices' => array(
						'add-user' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/add-user.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/add-user.svg',
								'height' => 128
							)
						),
						'airplay' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/airplay.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/airplay.svg',
								'height' => 128
							)
						),
						'alarm' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/alarm.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/alarm.svg',
								'height' => 128
							)
						),
						'album' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/album.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/album.svg',
								'height' => 128
							)
						),
						'albums' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/albums.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/albums.svg',
								'height' => 128
							)
						),
						'anchor' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/anchor.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/anchor.svg',
								'height' => 128
							)
						),
						'angle-down-circle' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-down-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-down-circle.svg',
								'height' => 128
							)
						),
						'angle-down' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-down.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-down.svg',
								'height' => 128
							)
						),
						'angle-left-circle' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-left-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-left-circle.svg',
								'height' => 128
							)
						),
						'angle-left' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-left.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-left.svg',
								'height' => 128
							)
						),
						'angle-right-circle' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-right-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-right-circle.svg',
								'height' => 128
							)
						),
						'angle-right' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-right.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-right.svg',
								'height' => 128
							)
						),
						'angle-up-circle' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-up-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-up-circle.svg',
								'height' => 128
							)
						),
						'angle-up' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-up.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/angle-up.svg',
								'height' => 128
							)
						),
						'arc' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/arc.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/arc.svg',
								'height' => 128
							)
						),
						'attention' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/attention.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/attention.svg',
								'height' => 128
							)
						),
						'back-2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/back-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/back-2.svg',
								'height' => 128
							)
						),
						'back' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/back.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/back.svg',
								'height' => 128
							)
						),
						'ball' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/ball.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/ball.svg',
								'height' => 128
							)
						),
						'bandaid' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/bandaid.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/bandaid.svg',
								'height' => 128
							)
						),
						'battery' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/battery.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/battery.svg',
								'height' => 128
							)
						),
						'bell' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/bell.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/bell.svg',
								'height' => 128
							)
						),
						'bicycle' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/bicycle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/bicycle.svg',
								'height' => 128
							)
						),
						'bluetooth' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/bluetooth.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/bluetooth.svg',
								'height' => 128
							)
						),
						'bookmarks' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/bookmarks.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/bookmarks.svg',
								'height' => 128
							)
						),
						'bottom-arrow' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/bottom-arrow.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/bottom-arrow.svg',
								'height' => 128
							)
						),
						'box1' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/box1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/box1.svg',
								'height' => 128
							)
						),
						'box2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/box2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/box2.svg',
								'height' => 128
							)
						),
						'browser' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/browser.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/browser.svg',
								'height' => 128
							)
						),
						'calculator' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/calculator.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/calculator.svg',
								'height' => 128
							)
						),
						'call' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/call.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/call.svg',
								'height' => 128
							)
						),
						'camera' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/camera.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/camera.svg',
								'height' => 128
							)
						),
						'car' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/car.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/car.svg',
								'height' => 128
							)
						),
						'cart' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/cart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/cart.svg',
								'height' => 128
							)
						),
						'cash' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/cash.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/cash.svg',
								'height' => 128
							)
						),
						'chat' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/chat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/chat.svg',
								'height' => 128
							)
						),
						'check' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/check.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/check.svg',
								'height' => 128
							)
						),
						'clock' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/clock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/clock.svg',
								'height' => 128
							)
						),
						'close-circle' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/close-circle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/close-circle.svg',
								'height' => 128
							)
						),
						'close' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/close.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/close.svg',
								'height' => 128
							)
						),
						'cloud-download' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/cloud-download.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/cloud-download.svg',
								'height' => 128
							)
						),
						'cloud-upload' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/cloud-upload.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/cloud-upload.svg',
								'height' => 128
							)
						),
						'cloud' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/cloud.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/cloud.svg',
								'height' => 128
							)
						),
						'coffee' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/coffee.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/coffee.svg',
								'height' => 128
							)
						),
						'comment' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/comment.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/comment.svg',
								'height' => 128
							)
						),
						'compass' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/compass.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/compass.svg',
								'height' => 128
							)
						),
						'config' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/config.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/config.svg',
								'height' => 128
							)
						),
						'copy-file' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/copy-file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/copy-file.svg',
								'height' => 128
							)
						),
						'credit' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/credit.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/credit.svg',
								'height' => 128
							)
						),
						'crop' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/crop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/crop.svg',
								'height' => 128
							)
						),
						'culture' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/culture.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/culture.svg',
								'height' => 128
							)
						),
						'cup' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/cup.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/cup.svg',
								'height' => 128
							)
						),
						'date' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/date.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/date.svg',
								'height' => 128
							)
						),
						'delete-user' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/delete-user.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/delete-user.svg',
								'height' => 128
							)
						),
						'diamond' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/diamond.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/diamond.svg',
								'height' => 128
							)
						),
						'disk' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/disk.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/disk.svg',
								'height' => 128
							)
						),
						'diskette' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/diskette.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/diskette.svg',
								'height' => 128
							)
						),
						'display1' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/display1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/display1.svg',
								'height' => 128
							)
						),
						'display2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/display2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/display2.svg',
								'height' => 128
							)
						),
						'door-lock' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/door-lock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/door-lock.svg',
								'height' => 128
							)
						),
						'download' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/download.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/download.svg',
								'height' => 128
							)
						),
						'drawer' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/drawer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/drawer.svg',
								'height' => 128
							)
						),
						'drop' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/drop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/drop.svg',
								'height' => 128
							)
						),
						'edit' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/edit.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/edit.svg',
								'height' => 128
							)
						),
						'exapnd2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/exapnd2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/exapnd2.svg',
								'height' => 128
							)
						),
						'expand1' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/expand1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/expand1.svg',
								'height' => 128
							)
						),
						'eyedropper' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/eyedropper.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/eyedropper.svg',
								'height' => 128
							)
						),
						'female' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/female.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/female.svg',
								'height' => 128
							)
						),
						'file' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/file.svg',
								'height' => 128
							)
						),
						'film' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/film.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/film.svg',
								'height' => 128
							)
						),
						'filter' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/filter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/filter.svg',
								'height' => 128
							)
						),
						'flag' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/flag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/flag.svg',
								'height' => 128
							)
						),
						'folder' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/folder.svg',
								'height' => 128
							)
						),
						'gift' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/gift.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/gift.svg',
								'height' => 128
							)
						),
						'glasses' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/glasses.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/glasses.svg',
								'height' => 128
							)
						),
						'gleam' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/gleam.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/gleam.svg',
								'height' => 128
							)
						),
						'global' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/global.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/global.svg',
								'height' => 128
							)
						),
						'graph' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/graph.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/graph.svg',
								'height' => 128
							)
						),
						'graph1' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/graph1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/graph1.svg',
								'height' => 128
							)
						),
						'graph2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/graph2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/graph2.svg',
								'height' => 128
							)
						),
						'graph3' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/graph3.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/graph3.svg',
								'height' => 128
							)
						),
						'gym' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/gym.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/gym.svg',
								'height' => 128
							)
						),
						'hammer' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/hammer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/hammer.svg',
								'height' => 128
							)
						),
						'headphones' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/headphones.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/headphones.svg',
								'height' => 128
							)
						),
						'helm' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/helm.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/helm.svg',
								'height' => 128
							)
						),
						'help1' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/help1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/help1.svg',
								'height' => 128
							)
						),
						'help2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/help2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/help2.svg',
								'height' => 128
							)
						),
						'home' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/home.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/home.svg',
								'height' => 128
							)
						),
						'hourglass' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/hourglass.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/hourglass.svg',
								'height' => 128
							)
						),
						'id' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/id.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/id.svg',
								'height' => 128
							)
						),
						'info' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/info.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/info.svg',
								'height' => 128
							)
						),
						'joy' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/joy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/joy.svg',
								'height' => 128
							)
						),
						'junk' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/junk.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/junk.svg',
								'height' => 128
							)
						),
						'key' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/key.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/key.svg',
								'height' => 128
							)
						),
						'keypad' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/keypad.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/keypad.svg',
								'height' => 128
							)
						),
						'leaf' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/leaf.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/leaf.svg',
								'height' => 128
							)
						),
						'left-arrow' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/left-arrow.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/left-arrow.svg',
								'height' => 128
							)
						),
						'less' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/less.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/less.svg',
								'height' => 128
							)
						),
						'light' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/light.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/light.svg',
								'height' => 128
							)
						),
						'like' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/like.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/like.svg',
								'height' => 128
							)
						),
						'like2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/like2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/like2.svg',
								'height' => 128
							)
						),
						'link' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/link.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/link.svg',
								'height' => 128
							)
						),
						'lintern' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/lintern.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/lintern.svg',
								'height' => 128
							)
						),
						'lock' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/lock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/lock.svg',
								'height' => 128
							)
						),
						'look' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/look.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/look.svg',
								'height' => 128
							)
						),
						'loop' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/loop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/loop.svg',
								'height' => 128
							)
						),
						'magic-wand' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/magic-wand.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/magic-wand.svg',
								'height' => 128
							)
						),
						'magnet' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/magnet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/magnet.svg',
								'height' => 128
							)
						),
						'mail-open-file' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/mail-open-file.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/mail-open-file.svg',
								'height' => 128
							)
						),
						'mail-open' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/mail-open.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/mail-open.svg',
								'height' => 128
							)
						),
						'mail' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/mail.svg',
								'height' => 128
							)
						),
						'male' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/male.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/male.svg',
								'height' => 128
							)
						),
						'map-2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/map-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/map-2.svg',
								'height' => 128
							)
						),
						'map-marker' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/map-marker.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/map-marker.svg',
								'height' => 128
							)
						),
						'map' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/map.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/map.svg',
								'height' => 128
							)
						),
						'medal' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/medal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/medal.svg',
								'height' => 128
							)
						),
						'menu' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/menu.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/menu.svg',
								'height' => 128
							)
						),
						'micro' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/micro.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/micro.svg',
								'height' => 128
							)
						),
						'monitor' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/monitor.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/monitor.svg',
								'height' => 128
							)
						),
						'moon' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/moon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/moon.svg',
								'height' => 128
							)
						),
						'more' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/more.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/more.svg',
								'height' => 128
							)
						),
						'mouse' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/mouse.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/mouse.svg',
								'height' => 128
							)
						),
						'music' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/music.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/music.svg',
								'height' => 128
							)
						),
						'musiclist' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/musiclist.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/musiclist.svg',
								'height' => 128
							)
						),
						'mute' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/mute.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/mute.svg',
								'height' => 128
							)
						),
						'network' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/network.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/network.svg',
								'height' => 128
							)
						),
						'news-paper' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/news-paper.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/news-paper.svg',
								'height' => 128
							)
						),
						'next-2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/next-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/next-2.svg',
								'height' => 128
							)
						),
						'next' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/next.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/next.svg',
								'height' => 128
							)
						),
						'note' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/note.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/note.svg',
								'height' => 128
							)
						),
						'note2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/note2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/note2.svg',
								'height' => 128
							)
						),
						'notebook' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/notebook.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/notebook.svg',
								'height' => 128
							)
						),
						'paint-bucket' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/paint-bucket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/paint-bucket.svg',
								'height' => 128
							)
						),
						'paint' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/paint.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/paint.svg',
								'height' => 128
							)
						),
						'paper-plane' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/paper-plane.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/paper-plane.svg',
								'height' => 128
							)
						),
						'paperclip' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/paperclip.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/paperclip.svg',
								'height' => 128
							)
						),
						'pen' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/pen.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/pen.svg',
								'height' => 128
							)
						),
						'pendrive' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/pendrive.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/pendrive.svg',
								'height' => 128
							)
						),
						'phone' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/phone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/phone.svg',
								'height' => 128
							)
						),
						'photo-gallery' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/photo-gallery.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/photo-gallery.svg',
								'height' => 128
							)
						),
						'photo' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/photo.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/photo.svg',
								'height' => 128
							)
						),
						'piggy' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/piggy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/piggy.svg',
								'height' => 128
							)
						),
						'pin' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/pin.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/pin.svg',
								'height' => 128
							)
						),
						'plane' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/plane.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/plane.svg',
								'height' => 128
							)
						),
						'play' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/play.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/play.svg',
								'height' => 128
							)
						),
						'plug' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/plug.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/plug.svg',
								'height' => 128
							)
						),
						'plugin' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/plugin.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/plugin.svg',
								'height' => 128
							)
						),
						'plus' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/plus.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/plus.svg',
								'height' => 128
							)
						),
						'portfolio' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/portfolio.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/portfolio.svg',
								'height' => 128
							)
						),
						'power' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/power.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/power.svg',
								'height' => 128
							)
						),
						'prev' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/prev.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/prev.svg',
								'height' => 128
							)
						),
						'print' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/print.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/print.svg',
								'height' => 128
							)
						),
						'radio' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/radio.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/radio.svg',
								'height' => 128
							)
						),
						'refresh-2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/refresh-2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/refresh-2.svg',
								'height' => 128
							)
						),
						'refresh-cloud' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/refresh-cloud.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/refresh-cloud.svg',
								'height' => 128
							)
						),
						'refresh' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/refresh.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/refresh.svg',
								'height' => 128
							)
						),
						'repeat' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/repeat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/repeat.svg',
								'height' => 128
							)
						),
						'ribbon' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/ribbon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/ribbon.svg',
								'height' => 128
							)
						),
						'right-arrow' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/right-arrow.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/right-arrow.svg',
								'height' => 128
							)
						),
						'rocket' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/rocket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/rocket.svg',
								'height' => 128
							)
						),
						'safe' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/safe.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/safe.svg',
								'height' => 128
							)
						),
						'science' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/science.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/science.svg',
								'height' => 128
							)
						),
						'scissors' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/scissors.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/scissors.svg',
								'height' => 128
							)
						),
						'search' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/search.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/search.svg',
								'height' => 128
							)
						),
						'server' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/server.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/server.svg',
								'height' => 128
							)
						),
						'settings' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/settings.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/settings.svg',
								'height' => 128
							)
						),
						'share' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/share.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/share.svg',
								'height' => 128
							)
						),
						'shield' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/shield.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/shield.svg',
								'height' => 128
							)
						),
						'shopbag' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/shopbag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/shopbag.svg',
								'height' => 128
							)
						),
						'shuffle' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/shuffle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/shuffle.svg',
								'height' => 128
							)
						),
						'signal' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/signal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/signal.svg',
								'height' => 128
							)
						),
						'smile' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/smile.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/smile.svg',
								'height' => 128
							)
						),
						'speaker' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/speaker.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/speaker.svg',
								'height' => 128
							)
						),
						'star' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/star.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/star.svg',
								'height' => 128
							)
						),
						'stopwatch' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/stopwatch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/stopwatch.svg',
								'height' => 128
							)
						),
						'study' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/study.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/study.svg',
								'height' => 128
							)
						),
						'sun' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/sun.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/sun.svg',
								'height' => 128
							)
						),
						'switch' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/switch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/switch.svg',
								'height' => 128
							)
						),
						'target' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/target.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/target.svg',
								'height' => 128
							)
						),
						'ticket' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/ticket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/ticket.svg',
								'height' => 128
							)
						),
						'timer' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/timer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/timer.svg',
								'height' => 128
							)
						),
						'tools' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/tools.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/tools.svg',
								'height' => 128
							)
						),
						'trash' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/trash.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/trash.svg',
								'height' => 128
							)
						),
						'umbrella' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/umbrella.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/umbrella.svg',
								'height' => 128
							)
						),
						'unlock' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/unlock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/unlock.svg',
								'height' => 128
							)
						),
						'up-arrow' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/up-arrow.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/up-arrow.svg',
								'height' => 128
							)
						),
						'upload' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/upload.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/upload.svg',
								'height' => 128
							)
						),
						'usb' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/usb.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/usb.svg',
								'height' => 128
							)
						),
						'user-female' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/user-female.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/user-female.svg',
								'height' => 128
							)
						),
						'user' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/user.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/user.svg',
								'height' => 128
							)
						),
						'users' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/users.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/users.svg',
								'height' => 128
							)
						),
						'vector' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/vector.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/vector.svg',
								'height' => 128
							)
						),
						'video' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/video.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/video.svg',
								'height' => 128
							)
						),
						'voicemail' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/voicemail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/voicemail.svg',
								'height' => 128
							)
						),
						'volume' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/volume.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/volume.svg',
								'height' => 128
							)
						),
						'volume1' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/volume1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/volume1.svg',
								'height' => 128
							)
						),
						'volume2' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/volume2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/volume2.svg',
								'height' => 128
							)
						),
						'wallet' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/wallet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/wallet.svg',
								'height' => 128
							)
						),
						'way' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/way.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/way.svg',
								'height' => 128
							)
						),
						'wine' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/wine.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/wine.svg',
								'height' => 128
							)
						),
						'world' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/world.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/world.svg',
								'height' => 128
							)
						),
						'wristwatch' => array(
							'small' => array(
								'src' => $uri . 'pe-icon-7-stroke/wristwatch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'pe-icon-7-stroke/wristwatch.svg',
								'height' => 128
							)
						),
					)
				)
			)
		),
	),
	'color' => array(
		'type' => 'select',
		'label' => __('Color', 'fw'),
		'choices' => array(
			'text-primary' => __('Primary', 'fw'),
			'text-secondary' => __('Secondary', 'fw'),
			'text-success' => __('Success', 'fw'),
			'text-info' => __('Info', 'fw'),
			'text-warning' => __('Warning', 'fw'),
			'text-danger' => __('Danger', 'fw'),
			'text-light' => __('Light', 'fw'),
			'text-dark' => __('Dark', 'fw')
		)
	),
	'size' => array(
		'type' => 'select',
		'label' => __('Size', 'fw'),
		'choices' => array(
			'' => __('Medium', 'fw'),
			'icon-lg' => __('Large', 'fw'),
			'icon-xl' => __('Extra Large', 'fw'),
			'icon-sm' => __('Small', 'fw'),
			'icon-xs' => __('Extra Small', 'fw'),
		)
	),
	'class' => array(
		'type' => 'text',
		'label' => __('Custom Class', 'fw')
	)
);
