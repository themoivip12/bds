<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/feature/static/img/icons/');
$options = array(
	'icon' => array(
    'type'  => 'multi-picker',
    'label' => false,
    'desc'  => false,
    'value' => array(
        /**
         * '<custom-key>' => 'default-choice'
         */
        'pack' => 'basic-flat',

        /**
         * These are the choices and their values,
         * they are available after option was saved to database
         */
        'laptop' => array(
            'price' => '123',
            'webcam' => false
        ),
        'phone' => array(
            'price' => '456',
            'memory' => '32'
        )
    ),
    'picker' => array(
        // '<custom-key>' => option
        'pack' => array(
            'label'   => __('Icon', 'fw'),
            'type'    => 'select', // or 'short-select'
            'choices' => array(
                'basic-flat'  => __('Basic Flat', 'fw'),
                'ballicons-1' => __('Ballicons 1', 'fw'),
								'ballicons-2-1' => __('Ballicons 2 - Vol 1', 'fw'),
                'ballicons-2-2' => __('Ballicons 2 - Vol 2', 'fw'),
								'ballicons-3' => __('Ballicons 3', 'fw'),
								'smallicons' => __('Smallicons', 'fw')
            )
        )
    ),
    'choices' => array(
			'basic-flat' => array(
				'icon' => array(
					'type'  => 'image-picker',
					'label' => __('', 'fw'),
					'choices' => array(
						'UFO' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/UFO.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/UFO.svg',
								'height' => 128
							)
						),
						'adaptive' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/adaptive.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/adaptive.svg',
								'height' => 128
							)
						),
						'ai' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/ai.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/ai.svg',
								'height' => 128
							)
						),
						'android' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/android.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/android.svg',
								'height' => 128
							)
						),
						'bag' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/bag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/bag.svg',
								'height' => 128
							)
						),
						'bagpack' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/bagpack.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/bagpack.svg',
								'height' => 128
							)
						),
						'bell' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/bell.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/bell.svg',
								'height' => 128
							)
						),
						'bird' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/bird.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/bird.svg',
								'height' => 128
							)
						),
						'bowling' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/bowling.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/bowling.svg',
								'height' => 128
							)
						),
						'box' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/box.svg',
								'height' => 128
							)
						),
						'briefcase' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/briefcase.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/briefcase.svg',
								'height' => 128
							)
						),
						'brush' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/brush.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/brush.svg',
								'height' => 128
							)
						),
						'calculator' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/calculator.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/calculator.svg',
								'height' => 128
							)
						),
						'calendar' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/calendar.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/calendar.svg',
								'height' => 128
							)
						),
						'camera-1' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/camera-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/camera-1.svg',
								'height' => 128
							)
						),
						'camera' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/camera.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/camera.svg',
								'height' => 128
							)
						),
						'car' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/car.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/car.svg',
								'height' => 128
							)
						),
						'card' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/card.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/card.svg',
								'height' => 128
							)
						),
						'certificate' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/certificate.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/certificate.svg',
								'height' => 128
							)
						),
						'chair' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/chair.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/chair.svg',
								'height' => 128
							)
						),
						'chart-1' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/chart-1.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/chart-1.svg',
								'height' => 128
							)
						),
						'chart' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/chart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/chart.svg',
								'height' => 128
							)
						),
						'chemistry' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/chemistry.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/chemistry.svg',
								'height' => 128
							)
						),
						'clipboard' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/clipboard.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/clipboard.svg',
								'height' => 128
							)
						),
						'clock' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/clock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/clock.svg',
								'height' => 128
							)
						),
						'code' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/code.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/code.svg',
								'height' => 128
							)
						),
						'color' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/color.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/color.svg',
								'height' => 128
							)
						),
						'compass' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/compass.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/compass.svg',
								'height' => 128
							)
						),
						'cone' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/cone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/cone.svg',
								'height' => 128
							)
						),
						'converse' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/converse.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/converse.svg',
								'height' => 128
							)
						),
						'cup' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/cup.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/cup.svg',
								'height' => 128
							)
						),
						'dialog' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/dialog.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/dialog.svg',
								'height' => 128
							)
						),
						'diamond' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/diamond.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/diamond.svg',
								'height' => 128
							)
						),
						'dock' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/dock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/dock.svg',
								'height' => 128
							)
						),
						'donut' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/donut.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/donut.svg',
								'height' => 128
							)
						),
						'earth' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/earth.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/earth.svg',
								'height' => 128
							)
						),
						'eggs' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/eggs.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/eggs.svg',
								'height' => 128
							)
						),
						'eraser' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/eraser.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/eraser.svg',
								'height' => 128
							)
						),
						'eye' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/eye.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/eye.svg',
								'height' => 128
							)
						),
						'folder' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/folder.svg',
								'height' => 128
							)
						),
						'gamepad' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/gamepad.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/gamepad.svg',
								'height' => 128
							)
						),
						'girl' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/girl.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/girl.svg',
								'height' => 128
							)
						),
						'glass snowball' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/glass snowball.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/glass snowball.svg',
								'height' => 128
							)
						),
						'headphones' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/headphones.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/headphones.svg',
								'height' => 128
							)
						),
						'heart' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/heart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/heart.svg',
								'height' => 128
							)
						),
						'help' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/help.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/help.svg',
								'height' => 128
							)
						),
						'iPhone' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/iPhone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/iPhone.svg',
								'height' => 128
							)
						),
						'imac' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/imac.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/imac.svg',
								'height' => 128
							)
						),
						'ipad' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/ipad.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/ipad.svg',
								'height' => 128
							)
						),
						'keys' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/keys.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/keys.svg',
								'height' => 128
							)
						),
						'letter' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/letter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/letter.svg',
								'height' => 128
							)
						),
						'lock' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/lock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/lock.svg',
								'height' => 128
							)
						),
						'macbook' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/macbook.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/macbook.svg',
								'height' => 128
							)
						),
						'magic hat' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/magic hat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/magic hat.svg',
								'height' => 128
							)
						),
						'map' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/map.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/map.svg',
								'height' => 128
							)
						),
						'medal' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/medal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/medal.svg',
								'height' => 128
							)
						),
						'mind' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/mind.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/mind.svg',
								'height' => 128
							)
						),
						'money' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/money.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/money.svg',
								'height' => 128
							)
						),
						'monitor' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/monitor.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/monitor.svg',
								'height' => 128
							)
						),
						'notepad' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/notepad.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/notepad.svg',
								'height' => 128
							)
						),
						'on_off' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/on_off.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/on_off.svg',
								'height' => 128
							)
						),
						'pantone' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/pantone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/pantone.svg',
								'height' => 128
							)
						),
						'paper' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/paper.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/paper.svg',
								'height' => 128
							)
						),
						'passport' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/passport.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/passport.svg',
								'height' => 128
							)
						),
						'patch' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/patch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/patch.svg',
								'height' => 128
							)
						),
						'pencil' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/pencil.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/pencil.svg',
								'height' => 128
							)
						),
						'pictures' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/pictures.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/pictures.svg',
								'height' => 128
							)
						),
						'pig' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/pig.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/pig.svg',
								'height' => 128
							)
						),
						'plane' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/plane.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/plane.svg',
								'height' => 128
							)
						),
						'present' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/present.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/present.svg',
								'height' => 128
							)
						),
						'printer' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/printer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/printer.svg',
								'height' => 128
							)
						),
						'ps' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/ps.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/ps.svg',
								'height' => 128
							)
						),
						'rocket' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/rocket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/rocket.svg',
								'height' => 128
							)
						),
						'sale' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/sale.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/sale.svg',
								'height' => 128
							)
						),
						'satellite' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/satellite.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/satellite.svg',
								'height' => 128
							)
						),
						'search' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/search.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/search.svg',
								'height' => 128
							)
						),
						'server' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/server.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/server.svg',
								'height' => 128
							)
						),
						'settings' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/settings.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/settings.svg',
								'height' => 128
							)
						),
						'sheald' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/sheald.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/sheald.svg',
								'height' => 128
							)
						),
						'shirt' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/shirt.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/shirt.svg',
								'height' => 128
							)
						),
						'sim' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/sim.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/sim.svg',
								'height' => 128
							)
						),
						'site' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/site.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/site.svg',
								'height' => 128
							)
						),
						'speaker' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/speaker.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/speaker.svg',
								'height' => 128
							)
						),
						'store' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/store.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/store.svg',
								'height' => 128
							)
						),
						'study hat' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/study hat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/study hat.svg',
								'height' => 128
							)
						),
						'tablet' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/tablet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/tablet.svg',
								'height' => 128
							)
						),
						'target' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/target.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/target.svg',
								'height' => 128
							)
						),
						'theatre' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/theatre.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/theatre.svg',
								'height' => 128
							)
						),
						'thermometer' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/thermometer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/thermometer.svg',
								'height' => 128
							)
						),
						'tick' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/tick.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/tick.svg',
								'height' => 128
							)
						),
						'ticket' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/ticket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/ticket.svg',
								'height' => 128
							)
						),
						'trash' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/trash.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/trash.svg',
								'height' => 128
							)
						),
						'twitter' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/twitter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/twitter.svg',
								'height' => 128
							)
						),
						'umbrella' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/umbrella.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/umbrella.svg',
								'height' => 128
							)
						),
						'user' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/user.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/user.svg',
								'height' => 128
							)
						),
						'video' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/video.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/video.svg',
								'height' => 128
							)
						),
						'wallet' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/wallet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/wallet.svg',
								'height' => 128
							)
						),
						'weather' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/weather.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/weather.svg',
								'height' => 128
							)
						),
						'webcam' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/webcam.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/webcam.svg',
								'height' => 128
							)
						),
						'youtube' => array(
							'small' => array(
								'src' => $uri . 'basic-flat/youtube.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'basic-flat/youtube.svg',
								'height' => 128
							)
						),
					),
				),
			),
			'ballicons-1' => array(
				'icon' => array(
					'type'  => 'image-picker',
					'label' => __('', 'fw'),
					'choices' => array(
						'3d' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/3d.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/3d.svg',
								'height' => 128
							)
						),
						'5c-case' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/5c-case.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/5c-case.svg',
								'height' => 128
							)
						),
						'5c' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/5c.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/5c.svg',
								'height' => 128
							)
						),
						'Briefcase' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/Briefcase.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/Briefcase.svg',
								'height' => 128
							)
						),
						'air-balloon' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/air-balloon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/air-balloon.svg',
								'height' => 128
							)
						),
						'airplane' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/airplane.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/airplane.svg',
								'height' => 128
							)
						),
						'alarm-clock' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/alarm-clock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/alarm-clock.svg',
								'height' => 128
							)
						),
						'analytics' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/analytics.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/analytics.svg',
								'height' => 128
							)
						),
						'android' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/android.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/android.svg',
								'height' => 128
							)
						),
						'archive' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/archive.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/archive.svg',
								'height' => 128
							)
						),
						'backpack' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/backpack.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/backpack.svg',
								'height' => 128
							)
						),
						'baloons' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/baloons.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/baloons.svg',
								'height' => 128
							)
						),
						'bell' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/bell.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/bell.svg',
								'height' => 128
							)
						),
						'bike' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/bike.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/bike.svg',
								'height' => 128
							)
						),
						'bird' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/bird.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/bird.svg',
								'height' => 128
							)
						),
						'book' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/book.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/book.svg',
								'height' => 128
							)
						),
						'box' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/box.svg',
								'height' => 128
							)
						),
						'browser' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/browser.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/browser.svg',
								'height' => 128
							)
						),
						'bubbles' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/bubbles.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/bubbles.svg',
								'height' => 128
							)
						),
						'calculate' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/calculate.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/calculate.svg',
								'height' => 128
							)
						),
						'calendar' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/calendar.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/calendar.svg',
								'height' => 128
							)
						),
						'camera' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/camera.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/camera.svg',
								'height' => 128
							)
						),
						'candy' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/candy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/candy.svg',
								'height' => 128
							)
						),
						'car' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/car.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/car.svg',
								'height' => 128
							)
						),
						'cassette' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/cassette.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/cassette.svg',
								'height' => 128
							)
						),
						'cat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/cat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/cat.svg',
								'height' => 128
							)
						),
						'certificate' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/certificate.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/certificate.svg',
								'height' => 128
							)
						),
						'chandelier' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/chandelier.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/chandelier.svg',
								'height' => 128
							)
						),
						'chart' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/chart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/chart.svg',
								'height' => 128
							)
						),
						'chemistry' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/chemistry.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/chemistry.svg',
								'height' => 128
							)
						),
						'clap' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/clap.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/clap.svg',
								'height' => 128
							)
						),
						'clipboard' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/clipboard.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/clipboard.svg',
								'height' => 128
							)
						),
						'clock' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/clock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/clock.svg',
								'height' => 128
							)
						),
						'cloud' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/cloud.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/cloud.svg',
								'height' => 128
							)
						),
						'cocktail-glass' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/cocktail-glass.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/cocktail-glass.svg',
								'height' => 128
							)
						),
						'coffee' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/coffee.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/coffee.svg',
								'height' => 128
							)
						),
						'credit-card' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/credit-card.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/credit-card.svg',
								'height' => 128
							)
						),
						'cup' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/cup.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/cup.svg',
								'height' => 128
							)
						),
						'diamond' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/diamond.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/diamond.svg',
								'height' => 128
							)
						),
						'dj' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/dj.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/dj.svg',
								'height' => 128
							)
						),
						'dock' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/dock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/dock.svg',
								'height' => 128
							)
						),
						'eye' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/eye.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/eye.svg',
								'height' => 128
							)
						),
						'fish' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/fish.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/fish.svg',
								'height' => 128
							)
						),
						'flag' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/flag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/flag.svg',
								'height' => 128
							)
						),
						'flashlight' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/flashlight.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/flashlight.svg',
								'height' => 128
							)
						),
						'folder' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/folder.svg',
								'height' => 128
							)
						),
						'gift-box' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/gift-box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/gift-box.svg',
								'height' => 128
							)
						),
						'globe' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/globe.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/globe.svg',
								'height' => 128
							)
						),
						'graph' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/graph.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/graph.svg',
								'height' => 128
							)
						),
						'guitar' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/guitar.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/guitar.svg',
								'height' => 128
							)
						),
						'guitar_2' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/guitar_2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/guitar_2.svg',
								'height' => 128
							)
						),
						'hat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/hat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/hat.svg',
								'height' => 128
							)
						),
						'ice-cream' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/ice-cream.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/ice-cream.svg',
								'height' => 128
							)
						),
						'illustrator' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/illustrator.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/illustrator.svg',
								'height' => 128
							)
						),
						'imac' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/imac.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/imac.svg',
								'height' => 128
							)
						),
						'ios-devices' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/ios-devices.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/ios-devices.svg',
								'height' => 128
							)
						),
						'ipod' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/ipod.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/ipod.svg',
								'height' => 128
							)
						),
						'joypad' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/joypad.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/joypad.svg',
								'height' => 128
							)
						),
						'keyboards' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/keyboards.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/keyboards.svg',
								'height' => 128
							)
						),
						'lightbulb' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/lightbulb.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/lightbulb.svg',
								'height' => 128
							)
						),
						'lock' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/lock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/lock.svg',
								'height' => 128
							)
						),
						'macbook' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/macbook.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/macbook.svg',
								'height' => 128
							)
						),
						'magic-hat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/magic-hat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/magic-hat.svg',
								'height' => 128
							)
						),
						'magnifier' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/magnifier.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/magnifier.svg',
								'height' => 128
							)
						),
						'mail' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/mail.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/mail.svg',
								'height' => 128
							)
						),
						'man' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/man.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/man.svg',
								'height' => 128
							)
						),
						'map' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/map.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/map.svg',
								'height' => 128
							)
						),
						'medal' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/medal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/medal.svg',
								'height' => 128
							)
						),
						'mic' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/mic.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/mic.svg',
								'height' => 128
							)
						),
						'money-saving' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/money-saving.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/money-saving.svg',
								'height' => 128
							)
						),
						'money' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/money.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/money.svg',
								'height' => 128
							)
						),
						'news' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/news.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/news.svg',
								'height' => 128
							)
						),
						'open-box' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/open-box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/open-box.svg',
								'height' => 128
							)
						),
						'package' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/package.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/package.svg',
								'height' => 128
							)
						),
						'pencils' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/pencils.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/pencils.svg',
								'height' => 128
							)
						),
						'photoshop' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/photoshop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/photoshop.svg',
								'height' => 128
							)
						),
						'picture' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/picture.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/picture.svg',
								'height' => 128
							)
						),
						'plate' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/plate.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/plate.svg',
								'height' => 128
							)
						),
						'printer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/printer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/printer.svg',
								'height' => 128
							)
						),
						'radio' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/radio.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/radio.svg',
								'height' => 128
							)
						),
						'rocket' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/rocket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/rocket.svg',
								'height' => 128
							)
						),
						'rolls' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/rolls.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/rolls.svg',
								'height' => 128
							)
						),
						'room' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/room.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/room.svg',
								'height' => 128
							)
						),
						'sale' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/sale.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/sale.svg',
								'height' => 128
							)
						),
						'satellite' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/satellite.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/satellite.svg',
								'height' => 128
							)
						),
						'scissor' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/scissor.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/scissor.svg',
								'height' => 128
							)
						),
						'settings' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/settings.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/settings.svg',
								'height' => 128
							)
						),
						'settings_2' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/settings_2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/settings_2.svg',
								'height' => 128
							)
						),
						'shield' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/shield.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/shield.svg',
								'height' => 128
							)
						),
						'ship' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/ship.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/ship.svg',
								'height' => 128
							)
						),
						'shirt' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/shirt.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/shirt.svg',
								'height' => 128
							)
						),
						'space' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/space.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/space.svg',
								'height' => 128
							)
						),
						'speakers' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/speakers.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/speakers.svg',
								'height' => 128
							)
						),
						'store' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/store.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/store.svg',
								'height' => 128
							)
						),
						'study-hat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/study-hat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/study-hat.svg',
								'height' => 128
							)
						),
						'support' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/support.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/support.svg',
								'height' => 128
							)
						),
						'tactics' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/tactics.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/tactics.svg',
								'height' => 128
							)
						),
						'target' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/target.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/target.svg',
								'height' => 128
							)
						),
						'ticket' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/ticket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/ticket.svg',
								'height' => 128
							)
						),
						'timer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/timer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/timer.svg',
								'height' => 128
							)
						),
						'tourist-bag' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/tourist-bag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/tourist-bag.svg',
								'height' => 128
							)
						),
						'tv' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/tv.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/tv.svg',
								'height' => 128
							)
						),
						'ufo' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/ufo.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/ufo.svg',
								'height' => 128
							)
						),
						'umbrella' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/umbrella.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/umbrella.svg',
								'height' => 128
							)
						),
						'video' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/video.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/video.svg',
								'height' => 128
							)
						),
						'weather' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/weather.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/weather.svg',
								'height' => 128
							)
						),
						'wi-fi' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/wi-fi.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/wi-fi.svg',
								'height' => 128
							)
						),
						'windows-phone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/windows-phone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/windows-phone.svg',
								'height' => 128
							)
						),
						'wine' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/wine.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/wine.svg',
								'height' => 128
							)
						),
						'wooman' => array(
							'small' => array(
								'src' => $uri . 'ballicons-1/wooman.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-1/wooman.svg',
								'height' => 128
							)
						),
					),
				),
			),
			'ballicons-2-1' => array(
				'icon' => array(
					'type' => 'image-picker',
					'label' => __('', 'fw'),
					'choices' => array(
						'UFO' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/UFO.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/UFO.svg',
								'height' => 100
							)
						),
						'airplane' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/airplane.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/airplane.svg',
								'height' => 100
							)
						),
						'analytics' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/analytics.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/analytics.svg',
								'height' => 100
							)
						),
						'android' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/android.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/android.svg',
								'height' => 100
							)
						),
						'archive 2' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/archive 2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/archive 2.svg',
								'height' => 100
							)
						),
						'archive' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/archive.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/archive.svg',
								'height' => 100
							)
						),
						'baloons' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/baloons.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/baloons.svg',
								'height' => 100
							)
						),
						'beacon' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/beacon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/beacon.svg',
								'height' => 100
							)
						),
						'blueprints' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/blueprints.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/blueprints.svg',
								'height' => 100
							)
						),
						'book' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/book.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/book.svg',
								'height' => 100
							)
						),
						'box' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/box.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/box.svg',
								'height' => 100
							)
						),
						'boy' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/boy.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/boy.svg',
								'height' => 100
							)
						),
						'brain' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/brain.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/brain.svg',
								'height' => 100
							)
						),
						'breakfast' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/breakfast.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/breakfast.svg',
								'height' => 100
							)
						),
						'briefcase' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/briefcase.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/briefcase.svg',
								'height' => 100
							)
						),
						'brochure' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/brochure.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/brochure.svg',
								'height' => 100
							)
						),
						'browser' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/browser.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/browser.svg',
								'height' => 100
							)
						),
						'bulb' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/bulb.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/bulb.svg',
								'height' => 100
							)
						),
						'calendar' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/calendar.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/calendar.svg',
								'height' => 100
							)
						),
						'camera' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/camera.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/camera.svg',
								'height' => 100
							)
						),
						'car' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/car.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/car.svg',
								'height' => 100
							)
						),
						'chat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/chat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/chat.svg',
								'height' => 100
							)
						),
						'chemistry' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/chemistry.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/chemistry.svg',
								'height' => 100
							)
						),
						'chip' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/chip.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/chip.svg',
								'height' => 100
							)
						),
						'cinema' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/cinema.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/cinema.svg',
								'height' => 100
							)
						),
						'clipboard' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/clipboard.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/clipboard.svg',
								'height' => 100
							)
						),
						'coffee' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/coffee.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/coffee.svg',
								'height' => 100
							)
						),
						'compass' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/compass.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/compass.svg',
								'height' => 100
							)
						),
						'cone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/cone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/cone.svg',
								'height' => 100
							)
						),
						'console' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/console.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/console.svg',
								'height' => 100
							)
						),
						'credit card' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/credit card.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/credit card.svg',
								'height' => 100
							)
						),
						'cup' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/cup.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/cup.svg',
								'height' => 100
							)
						),
						'diamond' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/diamond.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/diamond.svg',
								'height' => 100
							)
						),
						'diploma' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/diploma.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/diploma.svg',
								'height' => 100
							)
						),
						'discount' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/discount.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/discount.svg',
								'height' => 100
							)
						),
						'document' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/document.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/document.svg',
								'height' => 100
							)
						),
						'dualshock' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/dualshock.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/dualshock.svg',
								'height' => 100
							)
						),
						'eraser' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/eraser.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/eraser.svg',
								'height' => 100
							)
						),
						'eye' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/eye.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/eye.svg',
								'height' => 100
							)
						),
						'factory' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/factory.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/factory.svg',
								'height' => 100
							)
						),
						'flag' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/flag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/flag.svg',
								'height' => 100
							)
						),
						'football' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/football.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/football.svg',
								'height' => 100
							)
						),
						'gift' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/gift.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/gift.svg',
								'height' => 100
							)
						),
						'girl' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/girl.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/girl.svg',
								'height' => 100
							)
						),
						'glasses' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/glasses.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/glasses.svg',
								'height' => 100
							)
						),
						'globe' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/globe.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/globe.svg',
								'height' => 100
							)
						),
						'golf' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/golf.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/golf.svg',
								'height' => 100
							)
						),
						'headphones' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/headphones.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/headphones.svg',
								'height' => 100
							)
						),
						'heart' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/heart.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/heart.svg',
								'height' => 100
							)
						),
						'help' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/help.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/help.svg',
								'height' => 100
							)
						),
						'honey' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/honey.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/honey.svg',
								'height' => 100
							)
						),
						'iPad' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/iPad.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/iPad.svg',
								'height' => 100
							)
						),
						'iPhone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/iPhone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/iPhone.svg',
								'height' => 100
							)
						),
						'imac' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/imac.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/imac.svg',
								'height' => 100
							)
						),
						'key' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/key.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/key.svg',
								'height' => 100
							)
						),
						'laptop' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/laptop.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/laptop.svg',
								'height' => 100
							)
						),
						'lego' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/lego.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/lego.svg',
								'height' => 100
							)
						),
						'letter' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/letter.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/letter.svg',
								'height' => 100
							)
						),
						'like' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/like.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/like.svg',
								'height' => 100
							)
						),
						'lock' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/lock.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/lock.svg',
								'height' => 100
							)
						),
						'magic' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/magic.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/magic.svg',
								'height' => 100
							)
						),
						'map' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/map.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/map.svg',
								'height' => 100
							)
						),
						'match' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/match.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/match.svg',
								'height' => 100
							)
						),
						'microphine' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/microphine.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/microphine.svg',
								'height' => 100
							)
						),
						'money' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/money.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/money.svg',
								'height' => 100
							)
						),
						'mountains' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/mountains.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/mountains.svg',
								'height' => 100
							)
						),
						'notebook' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/notebook.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/notebook.svg',
								'height' => 100
							)
						),
						'pantone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/pantone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/pantone.svg',
								'height' => 100
							)
						),
						'paper' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/paper.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/paper.svg',
								'height' => 100
							)
						),
						'patch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/patch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/patch.svg',
								'height' => 100
							)
						),
						'pencil' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/pencil.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/pencil.svg',
								'height' => 100
							)
						),
						'piggy bank' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/piggy bank.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/piggy bank.svg',
								'height' => 100
							)
						),
						'polaroid' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/polaroid.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/polaroid.svg',
								'height' => 100
							)
						),
						'popcorn' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/popcorn.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/popcorn.svg',
								'height' => 100
							)
						),
						'printer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/printer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/printer.svg',
								'height' => 100
							)
						),
						'radio' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/radio.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/radio.svg',
								'height' => 100
							)
						),
						'rocket' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/rocket.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/rocket.svg',
								'height' => 100
							)
						),
						'search' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/search.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/search.svg',
								'height' => 100
							)
						),
						'settings' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/settings.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/settings.svg',
								'height' => 100
							)
						),
						'shield' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/shield.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/shield.svg',
								'height' => 100
							)
						),
						'shopping bag' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/shopping bag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/shopping bag.svg',
								'height' => 100
							)
						),
						'speed' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/speed.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/speed.svg',
								'height' => 100
							)
						),
						'statistic' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/statistic.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/statistic.svg',
								'height' => 100
							)
						),
						'store' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/store.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/store.svg',
								'height' => 100
							)
						),
						'study hat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/study hat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/study hat.svg',
								'height' => 100
							)
						),
						'suit up' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/suit up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/suit up.svg',
								'height' => 100
							)
						),
						'synthesizer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/synthesizer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/synthesizer.svg',
								'height' => 100
							)
						),
						'target' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/target.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/target.svg',
								'height' => 100
							)
						),
						'tick' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/tick.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/tick.svg',
								'height' => 100
							)
						),
						'ticket' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/ticket.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/ticket.svg',
								'height' => 100
							)
						),
						'trash' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/trash.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/trash.svg',
								'height' => 100
							)
						),
						'twit' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/twit.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/twit.svg',
								'height' => 100
							)
						),
						'typewriter' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/typewriter.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/typewriter.svg',
								'height' => 100
							)
						),
						'video' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/video.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/video.svg',
								'height' => 100
							)
						),
						'watch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/watch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/watch.svg',
								'height' => 100
							)
						),
						'water-bottle' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/water-bottle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/water-bottle.svg',
								'height' => 100
							)
						),
						'weather' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/weather.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/weather.svg',
								'height' => 100
							)
						),
						'wifi' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/wifi.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/wifi.svg',
								'height' => 100
							)
						),
						'windows-phone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/windows-phone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/windows-phone.svg',
								'height' => 100
							)
						),
						'wine' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-1/wine.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-1/wine.svg',
								'height' => 100
							)
						),
					),
				),
			),
			'ballicons-2-2' => array(
				'icon' => array(
					'type' => 'image-picker',
					'label' => __('', 'fw'),
					'choices' => array(
						'ad' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/ad.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/ad.svg',
								'height' => 100,
							),
						),
						'anchor' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/anchor.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/anchor.svg',
								'height' => 100,
							),
						),
						'atom' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/atom.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/atom.svg',
								'height' => 100,
							),
						),
						'backpack' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/backpack.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/backpack.svg',
								'height' => 100,
							),
						),
						'badge' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/badge.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/badge.svg',
								'height' => 100,
							),
						),
						'batteries' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/batteries.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/batteries.svg',
								'height' => 100,
							),
						),
						'bell' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/bell.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/bell.svg',
								'height' => 100,
							),
						),
						'bill' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/bill.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/bill.svg',
								'height' => 100,
							),
						),
						'bowling' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/bowling.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/bowling.svg',
								'height' => 100,
							),
						),
						'bridge' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/bridge.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/bridge.svg',
								'height' => 100,
							),
						),
						'brush' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/brush.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/brush.svg',
								'height' => 100,
							),
						),
						'button' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/button.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/button.svg',
								'height' => 100,
							),
						),
						'buzzer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/buzzer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/buzzer.svg',
								'height' => 100,
							),
						),
						'calculator' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/calculator.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/calculator.svg',
								'height' => 100,
							),
						),
						'candy' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/candy.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/candy.svg',
								'height' => 100,
							),
						),
						'cap-shield' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/cap-shield.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/cap-shield.svg',
								'height' => 100,
							),
						),
						'castle' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/castle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/castle.svg',
								'height' => 100,
							),
						),
						'chair' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/chair.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/chair.svg',
								'height' => 100,
							),
						),
						'chalk-board' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/chalk-board.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/chalk-board.svg',
								'height' => 100,
							),
						),
						'chip' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/chip.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/chip.svg',
								'height' => 100,
							),
						),
						'cocktail' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/cocktail.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/cocktail.svg',
								'height' => 100,
							),
						),
						'column' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/column.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/column.svg',
								'height' => 100,
							),
						),
						'config' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/config.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/config.svg',
								'height' => 100,
							),
						),
						'converse' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/converse.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/converse.svg',
								'height' => 100,
							),
						),
						'crown' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/crown.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/crown.svg',
								'height' => 100,
							),
						),
						'delivery' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/delivery.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/delivery.svg',
								'height' => 100,
							),
						),
						'demoltion' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/demoltion.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/demoltion.svg',
								'height' => 100,
							),
						),
						'donut' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/donut.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/donut.svg',
								'height' => 100,
							),
						),
						'dynamite' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/dynamite.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/dynamite.svg',
								'height' => 100,
							),
						),
						'engine' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/engine.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/engine.svg',
								'height' => 100,
							),
						),
						'film' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/film.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/film.svg',
								'height' => 100,
							),
						),
						'fire-extinguisher' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/fire-extinguisher.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/fire-extinguisher.svg',
								'height' => 100,
							),
						),
						'flasher' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/flasher.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/flasher.svg',
								'height' => 100,
							),
						),
						'flashlight' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/flashlight.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/flashlight.svg',
								'height' => 100,
							),
						),
						'flower' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/flower.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/flower.svg',
								'height' => 100,
							),
						),
						'folder' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/folder.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/folder.svg',
								'height' => 100,
							),
						),
						'gas' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/gas.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/gas.svg',
								'height' => 100,
							),
						),
						'glass-snowball' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/glass-snowball.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/glass-snowball.svg',
								'height' => 100,
							),
						),
						'graph' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/graph.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/graph.svg',
								'height' => 100,
							),
						),
						'guitar' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/guitar.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/guitar.svg',
								'height' => 100,
							),
						),
						'hammer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/hammer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/hammer.svg',
								'height' => 100,
							),
						),
						'hat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/hat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/hat.svg',
								'height' => 100,
							),
						),
						'helmet' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/helmet.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/helmet.svg',
								'height' => 100,
							),
						),
						'hospital' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/hospital.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/hospital.svg',
								'height' => 100,
							),
						),
						'hotdog' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/hotdog.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/hotdog.svg',
								'height' => 100,
							),
						),
						'lady-handbag' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/lady-handbag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/lady-handbag.svg',
								'height' => 100,
							),
						),
						'lamp' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/lamp.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/lamp.svg',
								'height' => 100,
							),
						),
						'luggage' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/luggage.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/luggage.svg',
								'height' => 100,
							),
						),
						'magic-ball' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/magic-ball.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/magic-ball.svg',
								'height' => 100,
							),
						),
						'magnet' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/magnet.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/magnet.svg',
								'height' => 100,
							),
						),
						'mailbox' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/mailbox.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/mailbox.svg',
								'height' => 100,
							),
						),
						'medal' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/medal.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/medal.svg',
								'height' => 100,
							),
						),
						'megaphone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/megaphone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/megaphone.svg',
								'height' => 100,
							),
						),
						'mirror' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/mirror.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/mirror.svg',
								'height' => 100,
							),
						),
						'monitor' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/monitor.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/monitor.svg',
								'height' => 100,
							),
						),
						'note' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/note.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/note.svg',
								'height' => 100,
							),
						),
						'paper-airplane' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/paper-airplane.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/paper-airplane.svg',
								'height' => 100,
							),
						),
						'passport' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/passport.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/passport.svg',
								'height' => 100,
							),
						),
						'pen' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/pen.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/pen.svg',
								'height' => 100,
							),
						),
						'picture' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/picture.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/picture.svg',
								'height' => 100,
							),
						),
						'pills' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/pills.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/pills.svg',
								'height' => 100,
							),
						),
						'play' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/play.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/play.svg',
								'height' => 100,
							),
						),
						'player' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/player.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/player.svg',
								'height' => 100,
							),
						),
						'pointer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/pointer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/pointer.svg',
								'height' => 100,
							),
						),
						'pool' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/pool.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/pool.svg',
								'height' => 100,
							),
						),
						'recycle' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/recycle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/recycle.svg',
								'height' => 100,
							),
						),
						'responsive' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/responsive.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/responsive.svg',
								'height' => 100,
							),
						),
						'ring' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/ring.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/ring.svg',
								'height' => 100,
							),
						),
						'road' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/road.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/road.svg',
								'height' => 100,
							),
						),
						'run' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/run.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/run.svg',
								'height' => 100,
							),
						),
						'sand-watch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/sand-watch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/sand-watch.svg',
								'height' => 100,
							),
						),
						'satellite' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/satellite.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/satellite.svg',
								'height' => 100,
							),
						),
						'scan' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/scan.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/scan.svg',
								'height' => 100,
							),
						),
						'server' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/server.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/server.svg',
								'height' => 100,
							),
						),
						'ship' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/ship.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/ship.svg',
								'height' => 100,
							),
						),
						'shirt' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/shirt.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/shirt.svg',
								'height' => 100,
							),
						),
						'sim-card' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/sim-card.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/sim-card.svg',
								'height' => 100,
							),
						),
						'skate' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/skate.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/skate.svg',
								'height' => 100,
							),
						),
						'smart-watch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/smart-watch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/smart-watch.svg',
								'height' => 100,
							),
						),
						'spray' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/spray.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/spray.svg',
								'height' => 100,
							),
						),
						'stadium' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/stadium.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/stadium.svg',
								'height' => 100,
							),
						),
						'stage' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/stage.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/stage.svg',
								'height' => 100,
							),
						),
						'star' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/star.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/star.svg',
								'height' => 100,
							),
						),
						'switcher' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/switcher.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/switcher.svg',
								'height' => 100,
							),
						),
						'theatre' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/theatre.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/theatre.svg',
								'height' => 100,
							),
						),
						'thermometer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/thermometer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/thermometer.svg',
								'height' => 100,
							),
						),
						'tower' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/tower.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/tower.svg',
								'height' => 100,
							),
						),
						'umbrella' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/umbrella.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/umbrella.svg',
								'height' => 100,
							),
						),
						'usb' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/usb.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/usb.svg',
								'height' => 100,
							),
						),
						'vault' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/vault.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/vault.svg',
								'height' => 100,
							),
						),
						'vespa' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/vespa.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/vespa.svg',
								'height' => 100,
							),
						),
						'vinyl' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/vinyl.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/vinyl.svg',
								'height' => 100,
							),
						),
						'wacom' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/wacom.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/wacom.svg',
								'height' => 100,
							),
						),
						'wallet' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/wallet.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/wallet.svg',
								'height' => 100,
							),
						),
						'watch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/watch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/watch.svg',
								'height' => 100,
							),
						),
						'webcam' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/webcam.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/webcam.svg',
								'height' => 100,
							),
						),
						'workspace' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/workspace.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/workspace.svg',
								'height' => 100,
							),
						),
						'wrench' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/wrench.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/wrench.svg',
								'height' => 100,
							),
						),
						'xbox-one' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/xbox-one.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/xbox-one.svg',
								'height' => 100,
							),
						),
						'сloset' => array(
							'small' => array(
								'src' => $uri . 'ballicons-2-2/сloset.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'ballicons-2-2/сloset.svg',
								'height' => 100,
							),
						),
					),
				),
			),
			'ballicons-3' => array(
				'icon' => array(
					'type' => 'image-picker',
					'label' => __('', 'fw'),
					'choices' => array(
						'acamulator' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/acamulator.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/acamulator.svg',
								'height' => 100
							)
						),
						'ad' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/ad.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/ad.svg',
								'height' => 100
							)
						),
						'adaptive' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/adaptive.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/adaptive.svg',
								'height' => 100
							)
						),
						'adress' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/adress.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/adress.svg',
								'height' => 100
							)
						),
						'adventure' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/adventure.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/adventure.svg',
								'height' => 100
							)
						),
						'airbaloon' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/airbaloon.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/airbaloon.svg',
								'height' => 100
							)
						),
						'almaz' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/almaz.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/almaz.svg',
								'height' => 100
							)
						),
						'america' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/america.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/america.svg',
								'height' => 100
							)
						),
						'anchor' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/anchor.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/anchor.svg',
								'height' => 100
							)
						),
						'android' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/android.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/android.svg',
								'height' => 100
							)
						),
						'archive' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/archive.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/archive.svg',
								'height' => 100
							)
						),
						'archive2' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/archive2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/archive2.svg',
								'height' => 100
							)
						),
						'auction' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/auction.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/auction.svg',
								'height' => 100
							)
						),
						'autumn' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/autumn.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/autumn.svg',
								'height' => 100
							)
						),
						'backpack' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/backpack.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/backpack.svg',
								'height' => 100
							)
						),
						'bacteria' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bacteria.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bacteria.svg',
								'height' => 100
							)
						),
						'badge' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/badge.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/badge.svg',
								'height' => 100
							)
						),
						'bag 2' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bag 2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bag 2.svg',
								'height' => 100
							)
						),
						'bag' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bag.svg',
								'height' => 100
							)
						),
						'baloons' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/baloons.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/baloons.svg',
								'height' => 100
							)
						),
						'baseball' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/baseball.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/baseball.svg',
								'height' => 100
							)
						),
						'bell' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bell.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bell.svg',
								'height' => 100
							)
						),
						'bicycle' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bicycle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bicycle.svg',
								'height' => 100
							)
						),
						'bill' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bill.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bill.svg',
								'height' => 100
							)
						),
						'billboard' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/billboard.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/billboard.svg',
								'height' => 100
							)
						),
						'bird' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bird.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bird.svg',
								'height' => 100
							)
						),
						'blackboard' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/blackboard.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/blackboard.svg',
								'height' => 100
							)
						),
						'blueprint' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/blueprint.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/blueprint.svg',
								'height' => 100
							)
						),
						'board' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/board.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/board.svg',
								'height' => 100
							)
						),
						'boat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/boat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/boat.svg',
								'height' => 100
							)
						),
						'book' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/book.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/book.svg',
								'height' => 100
							)
						),
						'bottle' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bottle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bottle.svg',
								'height' => 100
							)
						),
						'bowling' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bowling.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bowling.svg',
								'height' => 100
							)
						),
						'box' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/box.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/box.svg',
								'height' => 100
							)
						),
						'boy' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/boy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/boy.svg',
								'height' => 100
							)
						),
						'brain' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/brain.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/brain.svg',
								'height' => 100
							)
						),
						'breakfast' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/breakfast.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/breakfast.svg',
								'height' => 100
							)
						),
						'bridge' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/bridge.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/bridge.svg',
								'height' => 100
							)
						),
						'brochure' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/brochure.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/brochure.svg',
								'height' => 100
							)
						),
						'browser' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/browser.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/browser.svg',
								'height' => 100
							)
						),
						'burger' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/burger.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/burger.svg',
								'height' => 100
							)
						),
						'button' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/button.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/button.svg',
								'height' => 100
							)
						),
						'calculator' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/calculator.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/calculator.svg',
								'height' => 100
							)
						),
						'calendar' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/calendar.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/calendar.svg',
								'height' => 100
							)
						),
						'call' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/call.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/call.svg',
								'height' => 100
							)
						),
						'camera' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/camera.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/camera.svg',
								'height' => 100
							)
						),
						'camping' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/camping.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/camping.svg',
								'height' => 100
							)
						),
						'candy' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/candy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/candy.svg',
								'height' => 100
							)
						),
						'car' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/car.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/car.svg',
								'height' => 100
							)
						),
						'card' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/card.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/card.svg',
								'height' => 100
							)
						),
						'casino' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/casino.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/casino.svg',
								'height' => 100
							)
						),
						'castle' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/castle.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/castle.svg',
								'height' => 100
							)
						),
						'cat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/cat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/cat.svg',
								'height' => 100
							)
						),
						'chair' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/chair.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/chair.svg',
								'height' => 100
							)
						),
						'check' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/check.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/check.svg',
								'height' => 100
							)
						),
						'chemistry' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/chemistry.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/chemistry.svg',
								'height' => 100
							)
						),
						'chip' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/chip.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/chip.svg',
								'height' => 100
							)
						),
						'christmas' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/christmas.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/christmas.svg',
								'height' => 100
							)
						),
						'cigarette' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/cigarette.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/cigarette.svg',
								'height' => 100
							)
						),
						'city' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/city.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/city.svg',
								'height' => 100
							)
						),
						'clipboard' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/clipboard.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/clipboard.svg',
								'height' => 100
							)
						),
						'clock' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/clock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/clock.svg',
								'height' => 100
							)
						),
						'closet' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/closet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/closet.svg',
								'height' => 100
							)
						),
						'cocktails' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/cocktails.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/cocktails.svg',
								'height' => 100
							)
						),
						'coffee' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/coffee.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/coffee.svg',
								'height' => 100
							)
						),
						'column' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/column.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/column.svg',
								'height' => 100
							)
						),
						'comete' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/comete.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/comete.svg',
								'height' => 100
							)
						),
						'compas' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/compas.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/compas.svg',
								'height' => 100
							)
						),
						'cone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/cone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/cone.svg',
								'height' => 100
							)
						),
						'console' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/console.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/console.svg',
								'height' => 100
							)
						),
						'converse' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/converse.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/converse.svg',
								'height' => 100
							)
						),
						'crown' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/crown.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/crown.svg',
								'height' => 100
							)
						),
						'cup' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/cup.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/cup.svg',
								'height' => 100
							)
						),
						'delivery' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/delivery.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/delivery.svg',
								'height' => 100
							)
						),
						'desert' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/desert.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/desert.svg',
								'height' => 100
							)
						),
						'desklamp' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/desklamp.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/desklamp.svg',
								'height' => 100
							)
						),
						'desktop' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/desktop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/desktop.svg',
								'height' => 100
							)
						),
						'diplom' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/diplom.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/diplom.svg',
								'height' => 100
							)
						),
						'document' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/document.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/document.svg',
								'height' => 100
							)
						),
						'dog' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/dog.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/dog.svg',
								'height' => 100
							)
						),
						'donut' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/donut.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/donut.svg',
								'height' => 100
							)
						),
						'dynamite' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/dynamite.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/dynamite.svg',
								'height' => 100
							)
						),
						'easter' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/easter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/easter.svg',
								'height' => 100
							)
						),
						'egypt' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/egypt.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/egypt.svg',
								'height' => 100
							)
						),
						'england' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/england.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/england.svg',
								'height' => 100
							)
						),
						'eraser' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/eraser.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/eraser.svg',
								'height' => 100
							)
						),
						'extinguisher' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/extinguisher.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/extinguisher.svg',
								'height' => 100
							)
						),
						'fabric' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/fabric.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/fabric.svg',
								'height' => 100
							)
						),
						'fight' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/fight.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/fight.svg',
								'height' => 100
							)
						),
						'fire' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/fire.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/fire.svg',
								'height' => 100
							)
						),
						'fitness' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/fitness.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/fitness.svg',
								'height' => 100
							)
						),
						'flag' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/flag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/flag.svg',
								'height' => 100
							)
						),
						'flashlight' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/flashlight.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/flashlight.svg',
								'height' => 100
							)
						),
						'flower' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/flower.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/flower.svg',
								'height' => 100
							)
						),
						'folder' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/folder.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/folder.svg',
								'height' => 100
							)
						),
						'game' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/game.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/game.svg',
								'height' => 100
							)
						),
						'gas' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/gas.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/gas.svg',
								'height' => 100
							)
						),
						'gift' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/gift.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/gift.svg',
								'height' => 100
							)
						),
						'girl' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/girl.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/girl.svg',
								'height' => 100
							)
						),
						'glasses' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/glasses.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/glasses.svg',
								'height' => 100
							)
						),
						'globe' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/globe.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/globe.svg',
								'height' => 100
							)
						),
						'golf' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/golf.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/golf.svg',
								'height' => 100
							)
						),
						'graph' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/graph.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/graph.svg',
								'height' => 100
							)
						),
						'greenenergy' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/greenenergy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/greenenergy.svg',
								'height' => 100
							)
						),
						'guitar' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/guitar.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/guitar.svg',
								'height' => 100
							)
						),
						'halloween' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/halloween.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/halloween.svg',
								'height' => 100
							)
						),
						'hamer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/hamer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/hamer.svg',
								'height' => 100
							)
						),
						'hat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/hat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/hat.svg',
								'height' => 100
							)
						),
						'headphones' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/headphones.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/headphones.svg',
								'height' => 100
							)
						),
						'heart' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/heart.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/heart.svg',
								'height' => 100
							)
						),
						'helmet' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/helmet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/helmet.svg',
								'height' => 100
							)
						),
						'helmet2' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/helmet2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/helmet2.svg',
								'height' => 100
							)
						),
						'home' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/home.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/home.svg',
								'height' => 100
							)
						),
						'honey' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/honey.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/honey.svg',
								'height' => 100
							)
						),
						'hospital' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/hospital.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/hospital.svg',
								'height' => 100
							)
						),
						'hotdog' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/hotdog.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/hotdog.svg',
								'height' => 100
							)
						),
						'hourglass' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/hourglass.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/hourglass.svg',
								'height' => 100
							)
						),
						'ipad' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/ipad.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/ipad.svg',
								'height' => 100
							)
						),
						'iphone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/iphone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/iphone.svg',
								'height' => 100
							)
						),
						'italy' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/italy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/italy.svg',
								'height' => 100
							)
						),
						'jam' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/jam.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/jam.svg',
								'height' => 100
							)
						),
						'keys' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/keys.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/keys.svg',
								'height' => 100
							)
						),
						'lamp' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/lamp.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/lamp.svg',
								'height' => 100
							)
						),
						'laptop' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/laptop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/laptop.svg',
								'height' => 100
							)
						),
						'lego' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/lego.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/lego.svg',
								'height' => 100
							)
						),
						'letter' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/letter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/letter.svg',
								'height' => 100
							)
						),
						'lifebuoy' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/lifebuoy.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/lifebuoy.svg',
								'height' => 100
							)
						),
						'lighthouse' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/lighthouse.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/lighthouse.svg',
								'height' => 100
							)
						),
						'like' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/like.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/like.svg',
								'height' => 100
							)
						),
						'lock' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/lock.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/lock.svg',
								'height' => 100
							)
						),
						'magic' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/magic.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/magic.svg',
								'height' => 100
							)
						),
						'magicball' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/magicball.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/magicball.svg',
								'height' => 100
							)
						),
						'magnet' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/magnet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/magnet.svg',
								'height' => 100
							)
						),
						'map' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/map.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/map.svg',
								'height' => 100
							)
						),
						'masks' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/masks.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/masks.svg',
								'height' => 100
							)
						),
						'match' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/match.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/match.svg',
								'height' => 100
							)
						),
						'meat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/meat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/meat.svg',
								'height' => 100
							)
						),
						'medal' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/medal.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/medal.svg',
								'height' => 100
							)
						),
						'megaphone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/megaphone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/megaphone.svg',
								'height' => 100
							)
						),
						'microphone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/microphone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/microphone.svg',
								'height' => 100
							)
						),
						'microscope' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/microscope.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/microscope.svg',
								'height' => 100
							)
						),
						'mirror' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/mirror.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/mirror.svg',
								'height' => 100
							)
						),
						'molecula' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/molecula.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/molecula.svg',
								'height' => 100
							)
						),
						'money' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/money.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/money.svg',
								'height' => 100
							)
						),
						'monitor' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/monitor.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/monitor.svg',
								'height' => 100
							)
						),
						'mount' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/mount.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/mount.svg',
								'height' => 100
							)
						),
						'movie' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/movie.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/movie.svg',
								'height' => 100
							)
						),
						'music' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/music.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/music.svg',
								'height' => 100
							)
						),
						'newspaper' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/newspaper.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/newspaper.svg',
								'height' => 100
							)
						),
						'notebook' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/notebook.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/notebook.svg',
								'height' => 100
							)
						),
						'paint' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/paint.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/paint.svg',
								'height' => 100
							)
						),
						'pantone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pantone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pantone.svg',
								'height' => 100
							)
						),
						'paper_plane' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/paper_plane.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/paper_plane.svg',
								'height' => 100
							)
						),
						'paris' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/paris.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/paris.svg',
								'height' => 100
							)
						),
						'passport' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/passport.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/passport.svg',
								'height' => 100
							)
						),
						'patch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/patch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/patch.svg',
								'height' => 100
							)
						),
						'pearl' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pearl.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pearl.svg',
								'height' => 100
							)
						),
						'pen' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pen.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pen.svg',
								'height' => 100
							)
						),
						'pencil' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pencil.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pencil.svg',
								'height' => 100
							)
						),
						'phone_android' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/phone_android.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/phone_android.svg',
								'height' => 100
							)
						),
						'photos' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/photos.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/photos.svg',
								'height' => 100
							)
						),
						'pig' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pig.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pig.svg',
								'height' => 100
							)
						),
						'pill' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pill.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pill.svg',
								'height' => 100
							)
						),
						'pizza' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pizza.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pizza.svg',
								'height' => 100
							)
						),
						'plane' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/plane.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/plane.svg',
								'height' => 100
							)
						),
						'plate' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/plate.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/plate.svg',
								'height' => 100
							)
						),
						'player' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/player.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/player.svg',
								'height' => 100
							)
						),
						'pointer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pointer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pointer.svg',
								'height' => 100
							)
						),
						'polaroid' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/polaroid.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/polaroid.svg',
								'height' => 100
							)
						),
						'policehat' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/policehat.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/policehat.svg',
								'height' => 100
							)
						),
						'pool' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pool.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pool.svg',
								'height' => 100
							)
						),
						'popcorn' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/popcorn.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/popcorn.svg',
								'height' => 100
							)
						),
						'post' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/post.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/post.svg',
								'height' => 100
							)
						),
						'poststamp' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/poststamp.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/poststamp.svg',
								'height' => 100
							)
						),
						'pram' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/pram.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/pram.svg',
								'height' => 100
							)
						),
						'printer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/printer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/printer.svg',
								'height' => 100
							)
						),
						'quadrocopter' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/quadrocopter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/quadrocopter.svg',
								'height' => 100
							)
						),
						'radio' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/radio.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/radio.svg',
								'height' => 100
							)
						),
						'recycling' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/recycling.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/recycling.svg',
								'height' => 100
							)
						),
						'regbi' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/regbi.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/regbi.svg',
								'height' => 100
							)
						),
						'ring' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/ring.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/ring.svg',
								'height' => 100
							)
						),
						'rocket' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/rocket.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/rocket.svg',
								'height' => 100
							)
						),
						'run' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/run.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/run.svg',
								'height' => 100
							)
						),
						'safe' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/safe.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/safe.svg',
								'height' => 100
							)
						),
						'sale' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/sale.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/sale.svg',
								'height' => 100
							)
						),
						'satelite' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/satelite.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/satelite.svg',
								'height' => 100
							)
						),
						'save' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/save.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/save.svg',
								'height' => 100
							)
						),
						'scales' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/scales.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/scales.svg',
								'height' => 100
							)
						),
						'scaner' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/scaner.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/scaner.svg',
								'height' => 100
							)
						),
						'scooter' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/scooter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/scooter.svg',
								'height' => 100
							)
						),
						'server' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/server.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/server.svg',
								'height' => 100
							)
						),
						'setting' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/setting.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/setting.svg',
								'height' => 100
							)
						),
						'settings' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/settings.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/settings.svg',
								'height' => 100
							)
						),
						'shield' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/shield.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/shield.svg',
								'height' => 100
							)
						),
						'shirt' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/shirt.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/shirt.svg',
								'height' => 100
							)
						),
						'shop' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/shop.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/shop.svg',
								'height' => 100
							)
						),
						'shopping_bag' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/shopping_bag.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/shopping_bag.svg',
								'height' => 100
							)
						),
						'signaling' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/signaling.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/signaling.svg',
								'height' => 100
							)
						),
						'sim' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/sim.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/sim.svg',
								'height' => 100
							)
						),
						'skate' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/skate.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/skate.svg',
								'height' => 100
							)
						),
						'skull' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/skull.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/skull.svg',
								'height' => 100
							)
						),
						'smart_watch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/smart_watch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/smart_watch.svg',
								'height' => 100
							)
						),
						'snowball' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/snowball.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/snowball.svg',
								'height' => 100
							)
						),
						'spay' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/spay.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/spay.svg',
								'height' => 100
							)
						),
						'speech' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/speech.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/speech.svg',
								'height' => 100
							)
						),
						'speed' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/speed.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/speed.svg',
								'height' => 100
							)
						),
						'spring' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/spring.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/spring.svg',
								'height' => 100
							)
						),
						'stadium' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/stadium.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/stadium.svg',
								'height' => 100
							)
						),
						'statistic' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/statistic.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/statistic.svg',
								'height' => 100
							)
						),
						'suit' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/suit.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/suit.svg',
								'height' => 100
							)
						),
						'suitcase' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/suitcase.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/suitcase.svg',
								'height' => 100
							)
						),
						'summer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/summer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/summer.svg',
								'height' => 100
							)
						),
						'switch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/switch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/switch.svg',
								'height' => 100
							)
						),
						'synthesizer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/synthesizer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/synthesizer.svg',
								'height' => 100
							)
						),
						'target' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/target.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/target.svg',
								'height' => 100
							)
						),
						'tennis' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/tennis.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/tennis.svg',
								'height' => 100
							)
						),
						'theatre' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/theatre.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/theatre.svg',
								'height' => 100
							)
						),
						'thermometr' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/thermometr.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/thermometr.svg',
								'height' => 100
							)
						),
						'tickets' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/tickets.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/tickets.svg',
								'height' => 100
							)
						),
						'timer' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/timer.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/timer.svg',
								'height' => 100
							)
						),
						'tools' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/tools.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/tools.svg',
								'height' => 100
							)
						),
						'tower' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/tower.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/tower.svg',
								'height' => 100
							)
						),
						'trafficlight' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/trafficlight.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/trafficlight.svg',
								'height' => 100
							)
						),
						'train' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/train.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/train.svg',
								'height' => 100
							)
						),
						'traker' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/traker.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/traker.svg',
								'height' => 100
							)
						),
						'translate' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/translate.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/translate.svg',
								'height' => 100
							)
						),
						'trash' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/trash.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/trash.svg',
								'height' => 100
							)
						),
						'trash2' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/trash2.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/trash2.svg',
								'height' => 100
							)
						),
						'treasure' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/treasure.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/treasure.svg',
								'height' => 100
							)
						),
						'tv' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/tv.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/tv.svg',
								'height' => 100
							)
						),
						'tweet' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/tweet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/tweet.svg',
								'height' => 100
							)
						),
						'type' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/type.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/type.svg',
								'height' => 100
							)
						),
						'ufo' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/ufo.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/ufo.svg',
								'height' => 100
							)
						),
						'umbrella' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/umbrella.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/umbrella.svg',
								'height' => 100
							)
						),
						'usb' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/usb.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/usb.svg',
								'height' => 100
							)
						),
						'valentines' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/valentines.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/valentines.svg',
								'height' => 100
							)
						),
						'vendingmachine' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/vendingmachine.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/vendingmachine.svg',
								'height' => 100
							)
						),
						'vr' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/vr.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/vr.svg',
								'height' => 100
							)
						),
						'wacom' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/wacom.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/wacom.svg',
								'height' => 100
							)
						),
						'wallet' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/wallet.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/wallet.svg',
								'height' => 100
							)
						),
						'watch' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/watch.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/watch.svg',
								'height' => 100
							)
						),
						'weather' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/weather.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/weather.svg',
								'height' => 100
							)
						),
						'webcam' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/webcam.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/webcam.svg',
								'height' => 100
							)
						),
						'wifi' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/wifi.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/wifi.svg',
								'height' => 100
							)
						),
						'windowsphone' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/windowsphone.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/windowsphone.svg',
								'height' => 100
							)
						),
						'wine' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/wine.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/wine.svg',
								'height' => 100
							)
						),
						'winter' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/winter.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/winter.svg',
								'height' => 100
							)
						),
						'workplace' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/workplace.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/workplace.svg',
								'height' => 100
							)
						),
						'wrench' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/wrench.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/wrench.svg',
								'height' => 100
							)
						),
						'zoom' => array(
							'small' => array(
								'src' => $uri . 'ballicons-3/zoom.svg',
								'height' => 24
							),
							'large' => array(
								'src' => $uri . 'ballicons-3/zoom.svg',
								'height' => 100
							)
						),
					),
				),
			),
			'smallicons' => array(
				'icon' => array(
					'type' => 'image-picker',
					'label' => __('', 'fw'),
					'choices' => array(
						'+' => array(
							'small' => array(
								'src' => $uri . 'smallicons/+.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/+.svg',
								'height' => 100,
							),
						),
						'airplane' => array(
							'small' => array(
								'src' => $uri . 'smallicons/airplane.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/airplane.svg',
								'height' => 100,
							),
						),
						'alarm clock' => array(
							'small' => array(
								'src' => $uri . 'smallicons/alarm clock.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/alarm clock.svg',
								'height' => 100,
							),
						),
						'attachment' => array(
							'small' => array(
								'src' => $uri . 'smallicons/attachment.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/attachment.svg',
								'height' => 100,
							),
						),
						'backpack 2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/backpack 2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/backpack 2.svg',
								'height' => 100,
							),
						),
						'backpack' => array(
							'small' => array(
								'src' => $uri . 'smallicons/backpack.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/backpack.svg',
								'height' => 100,
							),
						),
						'bell' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bell.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bell.svg',
								'height' => 100,
							),
						),
						'book' => array(
							'small' => array(
								'src' => $uri . 'smallicons/book.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/book.svg',
								'height' => 100,
							),
						),
						'bookmark' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bookmark.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bookmark.svg',
								'height' => 100,
							),
						),
						'bookmark2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bookmark2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bookmark2.svg',
								'height' => 100,
							),
						),
						'bookshelf' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bookshelf.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bookshelf.svg',
								'height' => 100,
							),
						),
						'briefcase' => array(
							'small' => array(
								'src' => $uri . 'smallicons/briefcase.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/briefcase.svg',
								'height' => 100,
							),
						),
						'browser' => array(
							'small' => array(
								'src' => $uri . 'smallicons/browser.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/browser.svg',
								'height' => 100,
							),
						),
						'bubbl' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bubbl.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bubbl.svg',
								'height' => 100,
							),
						),
						'bus' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bus.svg',
								'height' => 100,
							),
						),
						'calendar' => array(
							'small' => array(
								'src' => $uri . 'smallicons/calendar.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/calendar.svg',
								'height' => 100,
							),
						),
						'candy' => array(
							'small' => array(
								'src' => $uri . 'smallicons/candy.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/candy.svg',
								'height' => 100,
							),
						),
						'car' => array(
							'small' => array(
								'src' => $uri . 'smallicons/car.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/car.svg',
								'height' => 100,
							),
						),
						'case' => array(
							'small' => array(
								'src' => $uri . 'smallicons/case.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/case.svg',
								'height' => 100,
							),
						),
						'chalkboard' => array(
							'small' => array(
								'src' => $uri . 'smallicons/chalkboard.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/chalkboard.svg',
								'height' => 100,
							),
						),
						'chart' => array(
							'small' => array(
								'src' => $uri . 'smallicons/chart.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/chart.svg',
								'height' => 100,
							),
						),
						'chat' => array(
							'small' => array(
								'src' => $uri . 'smallicons/chat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/chat.svg',
								'height' => 100,
							),
						),
						'chemistry' => array(
							'small' => array(
								'src' => $uri . 'smallicons/chemistry.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/chemistry.svg',
								'height' => 100,
							),
						),
						'clipboard' => array(
							'small' => array(
								'src' => $uri . 'smallicons/clipboard.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/clipboard.svg',
								'height' => 100,
							),
						),
						'clock' => array(
							'small' => array(
								'src' => $uri . 'smallicons/clock.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/clock.svg',
								'height' => 100,
							),
						),
						'death star' => array(
							'small' => array(
								'src' => $uri . 'smallicons/death star.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/death star.svg',
								'height' => 100,
							),
						),
						'diamond' => array(
							'small' => array(
								'src' => $uri . 'smallicons/diamond.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/diamond.svg',
								'height' => 100,
							),
						),
						'drop' => array(
							'small' => array(
								'src' => $uri . 'smallicons/drop.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/drop.svg',
								'height' => 100,
							),
						),
						'eraser' => array(
							'small' => array(
								'src' => $uri . 'smallicons/eraser.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/eraser.svg',
								'height' => 100,
							),
						),
						'eye' => array(
							'small' => array(
								'src' => $uri . 'smallicons/eye.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/eye.svg',
								'height' => 100,
							),
						),
						'eye2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/eye2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/eye2.svg',
								'height' => 100,
							),
						),
						'female' => array(
							'small' => array(
								'src' => $uri . 'smallicons/female.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/female.svg',
								'height' => 100,
							),
						),
						'flag' => array(
							'small' => array(
								'src' => $uri . 'smallicons/flag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/flag.svg',
								'height' => 100,
							),
						),
						'glasses' => array(
							'small' => array(
								'src' => $uri . 'smallicons/glasses.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/glasses.svg',
								'height' => 100,
							),
						),
						'graph' => array(
							'small' => array(
								'src' => $uri . 'smallicons/graph.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/graph.svg',
								'height' => 100,
							),
						),
						'graph2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/graph2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/graph2.svg',
								'height' => 100,
							),
						),
						'hand' => array(
							'small' => array(
								'src' => $uri . 'smallicons/hand.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/hand.svg',
								'height' => 100,
							),
						),
						'heart' => array(
							'small' => array(
								'src' => $uri . 'smallicons/heart.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/heart.svg',
								'height' => 100,
							),
						),
						'help' => array(
							'small' => array(
								'src' => $uri . 'smallicons/help.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/help.svg',
								'height' => 100,
							),
						),
						'image' => array(
							'small' => array(
								'src' => $uri . 'smallicons/image.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/image.svg',
								'height' => 100,
							),
						),
						'joypad' => array(
							'small' => array(
								'src' => $uri . 'smallicons/joypad.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/joypad.svg',
								'height' => 100,
							),
						),
						'key' => array(
							'small' => array(
								'src' => $uri . 'smallicons/key.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/key.svg',
								'height' => 100,
							),
						),
						'knife' => array(
							'small' => array(
								'src' => $uri . 'smallicons/knife.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/knife.svg',
								'height' => 100,
							),
						),
						'layer 1' => array(
							'small' => array(
								'src' => $uri . 'smallicons/layer 1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/layer 1.svg',
								'height' => 100,
							),
						),
						'layers 2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/layers 2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/layers 2.svg',
								'height' => 100,
							),
						),
						'lightning' => array(
							'small' => array(
								'src' => $uri . 'smallicons/lightning.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/lightning.svg',
								'height' => 100,
							),
						),
						'lock' => array(
							'small' => array(
								'src' => $uri . 'smallicons/lock.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/lock.svg',
								'height' => 100,
							),
						),
						'male' => array(
							'small' => array(
								'src' => $uri . 'smallicons/male.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/male.svg',
								'height' => 100,
							),
						),
						'map' => array(
							'small' => array(
								'src' => $uri . 'smallicons/map.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/map.svg',
								'height' => 100,
							),
						),
						'map2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/map2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/map2.svg',
								'height' => 100,
							),
						),
						'marker' => array(
							'small' => array(
								'src' => $uri . 'smallicons/marker.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/marker.svg',
								'height' => 100,
							),
						),
						'mcfly' => array(
							'small' => array(
								'src' => $uri . 'smallicons/mcfly.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/mcfly.svg',
								'height' => 100,
							),
						),
						'medal' => array(
							'small' => array(
								'src' => $uri . 'smallicons/medal.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/medal.svg',
								'height' => 100,
							),
						),
						'medicine' => array(
							'small' => array(
								'src' => $uri . 'smallicons/medicine.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/medicine.svg',
								'height' => 100,
							),
						),
						'megaphone' => array(
							'small' => array(
								'src' => $uri . 'smallicons/megaphone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/megaphone.svg',
								'height' => 100,
							),
						),
						'microphone' => array(
							'small' => array(
								'src' => $uri . 'smallicons/microphone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/microphone.svg',
								'height' => 100,
							),
						),
						'moon' => array(
							'small' => array(
								'src' => $uri . 'smallicons/moon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/moon.svg',
								'height' => 100,
							),
						),
						'mountain' => array(
							'small' => array(
								'src' => $uri . 'smallicons/mountain.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/mountain.svg',
								'height' => 100,
							),
						),
						'news' => array(
							'small' => array(
								'src' => $uri . 'smallicons/news.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/news.svg',
								'height' => 100,
							),
						),
						'packman' => array(
							'small' => array(
								'src' => $uri . 'smallicons/packman.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/packman.svg',
								'height' => 100,
							),
						),
						'paper plane' => array(
							'small' => array(
								'src' => $uri . 'smallicons/paper plane.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/paper plane.svg',
								'height' => 100,
							),
						),
						'patch' => array(
							'small' => array(
								'src' => $uri . 'smallicons/patch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/patch.svg',
								'height' => 100,
							),
						),
						'pencil' => array(
							'small' => array(
								'src' => $uri . 'smallicons/pencil.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/pencil.svg',
								'height' => 100,
							),
						),
						'photo 2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/photo 2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/photo 2.svg',
								'height' => 100,
							),
						),
						'photo' => array(
							'small' => array(
								'src' => $uri . 'smallicons/photo.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/photo.svg',
								'height' => 100,
							),
						),
						'piano' => array(
							'small' => array(
								'src' => $uri . 'smallicons/piano.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/piano.svg',
								'height' => 100,
							),
						),
						'piggy' => array(
							'small' => array(
								'src' => $uri . 'smallicons/piggy.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/piggy.svg',
								'height' => 100,
							),
						),
						'pill' => array(
							'small' => array(
								'src' => $uri . 'smallicons/pill.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/pill.svg',
								'height' => 100,
							),
						),
						'pin' => array(
							'small' => array(
								'src' => $uri . 'smallicons/pin.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/pin.svg',
								'height' => 100,
							),
						),
						'present' => array(
							'small' => array(
								'src' => $uri . 'smallicons/present.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/present.svg',
								'height' => 100,
							),
						),
						'profile' => array(
							'small' => array(
								'src' => $uri . 'smallicons/profile.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/profile.svg',
								'height' => 100,
							),
						),
						'r2d2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/r2d2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/r2d2.svg',
								'height' => 100,
							),
						),
						'rocket' => array(
							'small' => array(
								'src' => $uri . 'smallicons/rocket.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/rocket.svg',
								'height' => 100,
							),
						),
						'ruler' => array(
							'small' => array(
								'src' => $uri . 'smallicons/ruler.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/ruler.svg',
								'height' => 100,
							),
						),
						'save' => array(
							'small' => array(
								'src' => $uri . 'smallicons/save.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/save.svg',
								'height' => 100,
							),
						),
						'scissors' => array(
							'small' => array(
								'src' => $uri . 'smallicons/scissors.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/scissors.svg',
								'height' => 100,
							),
						),
						'server' => array(
							'small' => array(
								'src' => $uri . 'smallicons/server.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/server.svg',
								'height' => 100,
							),
						),
						'settings' => array(
							'small' => array(
								'src' => $uri . 'smallicons/settings.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/settings.svg',
								'height' => 100,
							),
						),
						'settings2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/settings2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/settings2.svg',
								'height' => 100,
							),
						),
						'share' => array(
							'small' => array(
								'src' => $uri . 'smallicons/share.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/share.svg',
								'height' => 100,
							),
						),
						'shopping bag' => array(
							'small' => array(
								'src' => $uri . 'smallicons/shopping bag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/shopping bag.svg',
								'height' => 100,
							),
						),
						'skull' => array(
							'small' => array(
								'src' => $uri . 'smallicons/skull.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/skull.svg',
								'height' => 100,
							),
						),
						'speakers' => array(
							'small' => array(
								'src' => $uri . 'smallicons/speakers.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/speakers.svg',
								'height' => 100,
							),
						),
						'speed' => array(
							'small' => array(
								'src' => $uri . 'smallicons/speed.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/speed.svg',
								'height' => 100,
							),
						),
						'star' => array(
							'small' => array(
								'src' => $uri . 'smallicons/star.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/star.svg',
								'height' => 100,
							),
						),
						'switcher' => array(
							'small' => array(
								'src' => $uri . 'smallicons/switcher.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/switcher.svg',
								'height' => 100,
							),
						),
						'tactic' => array(
							'small' => array(
								'src' => $uri . 'smallicons/tactic.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/tactic.svg',
								'height' => 100,
							),
						),
						'timer' => array(
							'small' => array(
								'src' => $uri . 'smallicons/timer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/timer.svg',
								'height' => 100,
							),
						),
						'train' => array(
							'small' => array(
								'src' => $uri . 'smallicons/train.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/train.svg',
								'height' => 100,
							),
						),
						'trash' => array(
							'small' => array(
								'src' => $uri . 'smallicons/trash.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/trash.svg',
								'height' => 100,
							),
						),
						'tune' => array(
							'small' => array(
								'src' => $uri . 'smallicons/tune.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/tune.svg',
								'height' => 100,
							),
						),
						'tv' => array(
							'small' => array(
								'src' => $uri . 'smallicons/tv.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/tv.svg',
								'height' => 100,
							),
						),
						'ufo' => array(
							'small' => array(
								'src' => $uri . 'smallicons/ufo.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/ufo.svg',
								'height' => 100,
							),
						),
						'umbrella' => array(
							'small' => array(
								'src' => $uri . 'smallicons/umbrella.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/umbrella.svg',
								'height' => 100,
							),
						),
						'video' => array(
							'small' => array(
								'src' => $uri . 'smallicons/video.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/video.svg',
								'height' => 100,
							),
						),
						'watch' => array(
							'small' => array(
								'src' => $uri . 'smallicons/watch.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/watch.svg',
								'height' => 100,
							),
						),
						'www' => array(
							'small' => array(
								'src' => $uri . 'smallicons/www.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/www.svg',
								'height' => 100,
							),
						),
						'zoom +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/zoom +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/zoom +.svg',
								'height' => 100,
							),
						),
						'zoom -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/zoom -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/zoom -.svg',
								'height' => 100,
							),
						),
						'percent' => array(
							'small' => array(
								'src' => $uri . 'smallicons/percent.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/percent.svg',
								'height' => 100,
							),
						),
						'amex' => array(
							'small' => array(
								'src' => $uri . 'smallicons/amex.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/amex.svg',
								'height' => 100,
							),
						),
						'barcode' => array(
							'small' => array(
								'src' => $uri . 'smallicons/barcode.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/barcode.svg',
								'height' => 100,
							),
						),
						'basket +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/basket +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/basket +.svg',
								'height' => 100,
							),
						),
						'basket -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/basket -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/basket -.svg',
								'height' => 100,
							),
						),
						'basket' => array(
							'small' => array(
								'src' => $uri . 'smallicons/basket.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/basket.svg',
								'height' => 100,
							),
						),
						'bill' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bill.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bill.svg',
								'height' => 100,
							),
						),
						'bitcoin' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bitcoin.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bitcoin.svg',
								'height' => 100,
							),
						),
						'calc' => array(
							'small' => array(
								'src' => $uri . 'smallicons/calc.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/calc.svg',
								'height' => 100,
							),
						),
						'card' => array(
							'small' => array(
								'src' => $uri . 'smallicons/card.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/card.svg',
								'height' => 100,
							),
						),
						'cashbox' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cashbox.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cashbox.svg',
								'height' => 100,
							),
						),
						'coin' => array(
							'small' => array(
								'src' => $uri . 'smallicons/coin.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/coin.svg',
								'height' => 100,
							),
						),
						'coins' => array(
							'small' => array(
								'src' => $uri . 'smallicons/coins.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/coins.svg',
								'height' => 100,
							),
						),
						'discover' => array(
							'small' => array(
								'src' => $uri . 'smallicons/discover.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/discover.svg',
								'height' => 100,
							),
						),
						'dollar' => array(
							'small' => array(
								'src' => $uri . 'smallicons/dollar.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/dollar.svg',
								'height' => 100,
							),
						),
						'euro' => array(
							'small' => array(
								'src' => $uri . 'smallicons/euro.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/euro.svg',
								'height' => 100,
							),
						),
						'google-wallet' => array(
							'small' => array(
								'src' => $uri . 'smallicons/google-wallet.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/google-wallet.svg',
								'height' => 100,
							),
						),
						'label' => array(
							'small' => array(
								'src' => $uri . 'smallicons/label.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/label.svg',
								'height' => 100,
							),
						),
						'maestro' => array(
							'small' => array(
								'src' => $uri . 'smallicons/maestro.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/maestro.svg',
								'height' => 100,
							),
						),
						'mastercard' => array(
							'small' => array(
								'src' => $uri . 'smallicons/mastercard.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/mastercard.svg',
								'height' => 100,
							),
						),
						'money 2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/money 2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/money 2.svg',
								'height' => 100,
							),
						),
						'money' => array(
							'small' => array(
								'src' => $uri . 'smallicons/money.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/money.svg',
								'height' => 100,
							),
						),
						'new' => array(
							'small' => array(
								'src' => $uri . 'smallicons/new.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/new.svg',
								'height' => 100,
							),
						),
						'paypal' => array(
							'small' => array(
								'src' => $uri . 'smallicons/paypal.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/paypal.svg',
								'height' => 100,
							),
						),
						'pound' => array(
							'small' => array(
								'src' => $uri . 'smallicons/pound.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/pound.svg',
								'height' => 100,
							),
						),
						'store' => array(
							'small' => array(
								'src' => $uri . 'smallicons/store.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/store.svg',
								'height' => 100,
							),
						),
						'visa 2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/visa 2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/visa 2.svg',
								'height' => 100,
							),
						),
						'visa' => array(
							'small' => array(
								'src' => $uri . 'smallicons/visa.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/visa.svg',
								'height' => 100,
							),
						),
						'wallet' => array(
							'small' => array(
								'src' => $uri . 'smallicons/wallet.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/wallet.svg',
								'height' => 100,
							),
						),
						'yena' => array(
							'small' => array(
								'src' => $uri . 'smallicons/yena.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/yena.svg',
								'height' => 100,
							),
						),
						'AI_1 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/AI_1 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/AI_1 +.svg',
								'height' => 100,
							),
						),
						'AI_1 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/AI_1 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/AI_1 -.svg',
								'height' => 100,
							),
						),
						'AI_1' => array(
							'small' => array(
								'src' => $uri . 'smallicons/AI_1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/AI_1.svg',
								'height' => 100,
							),
						),
						'AI_2 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/AI_2 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/AI_2 +.svg',
								'height' => 100,
							),
						),
						'AI_2 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/AI_2 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/AI_2 -.svg',
								'height' => 100,
							),
						),
						'AI_2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/AI_2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/AI_2.svg',
								'height' => 100,
							),
						),
						'PDF_1 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PDF_1 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PDF_1 +.svg',
								'height' => 100,
							),
						),
						'PDF_1 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PDF_1 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PDF_1 -.svg',
								'height' => 100,
							),
						),
						'PDF_1' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PDF_1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PDF_1.svg',
								'height' => 100,
							),
						),
						'PDF_2 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PDF_2 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PDF_2 +.svg',
								'height' => 100,
							),
						),
						'PDF_2 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PDF_2 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PDF_2 -.svg',
								'height' => 100,
							),
						),
						'PDF_2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PDF_2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PDF_2.svg',
								'height' => 100,
							),
						),
						'PS_1 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PS_1 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PS_1 +.svg',
								'height' => 100,
							),
						),
						'PS_1 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PS_1 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PS_1 -.svg',
								'height' => 100,
							),
						),
						'PS_1' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PS_1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PS_1.svg',
								'height' => 100,
							),
						),
						'PS_2 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PS_2 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PS_2 +.svg',
								'height' => 100,
							),
						),
						'PS_2 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PS_2 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PS_2 -.svg',
								'height' => 100,
							),
						),
						'PS_2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/PS_2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/PS_2.svg',
								'height' => 100,
							),
						),
						'archive arrow down' => array(
							'small' => array(
								'src' => $uri . 'smallicons/archive arrow down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/archive arrow down.svg',
								'height' => 100,
							),
						),
						'archive arrow up' => array(
							'small' => array(
								'src' => $uri . 'smallicons/archive arrow up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/archive arrow up.svg',
								'height' => 100,
							),
						),
						'archive doc' => array(
							'small' => array(
								'src' => $uri . 'smallicons/archive doc.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/archive doc.svg',
								'height' => 100,
							),
						),
						'archive' => array(
							'small' => array(
								'src' => $uri . 'smallicons/archive.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/archive.svg',
								'height' => 100,
							),
						),
						'folder +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/folder +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/folder +.svg',
								'height' => 100,
							),
						),
						'folder -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/folder -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/folder -.svg',
								'height' => 100,
							),
						),
						'folder arrow' => array(
							'small' => array(
								'src' => $uri . 'smallicons/folder arrow.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/folder arrow.svg',
								'height' => 100,
							),
						),
						'folder' => array(
							'small' => array(
								'src' => $uri . 'smallicons/folder.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/folder.svg',
								'height' => 100,
							),
						),
						'letter' => array(
							'small' => array(
								'src' => $uri . 'smallicons/letter.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/letter.svg',
								'height' => 100,
							),
						),
						'open letter' => array(
							'small' => array(
								'src' => $uri . 'smallicons/open letter.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/open letter.svg',
								'height' => 100,
							),
						),
						'table_1 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/table_1 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/table_1 +.svg',
								'height' => 100,
							),
						),
						'table_1 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/table_1 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/table_1 -.svg',
								'height' => 100,
							),
						),
						'table_1' => array(
							'small' => array(
								'src' => $uri . 'smallicons/table_1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/table_1.svg',
								'height' => 100,
							),
						),
						'table_2 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/table_2 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/table_2 +.svg',
								'height' => 100,
							),
						),
						'table_2 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/table_2 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/table_2 -.svg',
								'height' => 100,
							),
						),
						'table_2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/table_2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/table_2.svg',
								'height' => 100,
							),
						),
						'text_1 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/text_1 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/text_1 +.svg',
								'height' => 100,
							),
						),
						'text_1 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/text_1 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/text_1 -.svg',
								'height' => 100,
							),
						),
						'text_1' => array(
							'small' => array(
								'src' => $uri . 'smallicons/text_1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/text_1.svg',
								'height' => 100,
							),
						),
						'text_2 +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/text_2 +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/text_2 +.svg',
								'height' => 100,
							),
						),
						'text_2 -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/text_2 -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/text_2 -.svg',
								'height' => 100,
							),
						),
						'text_2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/text_2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/text_2.svg',
								'height' => 100,
							),
						),
						'bamboo' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bamboo.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bamboo.svg',
								'height' => 100,
							),
						),
						'galaxy' => array(
							'small' => array(
								'src' => $uri . 'smallicons/galaxy.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/galaxy.svg',
								'height' => 100,
							),
						),
						'htc one' => array(
							'small' => array(
								'src' => $uri . 'smallicons/htc one.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/htc one.svg',
								'height' => 100,
							),
						),
						'imac' => array(
							'small' => array(
								'src' => $uri . 'smallicons/imac.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/imac.svg',
								'height' => 100,
							),
						),
						'ipad' => array(
							'small' => array(
								'src' => $uri . 'smallicons/ipad.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/ipad.svg',
								'height' => 100,
							),
						),
						'iphone' => array(
							'small' => array(
								'src' => $uri . 'smallicons/iphone.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/iphone.svg',
								'height' => 100,
							),
						),
						'keyboard' => array(
							'small' => array(
								'src' => $uri . 'smallicons/keyboard.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/keyboard.svg',
								'height' => 100,
							),
						),
						'macbook' => array(
							'small' => array(
								'src' => $uri . 'smallicons/macbook.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/macbook.svg',
								'height' => 100,
							),
						),
						'mouse' => array(
							'small' => array(
								'src' => $uri . 'smallicons/mouse.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/mouse.svg',
								'height' => 100,
							),
						),
						'printer' => array(
							'small' => array(
								'src' => $uri . 'smallicons/printer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/printer.svg',
								'height' => 100,
							),
						),
						'0' => array(
							'small' => array(
								'src' => $uri . 'smallicons/0.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/0.svg',
								'height' => 100,
							),
						),
						'1' => array(
							'small' => array(
								'src' => $uri . 'smallicons/1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/1.svg',
								'height' => 100,
							),
						),
						'2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/2.svg',
								'height' => 100,
							),
						),
						'3' => array(
							'small' => array(
								'src' => $uri . 'smallicons/3.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/3.svg',
								'height' => 100,
							),
						),
						'4' => array(
							'small' => array(
								'src' => $uri . 'smallicons/4.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/4.svg',
								'height' => 100,
							),
						),
						'45-2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/45-2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/45-2.svg',
								'height' => 100,
							),
						),
						'45' => array(
							'small' => array(
								'src' => $uri . 'smallicons/45.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/45.svg',
								'height' => 100,
							),
						),
						'45_3' => array(
							'small' => array(
								'src' => $uri . 'smallicons/45_3.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/45_3.svg',
								'height' => 100,
							),
						),
						'45_4' => array(
							'small' => array(
								'src' => $uri . 'smallicons/45_4.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/45_4.svg',
								'height' => 100,
							),
						),
						'5' => array(
							'small' => array(
								'src' => $uri . 'smallicons/5.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/5.svg',
								'height' => 100,
							),
						),
						'6' => array(
							'small' => array(
								'src' => $uri . 'smallicons/6.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/6.svg',
								'height' => 100,
							),
						),
						'7' => array(
							'small' => array(
								'src' => $uri . 'smallicons/7.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/7.svg',
								'height' => 100,
							),
						),
						'8' => array(
							'small' => array(
								'src' => $uri . 'smallicons/8.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/8.svg',
								'height' => 100,
							),
						),
						'9' => array(
							'small' => array(
								'src' => $uri . 'smallicons/9.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/9.svg',
								'height' => 100,
							),
						),
						'Bluetooth' => array(
							'small' => array(
								'src' => $uri . 'smallicons/Bluetooth.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/Bluetooth.svg',
								'height' => 100,
							),
						),
						'anchors' => array(
							'small' => array(
								'src' => $uri . 'smallicons/anchors.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/anchors.svg',
								'height' => 100,
							),
						),
						'big grid' => array(
							'small' => array(
								'src' => $uri . 'smallicons/big grid.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/big grid.svg',
								'height' => 100,
							),
						),
						'buttery charging' => array(
							'small' => array(
								'src' => $uri . 'smallicons/buttery charging.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/buttery charging.svg',
								'height' => 100,
							),
						),
						'buttery full' => array(
							'small' => array(
								'src' => $uri . 'smallicons/buttery full.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/buttery full.svg',
								'height' => 100,
							),
						),
						'buttery low' => array(
							'small' => array(
								'src' => $uri . 'smallicons/buttery low.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/buttery low.svg',
								'height' => 100,
							),
						),
						'buttery medium' => array(
							'small' => array(
								'src' => $uri . 'smallicons/buttery medium.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/buttery medium.svg',
								'height' => 100,
							),
						),
						'buttery null' => array(
							'small' => array(
								'src' => $uri . 'smallicons/buttery null.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/buttery null.svg',
								'height' => 100,
							),
						),
						'center text' => array(
							'small' => array(
								'src' => $uri . 'smallicons/center text.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/center text.svg',
								'height' => 100,
							),
						),
						'change position' => array(
							'small' => array(
								'src' => $uri . 'smallicons/change position.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/change position.svg',
								'height' => 100,
							),
						),
						'check' => array(
							'small' => array(
								'src' => $uri . 'smallicons/check.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/check.svg',
								'height' => 100,
							),
						),
						'close' => array(
							'small' => array(
								'src' => $uri . 'smallicons/close.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/close.svg',
								'height' => 100,
							),
						),
						'cloud check' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloud check.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloud check.svg',
								'height' => 100,
							),
						),
						'cloud down' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloud down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloud down.svg',
								'height' => 100,
							),
						),
						'cloud error' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloud error.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloud error.svg',
								'height' => 100,
							),
						),
						'cloud refresh' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloud refresh.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloud refresh.svg',
								'height' => 100,
							),
						),
						'cloud up' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloud up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloud up.svg',
								'height' => 100,
							),
						),
						'collaps' => array(
							'small' => array(
								'src' => $uri . 'smallicons/collaps.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/collaps.svg',
								'height' => 100,
							),
						),
						'divide' => array(
							'small' => array(
								'src' => $uri . 'smallicons/divide.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/divide.svg',
								'height' => 100,
							),
						),
						'down' => array(
							'small' => array(
								'src' => $uri . 'smallicons/down.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/down.svg',
								'height' => 100,
							),
						),
						'download' => array(
							'small' => array(
								'src' => $uri . 'smallicons/download.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/download.svg',
								'height' => 100,
							),
						),
						'download_2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/download_2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/download_2.svg',
								'height' => 100,
							),
						),
						'eject' => array(
							'small' => array(
								'src' => $uri . 'smallicons/eject.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/eject.svg',
								'height' => 100,
							),
						),
						'equally' => array(
							'small' => array(
								'src' => $uri . 'smallicons/equally.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/equally.svg',
								'height' => 100,
							),
						),
						'exclamation mark' => array(
							'small' => array(
								'src' => $uri . 'smallicons/exclamation mark.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/exclamation mark.svg',
								'height' => 100,
							),
						),
						'expand' => array(
							'small' => array(
								'src' => $uri . 'smallicons/expand.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/expand.svg',
								'height' => 100,
							),
						),
						'fullscreen' => array(
							'small' => array(
								'src' => $uri . 'smallicons/fullscreen.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/fullscreen.svg',
								'height' => 100,
							),
						),
						'grid' => array(
							'small' => array(
								'src' => $uri . 'smallicons/grid.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/grid.svg',
								'height' => 100,
							),
						),
						'justify all' => array(
							'small' => array(
								'src' => $uri . 'smallicons/justify all.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/justify all.svg',
								'height' => 100,
							),
						),
						'left align text' => array(
							'small' => array(
								'src' => $uri . 'smallicons/left align text.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/left align text.svg',
								'height' => 100,
							),
						),
						'left-left' => array(
							'small' => array(
								'src' => $uri . 'smallicons/left-left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/left-left.svg',
								'height' => 100,
							),
						),
						'left' => array(
							'small' => array(
								'src' => $uri . 'smallicons/left.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/left.svg',
								'height' => 100,
							),
						),
						'list' => array(
							'small' => array(
								'src' => $uri . 'smallicons/list.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/list.svg',
								'height' => 100,
							),
						),
						'marquee' => array(
							'small' => array(
								'src' => $uri . 'smallicons/marquee.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/marquee.svg',
								'height' => 100,
							),
						),
						'minus' => array(
							'small' => array(
								'src' => $uri . 'smallicons/minus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/minus.svg',
								'height' => 100,
							),
						),
						'narrow' => array(
							'small' => array(
								'src' => $uri . 'smallicons/narrow.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/narrow.svg',
								'height' => 100,
							),
						),
						'network' => array(
							'small' => array(
								'src' => $uri . 'smallicons/network.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/network.svg',
								'height' => 100,
							),
						),
						'next' => array(
							'small' => array(
								'src' => $uri . 'smallicons/next.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/next.svg',
								'height' => 100,
							),
						),
						'pause' => array(
							'small' => array(
								'src' => $uri . 'smallicons/pause.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/pause.svg',
								'height' => 100,
							),
						),
						'play' => array(
							'small' => array(
								'src' => $uri . 'smallicons/play.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/play.svg',
								'height' => 100,
							),
						),
						'plus' => array(
							'small' => array(
								'src' => $uri . 'smallicons/plus.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/plus.svg',
								'height' => 100,
							),
						),
						'power' => array(
							'small' => array(
								'src' => $uri . 'smallicons/power.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/power.svg',
								'height' => 100,
							),
						),
						'prev' => array(
							'small' => array(
								'src' => $uri . 'smallicons/prev.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/prev.svg',
								'height' => 100,
							),
						),
						'record' => array(
							'small' => array(
								'src' => $uri . 'smallicons/record.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/record.svg',
								'height' => 100,
							),
						),
						'refresh 2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/refresh 2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/refresh 2.svg',
								'height' => 100,
							),
						),
						'refresh' => array(
							'small' => array(
								'src' => $uri . 'smallicons/refresh.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/refresh.svg',
								'height' => 100,
							),
						),
						'repeat' => array(
							'small' => array(
								'src' => $uri . 'smallicons/repeat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/repeat.svg',
								'height' => 100,
							),
						),
						'right align text' => array(
							'small' => array(
								'src' => $uri . 'smallicons/right align text.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/right align text.svg',
								'height' => 100,
							),
						),
						'right-right' => array(
							'small' => array(
								'src' => $uri . 'smallicons/right-right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/right-right.svg',
								'height' => 100,
							),
						),
						'right' => array(
							'small' => array(
								'src' => $uri . 'smallicons/right.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/right.svg',
								'height' => 100,
							),
						),
						'shuffle ' => array(
							'small' => array(
								'src' => $uri . 'smallicons/shuffle .svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/shuffle .svg',
								'height' => 100,
							),
						),
						'sitemap' => array(
							'small' => array(
								'src' => $uri . 'smallicons/sitemap.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/sitemap.svg',
								'height' => 100,
							),
						),
						'sitemap2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/sitemap2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/sitemap2.svg',
								'height' => 100,
							),
						),
						'sitemap3' => array(
							'small' => array(
								'src' => $uri . 'smallicons/sitemap3.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/sitemap3.svg',
								'height' => 100,
							),
						),
						'stop' => array(
							'small' => array(
								'src' => $uri . 'smallicons/stop.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/stop.svg',
								'height' => 100,
							),
						),
						'strech' => array(
							'small' => array(
								'src' => $uri . 'smallicons/strech.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/strech.svg',
								'height' => 100,
							),
						),
						'up' => array(
							'small' => array(
								'src' => $uri . 'smallicons/up.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/up.svg',
								'height' => 100,
							),
						),
						'upload' => array(
							'small' => array(
								'src' => $uri . 'smallicons/upload.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/upload.svg',
								'height' => 100,
							),
						),
						'upload_2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/upload_2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/upload_2.svg',
								'height' => 100,
							),
						),
						'volume +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/volume +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/volume +.svg',
								'height' => 100,
							),
						),
						'volume -' => array(
							'small' => array(
								'src' => $uri . 'smallicons/volume -.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/volume -.svg',
								'height' => 100,
							),
						),
						'volume big' => array(
							'small' => array(
								'src' => $uri . 'smallicons/volume big.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/volume big.svg',
								'height' => 100,
							),
						),
						'volume mini' => array(
							'small' => array(
								'src' => $uri . 'smallicons/volume mini.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/volume mini.svg',
								'height' => 100,
							),
						),
						'volume x' => array(
							'small' => array(
								'src' => $uri . 'smallicons/volume x.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/volume x.svg',
								'height' => 100,
							),
						),
						'wi-fi' => array(
							'small' => array(
								'src' => $uri . 'smallicons/wi-fi.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/wi-fi.svg',
								'height' => 100,
							),
						),
						'x' => array(
							'small' => array(
								'src' => $uri . 'smallicons/x.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/x.svg',
								'height' => 100,
							),
						),
						'bread' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bread.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bread.svg',
								'height' => 100,
							),
						),
						'cocktail' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cocktail.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cocktail.svg',
								'height' => 100,
							),
						),
						'coffee' => array(
							'small' => array(
								'src' => $uri . 'smallicons/coffee.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/coffee.svg',
								'height' => 100,
							),
						),
						'coffee2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/coffee2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/coffee2.svg',
								'height' => 100,
							),
						),
						'donut' => array(
							'small' => array(
								'src' => $uri . 'smallicons/donut.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/donut.svg',
								'height' => 100,
							),
						),
						'egg' => array(
							'small' => array(
								'src' => $uri . 'smallicons/egg.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/egg.svg',
								'height' => 100,
							),
						),
						'hamburger' => array(
							'small' => array(
								'src' => $uri . 'smallicons/hamburger.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/hamburger.svg',
								'height' => 100,
							),
						),
						'hotdog' => array(
							'small' => array(
								'src' => $uri . 'smallicons/hotdog.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/hotdog.svg',
								'height' => 100,
							),
						),
						'ice1' => array(
							'small' => array(
								'src' => $uri . 'smallicons/ice1.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/ice1.svg',
								'height' => 100,
							),
						),
						'ice2' => array(
							'small' => array(
								'src' => $uri . 'smallicons/ice2.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/ice2.svg',
								'height' => 100,
							),
						),
						'kettle' => array(
							'small' => array(
								'src' => $uri . 'smallicons/kettle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/kettle.svg',
								'height' => 100,
							),
						),
						'lemon' => array(
							'small' => array(
								'src' => $uri . 'smallicons/lemon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/lemon.svg',
								'height' => 100,
							),
						),
						'milk' => array(
							'small' => array(
								'src' => $uri . 'smallicons/milk.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/milk.svg',
								'height' => 100,
							),
						),
						'muffin' => array(
							'small' => array(
								'src' => $uri . 'smallicons/muffin.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/muffin.svg',
								'height' => 100,
							),
						),
						'pizza' => array(
							'small' => array(
								'src' => $uri . 'smallicons/pizza.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/pizza.svg',
								'height' => 100,
							),
						),
						'tea' => array(
							'small' => array(
								'src' => $uri . 'smallicons/tea.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/tea.svg',
								'height' => 100,
							),
						),
						'toaster' => array(
							'small' => array(
								'src' => $uri . 'smallicons/toaster.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/toaster.svg',
								'height' => 100,
							),
						),
						'toque' => array(
							'small' => array(
								'src' => $uri . 'smallicons/toque.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/toque.svg',
								'height' => 100,
							),
						),
						'tray' => array(
							'small' => array(
								'src' => $uri . 'smallicons/tray.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/tray.svg',
								'height' => 100,
							),
						),
						'wine' => array(
							'small' => array(
								'src' => $uri . 'smallicons/wine.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/wine.svg',
								'height' => 100,
							),
						),
						'cloud & sun' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloud & sun.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloud & sun.svg',
								'height' => 100,
							),
						),
						'cloud' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloud.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloud.svg',
								'height' => 100,
							),
						),
						'cloudy & lightning' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloudy & lightning.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloudy & lightning.svg',
								'height' => 100,
							),
						),
						'cloudy & rain' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloudy & rain.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloudy & rain.svg',
								'height' => 100,
							),
						),
						'cloudy & snow' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloudy & snow.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloudy & snow.svg',
								'height' => 100,
							),
						),
						'cloudy & wind' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloudy & wind.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloudy & wind.svg',
								'height' => 100,
							),
						),
						'cloudy night' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cloudy night.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cloudy night.svg',
								'height' => 100,
							),
						),
						'drop' => array(
							'small' => array(
								'src' => $uri . 'smallicons/drop.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/drop.svg',
								'height' => 100,
							),
						),
						'lightning & sun' => array(
							'small' => array(
								'src' => $uri . 'smallicons/lightning & sun.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/lightning & sun.svg',
								'height' => 100,
							),
						),
						'moon' => array(
							'small' => array(
								'src' => $uri . 'smallicons/moon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/moon.svg',
								'height' => 100,
							),
						),
						'night & lightning' => array(
							'small' => array(
								'src' => $uri . 'smallicons/night & lightning.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/night & lightning.svg',
								'height' => 100,
							),
						),
						'night & rain' => array(
							'small' => array(
								'src' => $uri . 'smallicons/night & rain.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/night & rain.svg',
								'height' => 100,
							),
						),
						'night & snow' => array(
							'small' => array(
								'src' => $uri . 'smallicons/night & snow.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/night & snow.svg',
								'height' => 100,
							),
						),
						'night & wind' => array(
							'small' => array(
								'src' => $uri . 'smallicons/night & wind.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/night & wind.svg',
								'height' => 100,
							),
						),
						'rain & sun' => array(
							'small' => array(
								'src' => $uri . 'smallicons/rain & sun.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/rain & sun.svg',
								'height' => 100,
							),
						),
						'snow & sun' => array(
							'small' => array(
								'src' => $uri . 'smallicons/snow & sun.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/snow & sun.svg',
								'height' => 100,
							),
						),
						'snowflake' => array(
							'small' => array(
								'src' => $uri . 'smallicons/snowflake.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/snowflake.svg',
								'height' => 100,
							),
						),
						'sun' => array(
							'small' => array(
								'src' => $uri . 'smallicons/sun.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/sun.svg',
								'height' => 100,
							),
						),
						'thermometer' => array(
							'small' => array(
								'src' => $uri . 'smallicons/thermometer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/thermometer.svg',
								'height' => 100,
							),
						),
						'wind & sun' => array(
							'small' => array(
								'src' => $uri . 'smallicons/wind & sun.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/wind & sun.svg',
								'height' => 100,
							),
						),
						'baseball' => array(
							'small' => array(
								'src' => $uri . 'smallicons/baseball.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/baseball.svg',
								'height' => 100,
							),
						),
						'basketball' => array(
							'small' => array(
								'src' => $uri . 'smallicons/basketball.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/basketball.svg',
								'height' => 100,
							),
						),
						'billiards' => array(
							'small' => array(
								'src' => $uri . 'smallicons/billiards.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/billiards.svg',
								'height' => 100,
							),
						),
						'finish' => array(
							'small' => array(
								'src' => $uri . 'smallicons/finish.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/finish.svg',
								'height' => 100,
							),
						),
						'football' => array(
							'small' => array(
								'src' => $uri . 'smallicons/football.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/football.svg',
								'height' => 100,
							),
						),
						'glove' => array(
							'small' => array(
								'src' => $uri . 'smallicons/glove.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/glove.svg',
								'height' => 100,
							),
						),
						'gold medal' => array(
							'small' => array(
								'src' => $uri . 'smallicons/gold medal.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/gold medal.svg',
								'height' => 100,
							),
						),
						'medal bronze' => array(
							'small' => array(
								'src' => $uri . 'smallicons/medal bronze.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/medal bronze.svg',
								'height' => 100,
							),
						),
						'medal silver' => array(
							'small' => array(
								'src' => $uri . 'smallicons/medal silver.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/medal silver.svg',
								'height' => 100,
							),
						),
						'rugby' => array(
							'small' => array(
								'src' => $uri . 'smallicons/rugby.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/rugby.svg',
								'height' => 100,
							),
						),
						'aquarium' => array(
							'small' => array(
								'src' => $uri . 'smallicons/aquarium.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/aquarium.svg',
								'height' => 100,
							),
						),
						'art' => array(
							'small' => array(
								'src' => $uri . 'smallicons/art.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/art.svg',
								'height' => 100,
							),
						),
						'bank' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bank.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bank.svg',
								'height' => 100,
							),
						),
						'baseball' => array(
							'small' => array(
								'src' => $uri . 'smallicons/baseball.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/baseball.svg',
								'height' => 100,
							),
						),
						'bawling' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bawling.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bawling.svg',
								'height' => 100,
							),
						),
						'blueprints' => array(
							'small' => array(
								'src' => $uri . 'smallicons/blueprints.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/blueprints.svg',
								'height' => 100,
							),
						),
						'bowtie' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bowtie.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bowtie.svg',
								'height' => 100,
							),
						),
						'box' => array(
							'small' => array(
								'src' => $uri . 'smallicons/box.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/box.svg',
								'height' => 100,
							),
						),
						'cake' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cake.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cake.svg',
								'height' => 100,
							),
						),
						'candle' => array(
							'small' => array(
								'src' => $uri . 'smallicons/candle.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/candle.svg',
								'height' => 100,
							),
						),
						'chair' => array(
							'small' => array(
								'src' => $uri . 'smallicons/chair.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/chair.svg',
								'height' => 100,
							),
						),
						'cheeze' => array(
							'small' => array(
								'src' => $uri . 'smallicons/cheeze.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/cheeze.svg',
								'height' => 100,
							),
						),
						'chicken' => array(
							'small' => array(
								'src' => $uri . 'smallicons/chicken.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/chicken.svg',
								'height' => 100,
							),
						),
						'chips' => array(
							'small' => array(
								'src' => $uri . 'smallicons/chips.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/chips.svg',
								'height' => 100,
							),
						),
						'christmas' => array(
							'small' => array(
								'src' => $uri . 'smallicons/christmas.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/christmas.svg',
								'height' => 100,
							),
						),
						'duck' => array(
							'small' => array(
								'src' => $uri . 'smallicons/duck.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/duck.svg',
								'height' => 100,
							),
						),

						'golf' => array(
							'small' => array(
								'src' => $uri . 'smallicons/golf.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/golf.svg',
								'height' => 100,
							),
						),
						'guitar сombo' => array(
							'small' => array(
								'src' => $uri . 'smallicons/guitar сombo.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/guitar сombo.svg',
								'height' => 100,
							),
						),
						'handbag' => array(
							'small' => array(
								'src' => $uri . 'smallicons/handbag.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/handbag.svg',
								'height' => 100,
							),
						),
						'hat' => array(
							'small' => array(
								'src' => $uri . 'smallicons/hat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/hat.svg',
								'height' => 100,
							),
						),
						'headphones' => array(
							'small' => array(
								'src' => $uri . 'smallicons/headphones.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/headphones.svg',
								'height' => 100,
							),
						),
						'house' => array(
							'small' => array(
								'src' => $uri . 'smallicons/house.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/house.svg',
								'height' => 100,
							),
						),
						'lamp' => array(
							'small' => array(
								'src' => $uri . 'smallicons/lamp.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/lamp.svg',
								'height' => 100,
							),
						),
						'lightbulb' => array(
							'small' => array(
								'src' => $uri . 'smallicons/lightbulb.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/lightbulb.svg',
								'height' => 100,
							),
						),
						'lights' => array(
							'small' => array(
								'src' => $uri . 'smallicons/lights.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/lights.svg',
								'height' => 100,
							),
						),
						'magic hat' => array(
							'small' => array(
								'src' => $uri . 'smallicons/magic hat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/magic hat.svg',
								'height' => 100,
							),
						),
						'mailbox' => array(
							'small' => array(
								'src' => $uri . 'smallicons/mailbox.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/mailbox.svg',
								'height' => 100,
							),
						),
						'meat' => array(
							'small' => array(
								'src' => $uri . 'smallicons/meat.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/meat.svg',
								'height' => 100,
							),
						),
						'microwave' => array(
							'small' => array(
								'src' => $uri . 'smallicons/microwave.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/microwave.svg',
								'height' => 100,
							),
						),
						'notepad' => array(
							'small' => array(
								'src' => $uri . 'smallicons/notepad.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/notepad.svg',
								'height' => 100,
							),
						),
						'pacifier' => array(
							'small' => array(
								'src' => $uri . 'smallicons/pacifier.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/pacifier.svg',
								'height' => 100,
							),
						),
						'paint' => array(
							'small' => array(
								'src' => $uri . 'smallicons/paint.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/paint.svg',
								'height' => 100,
							),
						),
						'paper' => array(
							'small' => array(
								'src' => $uri . 'smallicons/paper.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/paper.svg',
								'height' => 100,
							),
						),
						'processor' => array(
							'small' => array(
								'src' => $uri . 'smallicons/processor.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/processor.svg',
								'height' => 100,
							),
						),
						'ps4' => array(
							'small' => array(
								'src' => $uri . 'smallicons/ps4.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/ps4.svg',
								'height' => 100,
							),
						),
						'x-box one' => array(
							'small' => array(
								'src' => $uri . 'smallicons/x-box one.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/x-box one.svg',
								'height' => 100,
							),
						),
						'shield' => array(
							'small' => array(
								'src' => $uri . 'smallicons/shield.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/shield.svg',
								'height' => 100,
							),
						),
						'shirt' => array(
							'small' => array(
								'src' => $uri . 'smallicons/shirt.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/shirt.svg',
								'height' => 100,
							),
						),
						'shorts' => array(
							'small' => array(
								'src' => $uri . 'smallicons/shorts.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/shorts.svg',
								'height' => 100,
							),
						),
						'shrub' => array(
							'small' => array(
								'src' => $uri . 'smallicons/shrub.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/shrub.svg',
								'height' => 100,
							),
						),
						'sword' => array(
							'small' => array(
								'src' => $uri . 'smallicons/sword.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/sword.svg',
								'height' => 100,
							),
						),
						'teddy bear' => array(
							'small' => array(
								'src' => $uri . 'smallicons/teddy bear.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/teddy bear.svg',
								'height' => 100,
							),
						),
						'ticket' => array(
							'small' => array(
								'src' => $uri . 'smallicons/ticket.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/ticket.svg',
								'height' => 100,
							),
						),
						'tshirt' => array(
							'small' => array(
								'src' => $uri . 'smallicons/tshirt.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/tshirt.svg',
								'height' => 100,
							),
						),
						'vinyl' => array(
							'small' => array(
								'src' => $uri . 'smallicons/vinyl.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/vinyl.svg',
								'height' => 100,
							),
						),
						'wand' => array(
							'small' => array(
								'src' => $uri . 'smallicons/wand.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/wand.svg',
								'height' => 100,
							),
						),
						'web camera' => array(
							'small' => array(
								'src' => $uri . 'smallicons/web camera.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/web camera.svg',
								'height' => 100,
							),
						),
						'yin-yang' => array(
							'small' => array(
								'src' => $uri . 'smallicons/yin-yang.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/yin-yang.svg',
								'height' => 100,
							),
						),
						'zippo' => array(
							'small' => array(
								'src' => $uri . 'smallicons/zippo.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/zippo.svg',
								'height' => 100,
							),
						),
						'amazon' => array(
							'small' => array(
								'src' => $uri . 'smallicons/amazon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/amazon.svg',
								'height' => 100,
							),
						),
						'android' => array(
							'small' => array(
								'src' => $uri . 'smallicons/android.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/android.svg',
								'height' => 100,
							),
						),
						'apple' => array(
							'small' => array(
								'src' => $uri . 'smallicons/apple.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/apple.svg',
								'height' => 100,
							),
						),
						'appstore' => array(
							'small' => array(
								'src' => $uri . 'smallicons/appstore.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/appstore.svg',
								'height' => 100,
							),
						),
						'behance' => array(
							'small' => array(
								'src' => $uri . 'smallicons/behance.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/behance.svg',
								'height' => 100,
							),
						),
						'bloger' => array(
							'small' => array(
								'src' => $uri . 'smallicons/bloger.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/bloger.svg',
								'height' => 100,
							),
						),
						'chrome' => array(
							'small' => array(
								'src' => $uri . 'smallicons/chrome.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/chrome.svg',
								'height' => 100,
							),
						),
						'delicious' => array(
							'small' => array(
								'src' => $uri . 'smallicons/delicious.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/delicious.svg',
								'height' => 100,
							),
						),
						'deviantart' => array(
							'small' => array(
								'src' => $uri . 'smallicons/deviantart.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/deviantart.svg',
								'height' => 100,
							),
						),
						'dribbble' => array(
							'small' => array(
								'src' => $uri . 'smallicons/dribbble.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/dribbble.svg',
								'height' => 100,
							),
						),
						'dropbox' => array(
							'small' => array(
								'src' => $uri . 'smallicons/dropbox.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/dropbox.svg',
								'height' => 100,
							),
						),
						'facebook' => array(
							'small' => array(
								'src' => $uri . 'smallicons/facebook.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/facebook.svg',
								'height' => 100,
							),
						),
						'firefox' => array(
							'small' => array(
								'src' => $uri . 'smallicons/firefox.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/firefox.svg',
								'height' => 100,
							),
						),
						'flickr' => array(
							'small' => array(
								'src' => $uri . 'smallicons/flickr.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/flickr.svg',
								'height' => 100,
							),
						),
						'forrst' => array(
							'small' => array(
								'src' => $uri . 'smallicons/forrst.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/forrst.svg',
								'height' => 100,
							),
						),
						'github' => array(
							'small' => array(
								'src' => $uri . 'smallicons/github.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/github.svg',
								'height' => 100,
							),
						),
						'google +' => array(
							'small' => array(
								'src' => $uri . 'smallicons/google +.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/google +.svg',
								'height' => 100,
							),
						),
						'google drive' => array(
							'small' => array(
								'src' => $uri . 'smallicons/google drive.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/google drive.svg',
								'height' => 100,
							),
						),
						'google play' => array(
							'small' => array(
								'src' => $uri . 'smallicons/google play.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/google play.svg',
								'height' => 100,
							),
						),
						'html5' => array(
							'small' => array(
								'src' => $uri . 'smallicons/html5.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/html5.svg',
								'height' => 100,
							),
						),
						'instagram' => array(
							'small' => array(
								'src' => $uri . 'smallicons/instagram.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/instagram.svg',
								'height' => 100,
							),
						),
						'internet explorer' => array(
							'small' => array(
								'src' => $uri . 'smallicons/internet explorer.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/internet explorer.svg',
								'height' => 100,
							),
						),
						'lastfm' => array(
							'small' => array(
								'src' => $uri . 'smallicons/lastfm.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/lastfm.svg',
								'height' => 100,
							),
						),
						'linkedin' => array(
							'small' => array(
								'src' => $uri . 'smallicons/linkedin.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/linkedin.svg',
								'height' => 100,
							),
						),
						'microsoft' => array(
							'small' => array(
								'src' => $uri . 'smallicons/microsoft.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/microsoft.svg',
								'height' => 100,
							),
						),
						'myspace' => array(
							'small' => array(
								'src' => $uri . 'smallicons/myspace.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/myspace.svg',
								'height' => 100,
							),
						),
						'opera' => array(
							'small' => array(
								'src' => $uri . 'smallicons/opera.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/opera.svg',
								'height' => 100,
							),
						),
						'path' => array(
							'small' => array(
								'src' => $uri . 'smallicons/path.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/path.svg',
								'height' => 100,
							),
						),
						'pinterest' => array(
							'small' => array(
								'src' => $uri . 'smallicons/pinterest.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/pinterest.svg',
								'height' => 100,
							),
						),
						'rss' => array(
							'small' => array(
								'src' => $uri . 'smallicons/rss.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/rss.svg',
								'height' => 100,
							),
						),
						'safari' => array(
							'small' => array(
								'src' => $uri . 'smallicons/safari.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/safari.svg',
								'height' => 100,
							),
						),
						'skype' => array(
							'small' => array(
								'src' => $uri . 'smallicons/skype.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/skype.svg',
								'height' => 100,
							),
						),
						'soundcloud' => array(
							'small' => array(
								'src' => $uri . 'smallicons/soundcloud.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/soundcloud.svg',
								'height' => 100,
							),
						),
						'stumbleupon' => array(
							'small' => array(
								'src' => $uri . 'smallicons/stumbleupon.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/stumbleupon.svg',
								'height' => 100,
							),
						),
						'tumblr' => array(
							'small' => array(
								'src' => $uri . 'smallicons/tumblr.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/tumblr.svg',
								'height' => 100,
							),
						),
						'twitter' => array(
							'small' => array(
								'src' => $uri . 'smallicons/twitter.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/twitter.svg',
								'height' => 100,
							),
						),
						'vimeo' => array(
							'small' => array(
								'src' => $uri . 'smallicons/vimeo.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/vimeo.svg',
								'height' => 100,
							),
						),
						'wordpress' => array(
							'small' => array(
								'src' => $uri . 'smallicons/wordpress.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/wordpress.svg',
								'height' => 100,
							),
						),
						'yahoo' => array(
							'small' => array(
								'src' => $uri . 'smallicons/yahoo.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/yahoo.svg',
								'height' => 100,
							),
						),
						'youtube' => array(
							'small' => array(
								'src' => $uri . 'smallicons/youtube.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/youtube.svg',
								'height' => 100,
							),
						),
						'digg' => array(
							'small' => array(
								'src' => $uri . 'smallicons/digg.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/digg.svg',
								'height' => 100,
							),
						),
						'drupal' => array(
							'small' => array(
								'src' => $uri . 'smallicons/drupal.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/drupal.svg',
								'height' => 100,
							),
						),
						'envato' => array(
							'small' => array(
								'src' => $uri . 'smallicons/envato.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/envato.svg',
								'height' => 100,
							),
						),
						'evernote' => array(
							'small' => array(
								'src' => $uri . 'smallicons/evernote.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/evernote.svg',
								'height' => 100,
							),
						),
						'fancy' => array(
							'small' => array(
								'src' => $uri . 'smallicons/fancy.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/fancy.svg',
								'height' => 100,
							),
						),
						'foursquare' => array(
							'small' => array(
								'src' => $uri . 'smallicons/foursquare.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/foursquare.svg',
								'height' => 100,
							),
						),
						'glove' => array(
							'small' => array(
								'src' => $uri . 'smallicons/glove.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/glove.svg',
								'height' => 100,
							),
						),
						'basecamp' => array(
							'small' => array(
								'src' => $uri . 'smallicons/basecamp.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/basecamp.svg',
								'height' => 100,
							),
						),
						'grooveshark' => array(
							'small' => array(
								'src' => $uri . 'smallicons/grooveshark.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/grooveshark.svg',
								'height' => 100,
							),
						),
						'kickstarter' => array(
							'small' => array(
								'src' => $uri . 'smallicons/kickstarter.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/kickstarter.svg',
								'height' => 100,
							),
						),
						'quora' => array(
							'small' => array(
								'src' => $uri . 'smallicons/quora.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/quora.svg',
								'height' => 100,
							),
						),
						'rdio' => array(
							'small' => array(
								'src' => $uri . 'smallicons/rdio.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/rdio.svg',
								'height' => 100,
							),
						),
						'sony playstation' => array(
							'small' => array(
								'src' => $uri . 'smallicons/sony playstation.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/sony playstation.svg',
								'height' => 100,
							),
						),
						'spotify' => array(
							'small' => array(
								'src' => $uri . 'smallicons/spotify.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/spotify.svg',
								'height' => 100,
							),
						),
						'squarespace' => array(
							'small' => array(
								'src' => $uri . 'smallicons/squarespace.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/squarespace.svg',
								'height' => 100,
							),
						),
						'svpply' => array(
							'small' => array(
								'src' => $uri . 'smallicons/svpply.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/svpply.svg',
								'height' => 100,
							),
						),
						'vine' => array(
							'small' => array(
								'src' => $uri . 'smallicons/vine.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/vine.svg',
								'height' => 100,
							),
						),
						'weheartit' => array(
							'small' => array(
								'src' => $uri . 'smallicons/weheartit.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/weheartit.svg',
								'height' => 100,
							),
						),
						'wikipedia' => array(
							'small' => array(
								'src' => $uri . 'smallicons/wikipedia.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/wikipedia.svg',
								'height' => 100,
							),
						),
						'zerply' => array(
							'small' => array(
								'src' => $uri . 'smallicons/zerply.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/zerply.svg',
								'height' => 100,
							),
						),
						'x-box' => array(
							'small' => array(
								'src' => $uri . 'smallicons/x-box.svg',
								'height' => 24,
							),
							'large' => array(
								'src' => $uri . 'smallicons/x-box.svg',
								'height' => 100,
							),
						),
					),
				),
			),
    ),
    /**
     * (optional) if is true, the borders between choice options will be shown
     */
    'show_borders' => false,
	),
	'size'  => array(
		'type'    => 'group',
		'options' => array(
			'width'  => array(
				'type'  => 'text',
				'label' => __( 'Width', 'fw' ),
				'desc'  => __( 'Set image width', 'fw' )
			),
			'height' => array(
				'type'  => 'text',
				'label' => __( 'Height', 'fw' ),
				'desc'  => __( 'Set image height', 'fw' )
			)
		)
	),
	'title'   => array(
		'type'  => 'text',
		'label' => __( 'Title', 'fw' ),
	),
	'content' => array(
		'type'  => 'textarea',
		'label' => __( 'Content', 'fw' ),
		'desc'  => __( 'Enter the desired content', 'fw' ),
	),
);
