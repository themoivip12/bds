<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */
if ( ! empty( $atts['icon'] ) ) {
	$pack = $atts['icon']['pack'];
	$icon = $atts['icon'][$pack]['icon'];

	$width  = ( is_numeric( $atts['width'] ) && ( $atts['width'] > 0 ) ) ? $atts['width'] : '';
	$height = ( is_numeric( $atts['height'] ) && ( $atts['height'] > 0 ) ) ? $atts['height'] : '';

	$image = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/feature/static/img/icons/') . $pack . '/' . $icon . '.svg';

	$img_attributes = array(
		'src' => $image,
		'alt' => $icon
	);

	$img_attributes['style'] = '';

	if(!empty($width)){
		$img_attributes['style'] .= 'width: '. $width . "px;";
	}

	if(!empty($height)){
		$img_attributes['style'] .= 'height: '. $height . "px;";
	}
}
?>
<div class="fw-feature">
<?php if ( ! empty( $atts['icon'] ) ) : ?>
	<div class="fw-feature-image">
		<?php echo fw_html_tag('img', $img_attributes); ?>
	</div>
<?php endif; ?>
	<div class="fw-feature-aside">
		<div class="fw-feature-title">
			<h3 class="h4 font-weight-bold mt-5 mb-4"><?php echo $atts['title']; ?></h3>
		</div>
		<div class="fw-feature-text small">
			<?php echo $atts['content']; ?>
		</div>
	</div>
</div>
