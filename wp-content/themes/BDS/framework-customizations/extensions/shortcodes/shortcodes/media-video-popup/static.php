<?php if (!defined('FW')) die('Forbidden');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/media-video-popup');
wp_enqueue_style(
	'fw-shortcode-media-video-popup',
	$uri . '/static/css/styles.css'
);
