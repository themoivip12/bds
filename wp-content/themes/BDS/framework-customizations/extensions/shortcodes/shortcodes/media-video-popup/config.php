<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Video Popup', 'fw'),
	'description'   => __('Add a Video', 'fw'),
	'tab'           => __('Media Elements', 'fw'),
);
