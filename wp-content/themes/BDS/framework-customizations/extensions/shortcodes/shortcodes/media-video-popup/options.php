<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'id'    => array( 'type' => 'unique' ),
	'url'    => array(
		'type'  => 'text',
		'label' => __( 'Video URL', 'fw' ),
		'desc'  => __( 'Insert video URL to embed this video', 'fw' )
	),
	'thumbnail'    => array(
		'type'  => 'upload',
		'label' => __( 'Thumbnail', 'fw' ),
		'desc'  => __( 'Upload thumbnail for this video', 'fw' )
	),
	'size'             => array(
		'type'    => 'group',
		'options' => array(
			'width'  => array(
				'type'  => 'text',
				'label' => __( 'Width', 'fw' ),
				'desc'  => __( 'Set thumbnail width', 'fw' ),
				'value' => 300
			),
			'height' => array(
				'type'  => 'text',
				'label' => __( 'Height', 'fw' ),
				'desc'  => __( 'Set thumbnail height', 'fw' ),
				'value' => 200
			)
		)
	),
	// 'width'  => array(
	// 	'type'  => 'text',
	// 	'label' => __( 'Video Width', 'fw' ),
	// 	'desc'  => __( 'Enter a value for the width', 'fw' ),
	// 	'value' => 1280
	// ),
	// 'height' => array(
	// 	'type'  => 'text',
	// 	'label' => __( 'Video Height', 'fw' ),
	// 	'desc'  => __( 'Enter a value for the height', 'fw' ),
	// 	'value' => 720
	// )
);
