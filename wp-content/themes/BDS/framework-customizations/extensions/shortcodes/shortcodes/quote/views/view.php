<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
?>
<hr class="hr-md border-gray-300 mb-7">
<blockquote class="blockquote text-center mb-7">
	<p class="h2 text-primary mb-0">
		“<?php echo esc_attr( $atts['content'] ); ?>”
	</p>
	<footer class="blockquote-footer d-flex align-items-center justify-content-center mt-5 mt-md-7 mb-0">
		<div class="avatar avatar-xl mr-4">
			<img src="<?php echo esc_url( $atts['author_avatar']['url'] ); ?>" alt="<?php echo esc_attr( $atts['author_name'] ); ?>" class="avatar-img rounded-circle">
		</div>
		<div class="text-left">
			<div class="h4 font-weight-bold text-dark mb-0"><?php echo esc_attr( $atts['author_name'] ); ?></div>
			<div class="h5 text-muted mb-0"><?php echo esc_attr( $atts['author_job'] ); ?> at <a href="<?php echo esc_url( $atts['site_url'] ); ?>" target="_blank" rel="nofollow"><?php echo esc_attr( $atts['site_name'] ); ?></a></div>
		</div>
	</footer>
</blockquote>
<hr class="hr-md border-gray-300 mb-7">
