<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Quote', 'fw' ),
	'description' => __( 'Add a Quote', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);
