<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'type' => array(
		'type' => 'select',
		'label' => __( 'Type', 'fw' ),
		'choices' => array(
			'curve-1' => __( 'Curve 1', 'fw' ),
			'curve-2' => __( 'Curve 2', 'fw' ),
			'curve-3' => __( 'Curve 3', 'fw' ),
			'curve-4' => __( 'Curve 4', 'fw' ),
			'curve-5' => __( 'Curve 5', 'fw' ),
			'curve-6' => __( 'Curve 6', 'fw' ),
			'wave-1' => __( 'Wave 1', 'fw' ),
			'angle-top' => __( 'Angle Top', 'fw' ),
			'angle-bottom' => __( 'Angle Bottom', 'fw' ),
			'angle-right' => __( 'Angle Right', 'fw' ),
			'angle-left' => __( 'Angle Left', 'fw' ),
			'blur-1' => __( 'Blur 1', 'fw' ),
			'blur-2' => __( 'Blur 2', 'fw' ),
			'blur-3' => __( 'Blur 3', 'fw' ),
			'blur-4' => __( 'Blur 4', 'fw' ),
		)
	),
	'class' => array(
		'label' => __( 'Custom Class', 'fw' ),
		'type' => 'text'
	)
);
