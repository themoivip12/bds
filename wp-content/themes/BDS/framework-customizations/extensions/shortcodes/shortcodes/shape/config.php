<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Shape', 'fw'),
	'description'   => __('Add a Shape', 'fw'),
	'tab'           => __('Content Elements', 'fw'),
	'popup_size'    => 'small'
);
