<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
	$type = $atts['type'];
	$image = fw_get_template_customizations_directory('/extensions/shortcodes/shortcodes/shape/static/img/shapes/') . $type . '.svg';
	$class = 'shape';
	switch ( $type ) {
		case 'curve-1' :
			$class .= ' shape-bottom shape-fluid-x text-white';
		break;
		case 'curve-2' :
			$class .= ' shape-bottom shape-fluid-x text-white';
		break;
		case 'curve-3' :
			$class .= ' shape-bottom shape-fluid-x text-white';
		break;
		case 'curve-4' :
			$class .= ' shape-right shape-fluid-x text-white';
		break;
		case 'curve-5' :
			$class .= ' shape-left shape-fluid-x text-white';
		break;
		case 'curve-6' :
			$class .= ' shape-bottom shape-fluid-x text-white';
		break;
		case 'wave-1' :
			$class .= ' shape-top shape-fluid-x text-white';
		break;
		case 'angle-top' :
			$class .= ' shape-top shape-fluid-x text-white';
		break;
		case 'angle-bottom' :
			$class .= ' shape-bottom shape-fluid-x text-white';
		break;
		case 'angle-right' :
			$class .= ' shape-right shape-fluid-y text-white';
		break;
		case 'angle-left' :
			$class .= ' shape-left shape-fluid-y text-white';
		break;
		case 'blur-1' :
			$class .= ' shape-blur-1 text-gray-200';
		break;
		case 'blur-2' :
			$class .= ' shape-blur-2 text-gray-200';
		break;
		case 'blur-3' :
			$class .= ' shape-top shape-fluid-x text-dark';
		break;
		case 'blur-4' :
			$class .= ' shape-blur-4 text-gray-200';
		break;
	}
	if (!empty($atts['class'])) {
		$class .= ' ' . $atts['class'];
	}
?>
<div class="<?php echo esc_attr( $class ); ?>">
	<?php echo file_get_contents( $image ); ?>
</div>
