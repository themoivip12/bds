<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'subtitle' => array(
		'type'  => 'text',
		'label' => __( 'Hero Subtitle', 'fw' ),
		'desc'  => __( 'Write the hero subtitle', 'fw' ),
	),
	'title'    => array(
		'type'  => 'text',
		'label' => __( 'Hero Title', 'fw' ),
		'desc'  => __( 'Write the hero title', 'fw' ),
	),
	'content' => array(
		'type'  => 'text',
		'label'   => __('Hero Content', 'fw'),
	),
);
