<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Hero', 'fw'),
	'description'   => __('Add Hero', 'fw'),
	'tab'           => __('Content Elements', 'fw'),
);
