<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
?>
<div class="fw-hero">
	<div class="row justify-content-center text-center">
	<div class="col-12 col-md-10 col-lg-9">
		<?php if (!empty($atts['subtitle'])): ?>
			<h6 class="text-uppercase font-weight-bold text-primary"><?php echo $atts['subtitle']; ?></h6>
		<?php endif; ?>
		<?php if (!empty($atts['title'])): ?>
			<h1 class="display-2 font-weight-bold"><?php echo $atts['title']; ?></h1>
		<?php endif; ?>
		<?php if (!empty($atts['content'])): ?>
			<p class="font-size-lg text-muted mb-6 mb-md-8"><?php echo $atts['content']; ?></p>
		<?php endif; ?>
		<div class="px-md-12">
			<div class="form-row">
				<div class="col-12 col-md">
					<div class="form-group mb-md-0">
						<input class="form-control border-dark" type="email" placeholder="Enter your email address">
					</div>
				</div>
				<div class="col-12 col-md-auto">
					<a class="btn btn-block btn-primary shadow lift" href="https://app.workevo.io/register">Get started</a>
				</div>
			</div>
			<p class="mt-4 mb-0 text-muted small">
				Already have an account? <a href="https://app.workevo.io/" target="_blank" rel="noopener noreferrer">Sign in</a>.</p>
			</div>
		</div>
	</div>
</div>
