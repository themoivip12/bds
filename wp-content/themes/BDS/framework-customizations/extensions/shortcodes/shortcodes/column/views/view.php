<?php if (!defined('FW')) die('Forbidden');

$class = fw_ext_builder_get_item_width('page-builder', $atts['width'] . '/frontend_class');
if (!empty($atts['class'])) {
	$class .= ' ' . $atts['class'];
}
?>
<div class="<?php echo esc_attr($class); ?>">
	<?php echo do_shortcode($content); ?>
</div>
