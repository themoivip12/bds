<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'class' => array(
		'label' => __('Custom Class', 'fw'),
		'type'  => 'text',
	),
);
