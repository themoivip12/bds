<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'slides' => array(
		'type'          => 'addable-popup',
		'label'         => __( 'Slides', 'fw' ),
		'popup-title'   => __( 'Add/Edit Slide', 'fw' ),
		'desc'          => __( 'Create your slides', 'fw' ),
		'template'      => '{{=slide_title}}',
		'popup-options' => array(
			'slide_title'   => array(
				'type'  => 'text',
				'label' => __('Title', 'fw')
			),
			'slide_content' => array(
				'type'  => 'textarea',
				'label' => __('Content', 'fw')
			)
		)
	),
	'cell_class' => array(
		'type'    => 'text',
		'label'   => __('Cell Class', 'fw'),
		'value'   => 'col-12 col-md-4 px-1'
	),
	'cell_align' => array(
		'type' => 'select',
		'label' => __( 'Cell Align', 'fw' ),
		'help'    => __('Align cells within the carousel element.', 'fw'),
		'choices' => array(
			'left' => __('Left', 'fw'),
			'right' => __('Right', 'fw'),
			'center' => __('Center', 'fw'),
		)
	),
	'wrap_around' => array(
		'type'    => 'switch',
		'label'   => __('Wrap Around', 'fw'),
		'help'    => __('At the end of cells, wrap-around to the other end for infinite scrolling.', 'fw'),
	),
	'images_loaded' => array(
		'type'    => 'switch',
		'label'   => __('Images Loaded', 'fw'),
		'help'    => __('Unloaded images have no size, which can throw off cell positions. To fix this, the imagesLoaded option re-positions cells once their images have loaded.', 'fw'),
	),
	'page_dots' => array(
		'type'    => 'switch',
		'label'   => __('Page Dots', 'fw'),
		'help'    => __('Creates and enables page dots.', 'fw'),
	),
	'prev_next_buttons' => array(
		'type'    => 'switch',
		'label'   => __('Prev/Next Buttons', 'fw'),
		'help'    => __('Creates and enables previous & next buttons.', 'fw'),
	),
	'contain' => array(
		'type'    => 'switch',
		'label'   => __('Contain', 'fw'),
		'help'    => __('Contains cells to carousel element to prevent excess scroll at beginning or end.', 'fw'),
	),
	'custom_option' => array(
		'type'    => 'text',
		'label'   => __('Custom Options', 'fw'),
		'help'    => __('Custom Options for flickity', 'fw'),
		'desc' => __('See document: https://flickity.metafizzy.co/options.html', 'fw')
	),
	'custom_class' => array(
		'type'    => 'text',
		'label'   => __('Custom Class', 'fw'),
		'help'    => __('Custom Class for flickity', 'fw')
	),
);
