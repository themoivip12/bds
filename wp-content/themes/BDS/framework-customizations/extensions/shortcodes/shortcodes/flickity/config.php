<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Flickity', 'fw' ),
	'description' => __( 'Add a Flickity', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);
