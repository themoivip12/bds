<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
?>
<div <?php echo (isset( $atts['custom_class'] ) && $atts['custom_class']) ? 'class="' . $atts['custom_class'] . '" ' : ''; ?>data-flickity='{"pageDots": <?php echo (isset( $atts['page_dots'] ) && $atts['page_dots']) ? 'true' : 'false'; ?>,"prevNextButtons": <?php echo (isset( $atts['prev_next_buttons'] ) && $atts['prev_next_buttons']) ? 'true' : 'false'; ?>,"cellAlign": "<?php echo (isset( $atts['cell_align'] ) && $atts['cell_align']) ? $atts['cell_align'] : ''; ?>", "wrapAround": <?php echo (isset( $atts['wrap_around'] ) && $atts['wrap_around']) ? 'true' : 'false'; ?>, "imagesLoaded": <?php echo (isset( $atts['images_loaded'] ) && $atts['images_loaded']) ? 'true' : 'false'; ?>,"contain": <?php echo (isset( $atts['contain'] ) && $atts['contain']) ? 'true' : 'false'; ?><?php echo (isset( $atts['custom_option'] ) && $atts['custom_option']) ? ','.$atts['custom_option'] : ''; ?>}'>
	<?php foreach ( fw_akg( 'slides', $atts, array() ) as $tab ) : ?>
  <div class="<?php echo (isset( $atts['cell_class'] ) && $atts['cell_class']) ? $atts['cell_class'] : 'col-12 col-md-4 px-1'; ?>">
		<?php echo do_shortcode( $tab['slide_content'] ); ?>
  </div>
	<?php endforeach; ?>
</div>
