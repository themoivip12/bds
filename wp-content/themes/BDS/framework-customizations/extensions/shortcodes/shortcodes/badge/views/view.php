<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$extra_classes = '';
if ( isset( $atts['color'] ) && $atts['color'] ) {
	$extra_classes .= ' badge-' . esc_attr($atts['color']);
}
if ( isset( $atts['is_large'] ) && $atts['is_large'] ) {
	$extra_classes .= ' badge-lg';
}
if ( isset( $atts['is_circle'] ) && $atts['is_circle'] ) {
	$extra_classes .= ' badge-rounded-circle';
}
if ( isset( $atts['is_large'] ) && $atts['is_large'] ) {
	$extra_classes .= ' badge-lg';
}
if ( isset( $atts['float'] ) && $atts['float'] ) {
	$extra_classes .= ' badge-float badge-float-' . esc_attr($atts['float']);
}
if ( isset( $atts['class'] ) && $atts['class'] ) {
	$extra_classes .= ' ' . esc_attr($atts['class']);
}
?>
<div class="badge<?php echo esc_attr($extra_classes) ?>">
	<?php echo $atts['message']; ?>
</div>
