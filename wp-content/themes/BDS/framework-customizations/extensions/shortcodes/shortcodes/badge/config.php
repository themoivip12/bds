<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Badge', 'fw' ),
	'description' => __( 'Add a Badge', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);
