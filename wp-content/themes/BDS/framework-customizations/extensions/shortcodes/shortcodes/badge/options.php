<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'message' => array(
		'label' => __( 'Message', 'fw' ),
		'type'  => 'text',
	),
	'color'   => array(
		'label'   => __( 'Color', 'fw' ),
		'type'    => 'select',
		'choices' => array(
			'primary'    => __( 'Primary', 'fw' ),
			'secondary'    => __( 'Secondary', 'fw' ),
			'success' => __( 'Success', 'fw' ),
			'info'    => __( 'Info', 'fw' ),
			'warning' => __( 'Warning', 'fw' ),
			'danger'  => __( 'Danger', 'fw' ),
			'dark'    => __( 'Dark', 'fw' ),
			'light'    => __( 'Light', 'fw' ),
			'primary-soft'    => __( 'Primary Soft', 'fw' ),
			'secondary-soft'    => __( 'Secondary Soft', 'fw' ),
			'success-soft' => __( 'Success Soft', 'fw' ),
			'info-soft'    => __( 'Info Soft', 'fw' ),
			'warning-soft' => __( 'Warning Soft', 'fw' ),
			'danger-soft'  => __( 'Danger Soft', 'fw' ),
			'dark-soft'    => __( 'Dark Soft', 'fw' ),
			'light-soft'    => __( 'Light Soft', 'fw' ),
		)
	),
	'is_large' => array(
		'label'        => __('Large', 'fw'),
		'type'         => 'switch',
	),
	'is_circle' => array(
		'label'        => __('Circle', 'fw'),
		'type'         => 'switch',
	),
	'float'   => array(
		'label'   => __( 'Float', 'fw' ),
		'type'    => 'select',
		'choices' => array(
			''    => __( 'No Float', 'fw' ),
			'inside'    => __( 'Float Inside', 'fw' ),
			'outside' => __( 'Float Outside', 'fw' ),
		)
	),
	'class' => array(
		'label'        => __('Custom Class', 'fw'),
		'type'         => 'text',
	)
);
