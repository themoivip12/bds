<?php if (!defined('FW')) die('Forbidden');

/** @internal */
function _filter_disable_shortcodes($to_disable)
{
	//$to_disable[] = 'accordion';
	$to_disable[] = 'calendar';
	$to_disable[] = 'call_to_action';
	$to_disable[] = 'contact_form';
	$to_disable[] = 'divider';
	$to_disable[] = 'icon_box';
	$to_disable[] = 'map';
	$to_disable[] = 'table';
	//$to_disable[] = 'tabs';
	$to_disable[] = 'team_member';
	$to_disable[] = 'hero';
	$to_disable[] = 'feature';
	$to_disable[] = 'testimonials';
	return $to_disable;
}
add_filter('fw_ext_shortcodes_disable_shortcodes', '_filter_disable_shortcodes');
