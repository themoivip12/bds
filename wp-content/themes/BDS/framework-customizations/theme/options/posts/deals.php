<?php if (!defined( 'FW' )) die('Forbidden');

$options = array(
	'main' => array(
		'type' => 'box',
		'priority' => 'high',
		'title' => __('Deals Options', 'remote'),
		'options' => array(
			'logo' => array(
				'type'  => 'upload',
				'desc' => 'Kích cỡ: 480x220px',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:logo',
				)
			),
			'url' => array(
				'label' => 'Website',
				'type'  => 'text',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:url',
				)
			),
			'email' => array(
				'label' => 'Email',
				'type'  => 'text',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:email',
				)
			),
			'phone' => array(
				'label' => 'Phone',
				'type'  => 'text',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:phone',
				)
			),
			'is_featured' => array(
				'label'   => 'Nổi Bật',
				'type'    => 'switch',
				'value'   => false,
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:is_featured',
				)
			),
			'description' => array(
				'label' => 'Mô tả ngắn gọn về doanh nghiệp',
				'type' => 'textarea',
				'desc' => 'Dưới 120 ký tự',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:description',
				)
			),
			'uudai_tomtat' => array(
				'label' => 'Tóm tắt ưu đãi',
				'type'  => 'text',
				'desc' => 'Dưới 25 ký tự',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:uudai_tomtat',
				)
			),
			'uudai_cuthe' => array(
				'label' => 'Nội dung ưu đãi cụ thể',
				'type'  => 'wp-editor',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:uudai_cuthe',
				)
			),
			'uudai_cachthuc' => array(
				'label' => 'Cách thức nhận ưu đãi',
				'type'  => 'wp-editor',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:uudai_cachthuc',
				)
			),
			'setting_cachthuc' => array(
				'type'    => 'select',
				'label'   => 'Cài đặt cách thức',
				'choices' => array(
					'default' => 'Default',
					'coupon' => 'Coupon'
				),
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:setting_cachthuc',
				)
			),
			'coupon_list' => array(
				'label' => 'Danh Sách Coupon',
				'type' => 'textarea',
				'desc' => 'Mỗi Coupon 1 dòng',
				'fw-storage' => array(
					'type' => 'post-meta',
					'post-meta' => 'fw_option:coupon_list',
				)
			),
		),
	),
);
