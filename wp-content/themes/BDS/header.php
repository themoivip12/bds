<!DOCTYPE html>
<html class="h-100" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162245174-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-162245174-4');
</script>
</head>

<body <?php body_class( 'h-100' ); ?>>
<?php if ( is_home() ) : ?>
<header id="header" class="header header-box-shadow-on-scroll header-abs-top header-bg-transparent header-show-hide"
data-hs-header-options='{
	"fixMoment": 1000,
	"fixEffect": "slide"
}'>
<?php else : ?>
<header class="header border-bottom">
<?php endif; ?>
	<div class="header-section">
		<div class="container">
			<div class="navbar navbar-expand-lg">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand py-3">
					<img src="<?php echo get_template_directory_uri() . '/assets/logo.svg'; ?>" title="<?php bloginfo('name'); ?>" style="height: 40px; width: auto; float: left;">
					<span class="small border-left text-primary" style="width: 160px; white-space: normal; display: inline-block; font-size: 12px; padding-left: 20px; margin-left: 20px;">Đồng hành cùng #VRW hỗ trợ doanh nghiệp.</span>
				</a>
				<button type="button" class="navbar-toggler btn btn-icon btn-sm rounded-circle"
								aria-label="Toggle navigation"
								aria-expanded="false"
								aria-controls="navBar"
								data-toggle="collapse"
								data-target="#navBar">
					<span class="navbar-toggler-default">
						<svg width="14" height="14" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
							<path fill="currentColor" d="M17.4,6.2H0.6C0.3,6.2,0,5.9,0,5.5V4.1c0-0.4,0.3-0.7,0.6-0.7h16.9c0.3,0,0.6,0.3,0.6,0.7v1.4C18,5.9,17.7,6.2,17.4,6.2z M17.4,14.1H0.6c-0.3,0-0.6-0.3-0.6-0.7V12c0-0.4,0.3-0.7,0.6-0.7h16.9c0.3,0,0.6,0.3,0.6,0.7v1.4C18,13.7,17.7,14.1,17.4,14.1z"/>
						</svg>
					</span>
					<span class="navbar-toggler-toggled">
						<svg width="14" height="14" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
							<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z"/>
						</svg>
					</span>
				</button>
				<nav id="navBar" class="collapse navbar-collapse" role="navigation">
					<div class="navbar-body header-abs-top-inner d-lg-flex">
						<?php if ( has_nav_menu( 'primary' ) ) : ?>
							<?php
								wp_nav_menu( array(
									'container' => false,
									'theme_location' => 'primary',
									'menu_class' => 'navbar-nav',
									'depth' => 2,
									'fallback_cb' => 'remote_navwalker::fallback',
									'walker' => new remote_navwalker()
								) );
							?>
						<?php endif; ?>
						<ul class="navbar-nav ml-0">
							<li class="navbar-nav-last-item">
								<button class="btn btn-sm btn-primary transition-3d-hover" data-toggle="modal" data-target="#dangKyNhan">Đăng ký</button>
							</li>
						</ul>
					</div>
				</nav>
			</div>
			</div>
	</div>
</header>

<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main">
