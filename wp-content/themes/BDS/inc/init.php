<?php
if (!isset($content_width)) {
	$content_width = 750;
}

if (!function_exists('remote_setup')) :
	function remote_setup()
	{
		load_theme_textdomain('remote', get_template_directory() . '/languages');

		add_theme_support('automatic-feed-links');

		add_theme_support('title-tag');

		add_theme_support('post-thumbnails');

		register_nav_menus(array(
			'primary' => __('Primary Menu', 'remote'),
		));

		add_theme_support('html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		));

		add_theme_support('post-formats', array(
			'aside', 'image', 'video', 'quote', 'link',
		));
	}
endif;
add_action('after_setup_theme', 'remote_setup');

function remote_widgets_init()
{
	register_sidebar(array(
		'name'          => __('Sidebar', 'remote'),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s my-4">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title h6">',
		'after_title'   => '</h3>',
	));
}
add_action('widgets_init', 'remote_widgets_init');

function remote_search_form_modify($html)
{
	$html = str_replace('<label', '<label class="sr-only"', $html);
	$html = str_replace('</label>', '', $html);
	$html = str_replace('</span>', '</span></label>', $html);
	$html = str_replace('screen-reader-text', 'sr-only', $html);
	$html = str_replace('search-field', 'form-control', $html);
	$html = str_replace('search-submit', 'sr-only', $html);
	return $html;
}
add_filter('get_search_form', 'remote_search_form_modify');

function remote_json_custom_fields($data, $post, $request)
{
	$_data = $data->data;
	$_data['logo'] = get_post_meta($post->ID, 'fw_option:logo', true);
	$_data['website'] = get_post_meta($post->ID, 'fw_option:url', true);
	$_data['description'] = get_post_meta($post->ID, 'fw_option:description', true);
	$_data['uudai_tomtat'] = get_post_meta($post->ID, 'fw_option:uudai_tomtat', true);
	$data->data = $_data;
	return $data;
}
add_filter('rest_prepare_deals', 'remote_json_custom_fields', 10, 3);

/*thang function*/
function remotevn_get_current_url()
{
	global $wp;

	//
	$url = home_url($wp->request);

	$params = array(
		'keyword', 'category', 'order'
	);
	foreach ($params as $param) {
		if (isset($_GET[$param])) {
			$url = add_query_arg(array($param => esc_html($_GET[$param])), $url);
		}
	}

	return $url;
}

//start session
add_action('init', 'remote_start_session', 10);
function remote_start_session()
{
	if (!session_id()) {
		session_start();
	}
}

add_action('init', 'remote_check_email_deal', 10);
function remote_check_email_deal()
{
	if (isset($_GET['email']) && is_email($_GET['email'])) {
		$email = $_GET['email'];
		if (remote_workevo_email_valid($email)) {
			$_SESSION['remote_email'] = $email;
			$_SESSION['deal_permision'] = 1;
		} else {
			$_SESSION['remote_email'] = '';
			$_SESSION['deal_permision'] = 0;
		}

		//wp_safe_redirect(home_url('/'));
		//exit();
	}
}



function remote_check_permission_deal()
{
	if (isset($_SESSION['remote_email']) && $_SESSION['remote_email'] && isset($_SESSION['deal_permision']) && $_SESSION['deal_permision']) {
		return true;
	}

	return false;
}


function remote_workevo_email_valid($email = false)
{
	if (!$email) {
		return false;
	}

	$url = 'https://remotevn.workevo.io/contacts';
	$url = add_query_arg(array('email' => $email), $url);

	$args_for_get = array(
		'headers' => array(
			'Authorization' => ' Bearer qxn8jVs2dn96TvrwF8QEEh3k2dsRQr3JTn9P',
		)
	);

	$res = wp_remote_get($url, $args_for_get);
	if ($res && isset($res['response']) && isset($res['response']['code'])) {
		if ($res['response']['code'] == 200) {
			return true;
		}
	}

	return false;
}

add_action("wp", "remote_super_redirect");
function remote_super_redirect()
{
	if (is_home()) {
		if (isset($_GET["email"])) {
			wp_safe_redirect(home_url("uu-dai?email=" . sanitize_email($_GET["email"])));
		}
	}
}

function wp_get_shares( $post_id ) {
	//$cache_key = 'fb_shares_' . $post_id;
	//$access_token = '1756400394681806|0308e63c8c087928ad1bf66c3dc69f1c';
	//$count = get_transient( $cache_key ); // try to get value from WordPress cache
	$count= false;
	// if no value in the cache
	if ( $count === false ) {
		$response = wp_remote_get( add_query_arg( array(
			'id' => urlencode( get_permalink( $post_id ) ),
			'fields' => 'og_object{engagement}'
		), 'https://graph.facebook.com/' ) );
		$body = json_decode( $response['body'] );
		$count = intval( $body->og_object->engagement->count );

		//set_transient( $cache_key, $count, 3600 ); // store value in cache for a 1 hour
	}
	return $count;
}

// add_action('save_post', function ($postId) {
// 	add_post_meta($postId, 'fb_shares', 0, true);
// });
