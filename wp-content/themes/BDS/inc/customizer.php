<?php
function remote_customize_register( $wp_customize ) {
}
add_action( 'customize_register', 'remote_customize_register' );

function remote_get_theme_option( $option_name, $default = '' ) {
  $options = get_option( 'remote_theme_options' );
  if( isset($options[$option_name]) ) {
    return $options[$option_name];
  }
  return $default;
}
