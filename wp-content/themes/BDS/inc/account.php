<?php
/*-----------------------------------------------------------------------------------*/
/*  Account Modal
/*-----------------------------------------------------------------------------------*/
add_action( 'wp_footer', 'remote_account_modal' );
function remote_account_modal() {
	if ( ! is_user_logged_in() ) :
		?>
	<div id="accountModal" class="modal fade account-modal" tabindex="-1" role="dialog" aria-labelledby="accountModalTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="tab-content">
					<div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
						<div class="modal-header">
							<h5 class="modal-title" id="accountModalTitle">Đăng nhập</h5>
							<button type="button" class="btn btn-xs btn-icon btn-soft-secondary" data-dismiss="modal" aria-label="Close">
								<svg aria-hidden="true" width="10" height="10" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
									<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z"/>
								</svg>
							</button>
						</div>
						<div class="modal-body">
							<?php remote_ajax_login_form(); ?>
							<p class="text-small mt-4 mb-0 text-center">Bạn chưa có tài khoản? <a id="register-tab" data-toggle="pill" href="#register" role="tab" aria-controls="register" aria-selected="true" class="modal-form-change">Đăng ký ngay</a></p>
						</div>
					</div>
					<div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
						<div class="modal-header">
							<h5 class="modal-title" id="accountModalTitle">Đăng ký tài khoản</h5>
							<button type="button" class="btn btn-xs btn-icon btn-soft-secondary" data-dismiss="modal" aria-label="Close">
								<svg aria-hidden="true" width="10" height="10" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
									<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z"/>
								</svg>
							</button>
						</div>
						<div class="modal-body">
							<?php remote_ajax_register_form(); ?>
							<p class="text-small mt-3 mb-0 text-center">Bạn đã có tài khoản? <a id="login-tab" data-toggle="pill" href="#login" role="tab" aria-controls="login" aria-selected="true" class="modal-form-change">Đăng nhập</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif;
}

/*-----------------------------------------------------------------------------------*/
/*  Provides an ajax login form for use anywhere within WordPress.
/*-----------------------------------------------------------------------------------*/
function remote_ajax_login_form( $args = array() ) {
	$defaults = array(
		'echo' => true,
		'referer' => $_SERVER['REQUEST_URI'], // Default redirect is back to the current page
		'label_username' => __( 'Email' ),
		'label_password' => __( 'Mật khẩu' ),
		'label_remember' => __( 'Ghi nhớ tài khoản' ),
		'label_log_in' => __( 'Đăng nhập' ),
		'id_submit' => 'wp-submit',
		'remember' => true,
		'value_remember' => true, // Set this to true to default the "Remember me" checkbox to checked
	);
	$args = wp_parse_args( $args, apply_filters( 'ajax_login_form_defaults', $defaults ) );

	$form = '
		<form name="loginform" id="ajaxloginform" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
			<p class="alert alert-success d-none mb-3"></p>
			<div class="login-username form-group">
				<label for="login_user_login">' . esc_html( $args['label_username'] ) . '</label>
				<input type="text" name="username" id="login_user_login" class="form-control" />
			</div>
			<div class="login-password form-group">
				<label for="login_user_pass">' . esc_html( $args['label_password'] ) . '</label>
				<input type="password" name="password" id="login_user_pass" class="form-control" />
			</div>
			<div class="d-flex justify-content-between mb-4">
				' . wp_nonce_field( 'ajax-login-nonce', 'security', true, false) . '
				' . ( $args['remember'] ? '<div class="custom-control custom-checkbox">	<input class="custom-control-input" name="rememberme" type="checkbox" id="rememberme" value="forever"' . ( $args['value_remember'] ? ' checked="checked"' : '' ) . ' /><label class="custom-control-label" for="rememberme">' . esc_html( $args['label_remember'] ) . '</label></div>' : '' ) . '
				<a href="'.wp_lostpassword_url( get_bloginfo('url') ).'">Bạn quên mật khẩu?</a>
			</div>
			<button type="submit" name="wp-submit" id="' . esc_attr( $args['id_submit'] ) . '" class="btn btn-block btn-primary">' . esc_attr( $args['label_log_in'] ) . '</button>
			<input type="hidden" value="' . esc_attr( $args['referer'] ) . '" name="referer" />
			<input type="hidden" name="action" value="ajaxlogin" />
		</form>';

	if ( $args['echo'] )
		echo $form;
	else
		return $form;
}

/*-----------------------------------------------------------------------------------*/
/*  Ajax Login Process
/*-----------------------------------------------------------------------------------*/
add_action( 'wp_ajax_nopriv_ajaxlogin', 'remote_ajax_login' );
function remote_ajax_login(){
	check_ajax_referer( 'ajax-login-nonce', 'security' );
	$info = array();
	$info['user_login'] = $_POST['username'];
	$info['user_password'] = $_POST['password'];
	$info['remember'] = (isset($_POST['rememberme']))?true:false;
	$info['referer'] = $_POST['referer'];

	$user_signon = wp_signon( $info, false );
	if ( is_wp_error($user_signon) ){
			echo json_encode( array( 'loggedin' => false, 'message' => __('Wrong username or password', 'remote')));
	} else {
			echo json_encode( array( 'loggedin' => true, 'referer' => $info['referer'], 'message' =>__('Login successful, redirecting...', 'remote')));
	}

	die();
}

/*-----------------------------------------------------------------------------------*/
/*  Provides an ajax register form for use anywhere within WordPress.
/*-----------------------------------------------------------------------------------*/
function remote_ajax_register_form( $args = array() ) {
	$defaults = array(
		'echo' => true,
		'referer' => $_SERVER['REQUEST_URI'],
		'label_fname' => __( 'Tên của bạn' ),
		'label_lname' => __( 'Chức vụ' ),
		'label_company' => __( 'Tên công ty' ),
		'label_email' => __( 'Email công ty' ),
		'label_phone' => __( 'Số điện thoại' ),
		'label_password' => __( 'Mật khẩu' ),
		'label_sign_up' => __( 'Đăng ký' ),
		'id_submit' => 'wp-submit',
	);
	$args = wp_parse_args( $args, apply_filters( 'ajax_register_form_defaults', $defaults ) );

	$form = '
		<form name="registerform" id="ajaxregisterform" action="" method="post">
			<p class="alert alert-success d-none mb-3"></p>
			<div class="row">
				<div class="col-6">
					<div class="register-fname form-group">
						<label for="fname">' . esc_html( $args['label_fname'] ) . '</label>
						<input type="text" name="fname" id="fname" class="form-control" />
					</div>
				</div>
				<div class="col-6">
					<div class="register-lname form-group">
							<label for="lname">' . esc_html( $args['label_lname'] ) . '</label>
							<input type="text" name="lname" id="lname" class="form-control" />
					</div>
				</div>
			</div>
			<div class="register-company form-group">
				<label for="user_name">' . esc_html( $args['label_company'] ) . '</label>
				<input type="text" name="company" id="company" class="form-control" />
			</div>
			<div class="register-email form-group">
				<label for="user_email">' . esc_html( $args['label_email'] ) . '</label>
				<input type="text" name="email" id="user_email" class="form-control" />
			</div>
			<div class="register-phone form-group">
				<label for="user_phone">' . esc_html( $args['label_phone'] ) . '</label>
				<input type="text" name="phone" id="user_phone" class="form-control" />
			</div>
			<div class="register-password form-group mb-4">
				<label for="user_pass">' . esc_html( $args['label_password'] ) . '</label>
				<input type="password" name="password" id="user_pass" class="form-control" />
			</div>
			' . wp_nonce_field( 'ajax-register-nonce', 'register_security', true, false) . '
			<button type="submit" name="wp-submit" id="register-' . esc_attr( $args['id_submit'] ) . '" class="btn btn-block btn-primary">' . esc_attr( $args['label_sign_up'] ) . '</button>
			<input type="hidden" value="' . esc_attr( $args['referer'] ) . '" name="referer" />
			<input type="hidden" name="action" value="ajaxregister" />
		</form>';

	if ( $args['echo'] )
		echo $form;
	else
		return $form;
}

/*-----------------------------------------------------------------------------------*/
/*  Ajax Register Process
/*-----------------------------------------------------------------------------------*/
add_action( 'wp_ajax_nopriv_ajaxregister', 'maki_ajax_register' );
function maki_ajax_register(){

	check_ajax_referer( 'ajax-register-nonce', 'register_security' );

	$info = array();
	$info['first_name'] = $_POST['fname'];
	$info['last_name'] = $_POST['lname'];
	$info['user_email'] = $_POST['email'];
	$info['user_login'] = $_POST['username'];
	$info['user_pass'] = $_POST['password'];
	$info['referer'] = $_POST['referer'];

	if ( empty($_POST['fname']) ) {
		echo json_encode( array( 'registered' => false, 'message' => 'Please enter a first name.' ));
	} else if ( empty($_POST['lname']) ) {
		echo json_encode( array( 'registered' => false, 'message' => 'Please enter a last name.' ));
	} else if ( empty($_POST['username']) ) {
		echo json_encode( array( 'registered' => false, 'message' => 'Please enter an username.' ));
	} else if ( strlen($_POST['username']) < 4 ) {
		echo json_encode( array( 'registered' => false, 'message' => 'Username must be at least 4 characters.' ));
	} else if ( username_exists($_POST['username']) == true ) {
		echo json_encode( array( 'registered' => false, 'message' => 'This username is already exists.' ));
	} else if ( empty($_POST['email']) ) {
		echo json_encode( array( 'registered' => false, 'message' => 'Please enter an email.' ));
	} else if ( is_email($_POST['email']) == false ) {
		echo json_encode( array( 'registered' => false, 'message' => 'The email address isn’t correct.' ));
	} else if ( email_exists($_POST['email']) == true ) {
		echo json_encode( array( 'registered' => false, 'message' => 'This email is already exists.' ));
	} else if ( empty($_POST['password']) ) {
		echo json_encode( array( 'registered' => false, 'message' => 'Please enter a password.' ));
	} else {
		$user_id = wp_insert_user( $info );
		wp_set_auth_cookie( $user_id, false, is_ssl() );
		wp_new_user_notification( $user_id, $info['user_pass'] );

		echo json_encode( array( 'registered' => true, 'referer' => $info['referer'], 'message' => 'Register successful, redirecting...') );
	}
	die();
}
