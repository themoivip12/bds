<?php
add_action( 'init', 'remote_post_types_init' );
function remote_post_types_init() {

	$deals_labels = array(
		'name'               => _x( 'Ưu Đãi', 'post type general name', 'remote' ),
		'singular_name'      => _x( 'Ưu Đãi', 'post type singular name', 'remote' ),
		'menu_name'          => _x( 'Ưu Đãi', 'admin menu', 'remote' ),
		'name_admin_bar'     => _x( 'Ưu Đãi', 'add new on admin bar', 'remote' ),
		'add_new'            => _x( 'Thêm', 'customer', 'remote' ),
		'add_new_item'       => __( 'Thêm Ưu Đãi', 'remote' ),
		'new_item'           => __( 'Ưu Đãi Mới', 'remote' ),
		'edit_item'          => __( 'Sửa Ưu Đãi', 'remote' ),
		'view_item'          => __( 'Xem Ưu Đãi', 'remote' ),
		'all_items'          => __( 'Tất Cả Ưu Đãi', 'remote' ),
		'search_items'       => __( 'Tìm kiếm Ưu Đãi', 'remote' ),
		'parent_item_colon'  => __( 'Ưu Đãi:', 'remote' ),
		'not_found'          => __( 'Không tìm thấy ưu đãi nào.', 'remote' ),
		'not_found_in_trash' => __( 'Không tìm thấy ưu đãi nào trong thùng rác.', 'remote' )
	);

	$deals_args = array(
		'labels'             => $deals_labels,
		'description'        => __( '', 'remote' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'uu-dai', 'with_front' => false ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'          => 'dashicons-tickets-alt',
		'show_in_rest'       => true,
		'rest_base'          => 'deals',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'custom-fields' )
	);

	register_post_type( 'deals', $deals_args );
}

add_action( 'init', 'remote_custom_taxonomies', 0 );

function remote_custom_taxonomies() {
	$labels = array(
		'name'              => _x( 'Giải Pháp', 'taxonomy general name', 'remote' ),
		'singular_name'     => _x( 'Giải Pháp', 'taxonomy singular name', 'remote' ),
		'search_items'      => __( 'Tìm Giải Pháp', 'remote' ),
		'all_items'         => __( 'Tất Cả Giải Pháp', 'remote' ),
		'parent_item'       => __( 'Giải Pháp Cha', 'remote' ),
		'parent_item_colon' => __( 'Giải Pháp:', 'remote' ),
		'edit_item'         => __( 'Sửa Giải Pháp', 'remote' ),
		'update_item'       => __( 'Cập Nhật Giải Pháp', 'remote' ),
		'add_new_item'      => __( 'Thêm Giải Pháp Mới', 'remote' ),
		'new_item_name'     => __( 'Thêm Tên Giải Pháp', 'remote' ),
		'not_found'         => __( 'Không tìm thấy giải pháp nào.', 'remote' ),
		'menu_name'         => __( 'Giải Pháp', 'remote' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'giai-phap', 'with_front' => false ),
		'show_in_rest'          => true,
    'rest_base'             => 'types',
    'rest_controller_class' => 'WP_REST_Terms_Controller',
	);
	register_taxonomy( 'deals-type', array( 'deals' ), $args );
}
