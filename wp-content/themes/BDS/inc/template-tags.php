<?php
if ( ! function_exists( 'remote_entry_meta' ) ) :
function remote_entry_meta() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated sr-only" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		_x( '<i class="fa fa-calendar mr-1 text-muted"></i> %s', 'post date', 'remote' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		_x( '<i class="fa fa-user mr-1 text-muted"></i> %s', 'post author', 'remote' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline ml-3"> ' . $byline . '</span>';

	$categories_list = get_the_category_list( __( ', ', 'remote' ) );
	if ( $categories_list && remote_categorized_blog() ) {
		printf( '<span class="cat-links ml-3">' . __( '<i class="fa fa-folder"></i> %1$s', 'remote' ) . '</span>', $categories_list );
	}

	if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link ml-3">';
		echo '<i class="fa fa-comments mr-1 text-muted"></i>';
		comments_popup_link( __( '0 Comments', 'remote' ), __( '1 Comment', 'remote' ), __( '% Comments', 'remote' ) );
		echo '</span>';
	}

}
endif;

if ( ! function_exists( 'remote_entry_footer' ) ) :
function remote_entry_footer() {
	if ( 'post' == get_post_type() ) {
		echo '<footer class="entry-footer">';
		$tags_list = get_the_tag_list( '', __( ', ', 'remote' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . __( 'Tagged %1$s', 'remote' ) . '</span>', $tags_list );
		}
		echo '</footer>';
	}
}
endif;

if ( ! function_exists( 'remote_posts_navigation' ) ) :
	function remote_posts_navigation( $wp_query = null, $echo = true ) {
		if ( null === $wp_query ) {
			global $wp_query;
		}

		$pages = paginate_links( [
				'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
				'format'       => '?paged=%#%',
				'current'      => max( 1, get_query_var( 'paged' ) ),
				'total'        => $wp_query->max_num_pages,
				'type'         => 'array',
				'show_all'     => false,
				'end_size'     => 3,
				'mid_size'     => 1,
				'prev_next'    => true,
				'prev_text'    => __( 'Previous', 'remote' ),
				'next_text'    => __( 'Next', 'remote' ),
				'add_args'     => false,
				'add_fragment' => ''
			]
		);

		if ( is_array( $pages ) ) {
			$pagination = '<ul class="pagination justify-content-center mb-0">';

			foreach ($pages as $page) {
				$pagination .= '<li class="page-item' . (strpos($page, 'current') !== false ? ' active' : '') . '"> ' . str_replace('page-numbers', 'page-link', $page) . '</li>';
			}
			$pagination .= '</ul>';

			if ( $echo ) {
				echo $pagination;
			} else {
				return $pagination;
			}
		}

		return null;
	}
endif;

function remote_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'remote_categories' ) ) ) {
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			'number'     => 2,
		) );

		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'remote_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		return true;
	} else {
		return false;
	}
}

function remote_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	delete_transient( 'remote_categories' );
}
add_action( 'edit_category', 'remote_category_transient_flusher' );
add_action( 'save_post',     'remote_category_transient_flusher' );

if ( ! function_exists( 'remote_breadcrumbs' ) ) :
function remote_breadcrumbs() {
	if ( function_exists( 'yoast_breadcrumb' ) ) {
		yoast_breadcrumb( '<div class="breadcrumbs">', '</div>' );
	}
}
endif;

if ( ! function_exists( 'remote_primary_column_class' ) ) :
function remote_primary_column_class() {
	echo 'col-sm-8';
}
endif;

if ( ! function_exists( 'remote_secondary_column_class' ) ) :
function remote_secondary_column_class() {
	echo 'col-sm-4';
}
endif;
