<?php get_header();
/* ?keyword=designbold&category=marketing&order=new&page=1 */
$current_url = remotevn_get_current_url();

$args = array(
	'post_type' => 'deals',
	'post_status' => 'publish',
	'meta_key' => 'fb_shares',
	'orderby' => 'meta_value_num',
	'order' => 'DESC',
	'paged' => get_query_var( 'paged' )
);
if ( isset( $_GET['keyword'] ) ) {
	$args['s'] = esc_html( $_GET['keyword'] );
	$keyword = esc_html( $_GET['keyword'] );
} else {
	$keyword = '';
}
if ( isset( $_GET['page'] ) ) {
	$args['paged'] = esc_html( $_GET['page'] );
}
$taxonomy = get_query_var( 'taxonomy' );
$current_term = get_term_by( 'slug', get_query_var( 'term' ), $taxonomy );
if ( $taxonomy ) {
	$args['tax_query'] = array(
		array(
			'taxonomy' => 'deals-type',
			'field'    => 'slug',
			'terms'    => $current_term->slug
		)
	);
}

// remote_workevo_email_valid('maiht@novaon.vn');

global $wp_query;
$wp_query = new WP_Query($args);
$terms = get_terms( 'deals-type', array(
	'hide_empty' => false,
) );
?>
<div class="header text-center pb-5">
	<div class="container">
		<div class="pt-6 pb-4 w-75 mx-auto">
			<h1>Ưu đãi mùa dịch Covid-19</h1>
			<p class="mb-5">Dưới đây là danh sách gói cứu trợ mà VRW đã tổng hợp được. Hãy nhấn vào gói cứu trợ mà bạn muốn nhận và xem hướng dẫn của từng bên. Chúng tôi không thể hỗ trợ về mặt kỹ thuật trong quá trình sử dụng sản phẩm, vì vậy nếu bạn có thắc mắc gì xin hãy liên hệ trực tiếp với hỗ trợ khách hàng của các bên.</p>
			<form method="GET" action="<?php echo $current_url;?>" class="row">
				<div class="col px-1">
					<div class="form-group">
						<input type="search" name="keyword" class="form-control" placeholder="Từ khóa cần tìm" value="<?php echo ( isset( $_GET['keyword'] ) ) ? esc_html( $_GET['keyword'] ) : ""; ?>">
						<!-- <i class="fas fa-search position-absolute" style="top: 15%;left: 3%;transform: translate(20%,10%);"></i> -->
					</div>
				</div>
				<div class="col-auto px-1">
					<button type="submit" class="btn btn-primary">Tìm kiếm</button>
				</div>
			</form>
			<div class="text-center">
				<a href="<?php echo home_url() . '/uu-dai/'; ?>" class="btn btn-soft-primary btn-xs btn-pill m-1 <?php echo $current_term ? '' : ' active'; ?>">
					Tất cả
				</a>
				<?php
				$terms = get_terms( 'deals-type', array(
					'hide_empty' => false,
				) );
				if($terms):
					foreach($terms as $term):
				?>
					<a href="<?php echo esc_url( get_term_link($term->slug, 'deals-type') ); ?>" class="btn btn-soft-primary btn-xs btn-pill m-1<?php echo $current_term->slug === $term->slug ? ' active' : ''; ?>">
						<?php echo $term->name;?>
					</a>
				<?php
					endforeach;
				endif;
				?>
			</div>
		</div>
	</div>
</div>
<div class="content">
	<div class="container">
		<div class="card border mb-5 shadow-none">
			<!-- <div class="card-header d-flex">
				<strong>Danh sách ưu đãi</strong>
				<span class="ml-auto">Sắp xếp theo:</span>
				<div class="dropdown ml-1">
					<a href="#" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Nổi bật <i class="fas fa-angle-down"></i>
					</a>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="#">Mới nhất</a>
						<a class="dropdown-item" href="#">Xem nhiều</a>
					</div>
				</div>
			</div> -->
			<div class="card-body">
				<?php if ( have_posts() ) : ?>
					<ul class="list-group list-group-flush my-n4">
						<?php
						while (have_posts()) {
							the_post();
							$metas = fw_get_db_post_option(get_the_ID());
							?>
							<li class="list-group-item px-0 py-4">
								<div class="row align-items-center">
									<div class="col-lg-3 col-md-12 mb-4 mb-lg-0">
										<a href="<?php the_permalink(); ?>">
											<?php if(isset($metas['logo']['url']) && $metas['logo']['url']):?>
												<img class="rounded-sm border w-100 h-auto" src="<?php echo $metas['logo']['url'];?>">
											<?php endif; ?>
										</a>
									</div>
									<div class="col-xl-7 col-lg-6 col-md-8">
										<h2 class="mb-2 h4"><a href="<?php the_permalink(); ?>" class="text-dark"><?php the_title();?></a></h2>
										<p class="small mb-2 text-truncate"><?php echo $metas['description'];?></p>
										<p class="mb-0 text-primary d-flex align-items-center"><span class="badge badge-primary rounded-sm mr-2">Ưu đãi</span> <small><?php echo $metas['uudai_tomtat'];?></small></p>
									</div>
									<div class="col-xl-2 col-lg-3 col-md-4 mt-4 mt-md-0">
										<div class="text-center">
											<a class="btn btn-outline-primary btn-sm btn-block mb-3" href="<?php the_permalink(); ?>">Xem chi tiết</a>
											<div class="ml-4">
												<iframe src="https://www.facebook.com/plugins/share_button.php?href=<?php the_permalink(); ?>%2F&layout=button_count&size=small&appId=908194292967228&width=100&height=20" width="100" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
											</div>
										</div>
									</div>
								</div>
							</li>
							<?php
						} ?>
					</ul>
					<?php else : echo '<div class="text-center">Không có ưu đãi nào phù hợp với tiêu chí tìm kiếm của bạn.</div>'; endif; ?>
				</div>
			</div>
			<?php if ( $wp_query->max_num_pages > 1 ) : ?>
				<div class="mb-5"><?php remote_posts_navigation(); ?></div>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
	<?php get_footer();
