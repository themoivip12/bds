<?php get_header(); ?>
<!-- Hero Section -->
<section class="position-relative bg-img-hero"
	style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/svg/components/abstract-shapes-12.svg);">
	<div class="container space-top-3 space-top-lg-4 space-bottom-2 space-bottom-lg-3 position-relative z-index-2">
		<div class="row justify-content-lg-between align-items-md-center">
			<div class="col-md-6 mb-11 mb-md-0">
				<div class="mb-5">
					<h1 class="mb-4 display-3">Làm việc từ xa, đẩy lùi Corona</h1>
					<p class="mr-xl-9">Được Bộ Thông tin và Truyền thông bảo trợ, Cục Tin học hóa đồng hành, Vietnam Remote Workforce xây dựng danh sách các phần mềm và giải pháp hỗ trợ doanh nghiệp Việt trong giai đoạn chống lại dịch Covid-19, giúp bạn vừa tuân theo tiêu chí #stayhome để giữ an toàn cho bản thân và cộng đồng, vừa giữ được kết nối trong doanh nghiệp và làm việc từ xa hiệu quả.</p>
				</div>
				<a class="btn btn-primary btn-wide transition-3d-hover" href="#dangky">Đăng ký ngay</a>
				<a class="btn btn-link btn-wide" href="#thongtin">Tìm hiểu thêm <i
						class="fas fa-angle-right fa-sm ml-1"></i></a>
			</div>

			<div class="col-md-6">
				<div class="row justify-content-end">
					<div class="col-3 mb-4">
						<!-- Logo -->
						<div class="d-block mt-n3 ml-5 bg-primary rounded-circle avatar avatar-xl shadow-sm"
							data-aos="fade-up">
							<img class="avatar avatar-xl rounded-circle" style="opacity: .8" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar-8.jpg" alt="">
						</div>
						<!-- End Logo -->
					</div>
					<div class="col-4 mb-4">
						<!-- Logo -->
						<div class="d-block mx-auto mt-5 bg-primary rounded-circle avatar avatar-xl shadow-sm"
							data-aos="fade-up" data-aos-delay="50">
							<img class="avatar avatar-xl rounded-circle" style="opacity: .8" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar-6.jpg" alt="">
						</div>
						<!-- End Logo -->
					</div>
					<div class="col-4 mb-4">
						<!-- Logo -->
						<div class="d-block ml-auto bg-primary rounded-circle avatar avatar-xl shadow-sm" data-aos="fade-up"
							data-aos-delay="150">
							<img class="avatar avatar-xl rounded-circle" style="opacity: .8" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar-1.jpg" alt="">
						</div>
						<!-- End Logo -->
					</div>
				</div>

				<div class="row">
					<div class="col-3 offset-1 my-4">
						<!-- Logo -->
						<div class="d-block ml-auto bg-primary rounded-circle avatar avatar-xl shadow-sm" data-aos="fade-up"
							data-aos-delay="200">
							<img class="avatar avatar-xl rounded-circle" style="opacity: .8" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar-2.jpg" alt="">
						</div>
						<!-- End Logo -->
					</div>
					<div class="col-3 offset-2 my-4">
						<!-- Logo -->
						<div class="d-block ml-auto bg-primary rounded-circle avatar avatar-xl shadow-sm" data-aos="fade-up"
							data-aos-delay="250">
							<img class="avatar avatar-xl rounded-circle" style="opacity: .8" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar-3.jpg" alt="">
						</div>
						<!-- End Logo -->
					</div>
				</div>

				<div class="row d-none d-md-flex">
					<div class="col-6">
						<!-- Logo -->
						<div class="d-block ml-auto bg-primary rounded-circle avatar avatar-xl shadow-sm" data-aos="fade-up"
							data-aos-delay="300">
							<img class="avatar avatar-xl rounded-circle" style="opacity: .8" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar-4.jpg" alt="">
						</div>
						<!-- End Logo -->
					</div>
					<div class="col-6 mt-6">
						<!-- Logo -->
						<div class="d-block ml-auto bg-primary rounded-circle avatar avatar-xl shadow-sm" data-aos="fade-up"
							data-aos-delay="350">
							<img class="avatar avatar-xl rounded-circle" style="opacity: .8" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar-5.jpg" alt="">
						</div>
						<!-- End Logo -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- SVG Shape -->
	<figure class="position-absolute bottom-0 right-0 left-0">
		<svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1921 273">
			<polygon fill="#fff" points="0,273 1921,273 1921,0 " />
		</svg>
	</figure>
	<!-- End SVG Shape -->
</section>
<!-- End Hero Section -->

<!-- Articles Section -->
<section id="thongtin" class="container space-2">
	<!-- Title -->
	<div class="w-md-75 text-center mx-md-auto px-xl-3 mb-5 mb-md-9">
		<h2 class="h1">Thông tin về chương trình</h2>
		<p>Được Bộ Thông tin và truyền thông bảo trợ, Cục Tin học hóa đồng hành, Vietnam Remote Workforce tổng hợp ưu đãi dành cho các doanh nghiệp trong giai đoạn dịch Codvid-19.</p>
	</div>
	<!-- End Title -->

	<div class="row">
		<div class="col-md-4 mb-5 mb-md-0">
			<!-- Icon Blocks -->
			<div class="text-center px-lg-3">
				<figure class="max-w-10rem mx-auto mb-4">
					<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/svg/icons/icon-26.svg" alt="SVG">
				</figure>
				<h3>Điều kiện nhận ưu đãi</h3>
				<p>Ưu đãi áp dụng cho các doanh nghiệp có trụ sở chính tại Việt Nam, mã số thuế còn hoạt động bình thường.</p>
			</div>
			<!-- End Icon Blocks -->
		</div>

		<div class="col-md-4 mb-5 mb-md-0">
			<!-- Icon Blocks -->
			<div class="text-center px-lg-3">
				<figure class="max-w-10rem mx-auto mb-4">
					<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/svg/icons/icon-7.svg" alt="SVG">
				</figure>
				<h3>Cách thức nhận ưu đãi</h3>
				<p>Đại diện doanh nghiệp điền đầy đủ thông tin vào biểu mẫu phía dưới và chúng tôi sẽ tiến hành gửi ưu đãi qua email.</p>
			</div>
			<!-- End Icon Blocks -->
		</div>

		<div class="col-md-4">
			<!-- Icon Blocks -->
			<div class="text-center px-lg-3">
				<figure class="max-w-10rem mx-auto mb-4">
					<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/svg/icons/icon-30.svg" alt="SVG">
				</figure>
				<h3>Một chút về VRW</h3>
				<p>VRW (Vietnam Remote Workforce) là nơi chia sẻ kinh nghiệm và công nghệ về mô hình làm việc từ xa.</p>
			</div>
			<!-- End Icon Blocks -->
		</div>
	</div>
</section>
<!-- End Articles Section -->
<section id="quote" class="bg-light rounded mx-3 mx-md-11">
	<div class="container space-1 space-md-2">
		<div class="card bg-transparent shadow-none">
			<div class="row">
				<div class="col-lg-3 d-none d-lg-block">
					<div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll bg-light"
						data-options='{direction: "reverse"}' style="overflow: visible;">
						<div
							data-parallaxanimation='[{property: "transform", value:" translate3d(0,{{val}}rem,0)", initial:"4", mid:"0", final:"-4"}]'>
							<img class="img-fluid rounded shadow-lg" src="<?php echo get_template_directory_uri(); ?>/assets/img/bo-truong.jpg"
								alt="">

							<!-- SVG Shapes -->
							<figure class="max-w-15rem w-100 position-absolute bottom-0 left-0 z-index-n1">
								<div class="mb-n7 ml-n7">
									<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/svg/components/dots-5.svg" alt="">
								</div>
							</figure>
							<!-- End SVG Shapes -->
						</div>
					</div>
				</div>

				<div class="col-lg-9">
					<!-- Card Body -->
					<div class="card-body h-100 rounded p-0 p-md-4">
						<!-- SVG Quote -->
						<figure class="mb-3">
							<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="36" height="36" viewBox="0 0 8 8">
								<path fill="#377dff" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
									C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
									c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
									C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z" />
							</svg>
						</figure>
						<!-- End SVG Quote -->

						<div class="row">
							<div class="col-lg-8 mb-3 mb-lg-0">
								<div class="pr-lg-5">
									<blockquote class="h3 font-weight-normal mb-4">Dịch Covid-19 lây lan là do tiếp xúc. Công nghệ số
										là không tiếp xúc. Vì vậy, cơ hội lớn nhất lúc này là đẩy nhanh chuyển đổi số, là tạo ra các ứng
										dụng công nghệ số, là đưa mọi hoạt động sản xuất kinh doanh lên môi trường số. </blockquote>
									<div class="media">
										<div class="avatar avatar-xs avatar-circle d-lg-none mr-2">
											<img class="avatar-img" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar-bo-truong.jpg" alt="">
										</div>
										<div class="media-body">
											<span class="text-dark font-weight-bold">Nguyễn Mạnh Hùng</span>
											<span class="font-size-1">&mdash; Bộ trưởng Thông tin và Truyền thông</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 column-divider-lg">
								<hr class="d-lg-none">
								<div class="pl-lg-5">
									<span class="h1 text-primary">200+</span>
									<p class="font-size-1">Công cụ dưới đây sẽ giúp doanh nghiệp của bạn chuyển đổi số
										một cách nhanh chóng và hiệu quả nhất.</p>
								</div>
							</div>
						</div>
					</div>
					<!-- End Card Body -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonials Section -->
<section id="danhsach" class="container space-top-1 space-top-lg-2 space-bottom-1 space-bottom-lg-2">
	<!-- Hero -->
	<div class="w-md-75 text-center mx-md-auto px-xl-3 mb-5 mb-md-9">
		<h2 class="h1">Ưu đãi nổi bật</h2>
		<p>Dưới đây là các ưu đãi của các doanh nghiệp tham gia chương trình. Sau khi điền đầy đủ thông tin bạn sẽ nhận được thông tin chi tiết về ưu đãi và cách thức nhận ưu đãi của từng doanh nghiệp.</p>
		<div class="text-center">
			<a href="<?php echo home_url() . '/uu-dai/'; ?>" class="btn btn-soft-primary btn-xs btn-pill m-1">
				Tất cả
			</a>
			<?php
			$terms = get_terms( 'deals-type', array(
				'hide_empty' => false,
			) );
			if($terms):
				foreach($terms as $term):
			?>
				<a href="<?php echo esc_url( get_term_link($term->slug, 'deals-type') ); ?>" class="btn btn-soft-primary btn-xs btn-pill m-1">
					<?php echo $term->name;?>
				</a>
			<?php
				endforeach;
			endif;
			?>
		</div>
	</div>
	<?php
	$args = array(
		'post_type' => 'deals',
		'post_status' => 'publish',
		'posts_per_page' => 24,
		'orderby' => 'rand'
	);
	$the_query = new WP_Query($args);
	?>
	<?php if ( $the_query->have_posts() ) : ?>
		<div class="row">
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<div class="col-md-6 col-xl-4 mb-3 mb-md-5">
					<?php get_template_part( 'template-parts/content-deal-card' ); ?>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	<!-- End Hero -->
	<div class="row justify-content-center">
		<div class="col-md-4">
			<a href="<?php echo home_url() . '/uu-dai/'; ?>" class="btn btn-primary btn-block">
				Xem tất cả ưu đãi
			</a>
		</div>
	</div>
</section>
<section id="cacbaibao" class="space-1 border-top text-center">
	<div class="container">
		<h2 class="h6 mb-4 text-muted text-uppercase">Báo chí nói về chúng tôi</h2>
		<div class="text-center">
			<a class="mx-2" target="_blank" href="https://vietcetera.com/vietnam-remote-workforce-lam-viec-tu-xa-day-lui-corona/">
				<img style="height: 30px; width: auto" src="<?php echo get_template_directory_uri(); ?>/assets/img/viecetera.png" alt="">
			</a>
			<a class="mx-2" target="_blank" href="https://ictnews.vietnamnet.vn/cntt/chuyen-doi-so/bo-tt-tt-ra-mat-loat-giai-phap-ho-tro-doanh-nghiep-mua-covid-19-196430.ict">
				<img style="height: 40px; width: auto" src="<?php echo get_template_directory_uri(); ?>/assets/img/ictnews.png" alt="">
			</a>
			<a class="mx-2" target="_blank" href="https://www.youtube.com/watch?v=Cp6yk8IVgGk&amp;feature=youtu.be&amp;t=1209">
				<img style="height: 60px; width: auto" src="<?php echo get_template_directory_uri(); ?>/assets/img/hanoi.png" alt="">
			</a>
			<a class="mx-2" target="_blank" href="https://cafebiz.vn/covid-19-don-nhieu-doanh-nghiep-vao-duong-cung-de-tien-thang-den-chuyen-doi-so-va-chuan-hoa-quy-trinh-nhung-thu-dang-ra-mat-vai-nam-gio-day-thay-doi-chi-trong-vai-thang-20200331000606299.chn">
				<img style="height: 40px; width: auto" src="<?php echo get_template_directory_uri(); ?>/assets/img/cafebiz.png" alt="">
			</a>
		</div>
	</div>
</section>
<?php get_footer();
