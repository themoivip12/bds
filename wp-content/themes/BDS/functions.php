<?php
require get_template_directory() . '/inc/account.php';
require get_template_directory() . '/inc/init.php';
require get_template_directory() . '/inc/nav.php';
require get_template_directory() . '/inc/scripts.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/post-types.php';
