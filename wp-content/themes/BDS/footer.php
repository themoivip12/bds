	<section id="dangky" class="space-2 text-center bg-primary text-white">
		<div class="container">
			<div class="row no-gutters justify-content-center">
				<div class="col-lg-8">
					<h3 class="h2 text-white">Bạn muốn nhận ưu đãi?</h3>
					<p class="text-gray-500">Hãy nhấp vào nút phía bên dưới và điền đầy đủ thông tin để chúng tôi tiến hành kiểm duyệt và gửi thông tin nhận ưu đãi đến cho bạn.</p>
					<button class="btn btn-sm btn-light mt-2" data-toggle="modal" data-target="#dangKyNhan">Đăng ký nhận ưu đãi</button>
				</div>
			</div>
		</div>
	</section>
</main>

<footer class="bg-navy">
	<div class="container">
		<div class="space-1" style="border-bottom: 1px solid rgba(255,255,255,.1)">
			<div class="row">
				<div class="col-lg-4 mb-4 mb-lg-0">
					<div class="rounded p-4 text-white" style="border: 1px solid rgba(255,255,255,.1)">
						<div class="opacity-sm">Facebook</div>
						<a class="text-white font-weight-bold" target="_blank" href="https://facebook.com/groups/vietnamrw">fb.com/groups/vietnamrw</a>
					</div>
				</div>
				<div class="col-lg-4 mb-4 mb-lg-0">
					<div class="rounded p-4 text-white" style="border: 1px solid rgba(255,255,255,.1)">
						<div class="opacity-sm">Website</div>
						<a class="text-white font-weight-bold" target="_blank" href="https://www.remote.vn">www.remote.vn</a>
					</div>
				</div>
				<div class="col-lg-4 mb-4 mb-lg-0">
					<div class="rounded p-4 text-white" style="border: 1px solid rgba(255,255,255,.1)">
						<div class="opacity-sm">Email</div>
						<a class="text-white font-weight-bold" target="_blank" href="mailto:hello@remote.vn">hello@remote.vn</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="space-1 text-center">
		<div class="container">
			<p class="text-white opacity-sm small mb-0">Copyright © 2020 by <a href="https://www.remote.vn" class="text-white">Vietnam Remote Workforce</a>. All Rights Reserved.</p>
			<p class="text-white opacity-sm small mb-0">Developed by <a href="https://www.workevo.vn" class="text-white">Workevo</a> + <a href="https://www.designbold.com" class="text-white">DesignBold</a>.</p>
		</div>
	</div>
</footer>

<div class="modal fade" id="dangKyNhan" tabindex="-1" role="dialog" aria-labelledby="dangKyNhanTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalScrollableTitle">Đăng ký nhận ưu đãi</h5>
				<button type="button" class="btn btn-xs btn-icon btn-soft-secondary" data-dismiss="modal" aria-label="Close">
					<svg aria-hidden="true" width="10" height="10" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
						<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z"/>
					</svg>
				</button>
			</div>
			<div class="modal-body">
			<form class="js-validate" method="post" action="https://embed.workevo.com">
				<h5 class="mb-3">Thông tin công ty</h5>
				<div class="row mx-n2">
					<div class="w-100"></div>

					<div class="col-md-12 px-2 mb-3">
						<!-- Input -->
						<label class="sr-only">Tên công ty</label>
						<input type="hidden" name="fields[0][field]" value="company_name" />
						<div class="js-form-message">
							<div class="input-group">
								<input type="text" class="form-control" name="fields[0][value]" placeholder="Tên công ty"
									aria-label="Tên công ty" required data-msg="Hãy nhập tên công ty">
							</div>
						</div>
						<!-- End Input -->
					</div>

					<div class="col-md-12 px-2 mb-3">
						<!-- Input -->
						<label class="sr-only">Mã số thuế</label>
						<input type="hidden" name="fields[1][field]" value="tax_code_company" />
						<div class="js-form-message">
							<div class="input-group">
								<input type="number" class="form-control" name="fields[1][value]" placeholder="Mã số thuế công ty"
									aria-label="Mã số thuế công ty" required data-msg="Hãy nhập MST công ty">
							</div>
						</div>
						<!-- End Input -->
					</div>
					<div class="col-md-12 px-2 mb-3">
						<!-- Input -->
						<label class="sr-only">Website Công ty</label>
						<input type="hidden" name="fields[2][field]" value="website_company" />
						<div class="js-form-message">
							<div class="input-group">
								<input type="text" class="form-control" name="fields[2][value]" placeholder="Website Công ty"
									aria-label="Website Công ty" required data-msg="Hãy nhập website công ty">
							</div>
						</div>
						<!-- End Input -->
					</div>
					<div class="col-md-12 px-2 mb-3">
						<!-- Input -->
						<label class="sr-only">Số lượng nhân viên</label>
						<input type="hidden" name="fields[3][field]" value="number_of_employees" />
						<div class="js-form-message">
							<div class="input-group">
								<input type="text" class="form-control" name="fields[3][value]" placeholder="Số lượng nhân viên"
									aria-label="Số lượng nhân viên" required data-msg="Hãy nhập số lượng nhân viên">
							</div>
						</div>
						<!-- End Input -->
					</div>
				</div>
				<h5 class="mb-3">Người đại diện</h5>
				<div class="row mx-n2">
					<div class="col-md-6 px-2 mb-3">
						<label class="sr-only">Họ và tên</label>

						<input type="hidden" name="fields[4][field]" value="first_name" />
						<div class="js-form-message">
							<div class="input-group">
								<input type="text" class="form-control" name="fields[4][value]" placeholder="Họ tên"
									aria-label="Họ tên" required data-msg="Hãy nhập tên của bạn">
							</div>
						</div>
						<!-- End Input -->
					</div>

					<div class="col-md-6 px-2 mb-3">
						<!-- Input -->
						<label class="sr-only">Chức vụ</label>

						<input type="hidden" name="fields[5][field]" value="job_title" />
						<div class="js-form-message">
							<div class="input-group">
								<input type="text" class="form-control" name="fields[5][value]" placeholder="Chức vụ"
									aria-label="Chức vụ" required data-msg="Hãy nhập chức danh">
							</div>
						</div>
						<!-- End Input -->
					</div>
					<div class="col-md-12 px-2 mb-3">
						<!-- Input -->
						<label class="sr-only">Email</label>
						<input type="hidden" name="fields[6][field]" value="email" />
						<div class="js-form-message">
							<div class="input-group">
								<input type="email" class="form-control" name="fields[6][value]" placeholder="Email công ty"
									aria-label="Email công ty" required data-msg="Hãy nhập email">
							</div>
						</div>
						<!-- End Input -->
					</div>

					<div class="col-md-12 px-2 mb-3">
						<!-- Input -->
						<label class="sr-only">Số điện thoại</label>
						<input type="hidden" name="fields[7][field]" value="phone" />
						<div class="js-form-message">
							<div class="input-group">
								<input type="number" class="form-control" name="fields[7][value]" placeholder="Số điện thoại"
									aria-label="Số điện thoại" required data-msg="Hãy nhập số điện thoại">
							</div>
						</div>
						<!-- End Input -->
					</div>
				</div>
				<input type="hidden" name="form" value="901b96fe-02c8-45e6-b318-70f0306a872f">
				<input type="hidden" name="workspace" value="6943bdde-e0d7-4995-af70-8d62fcba2ecc">


				<input type="hidden" name="title" value="Gửi thông tin thành công">
				<input type="hidden" name="message" value="Chúng tôi đã nhận được thông tin của bạn và sẽ gửi email cho bạn trong ít phút nữa.">
				<button type="submit" class="btn btn-primary btn-wide transition-3d-hover">Gửi thông tin</button>
			</form>
			</div>
		</div>
	</div>
</div>
<?php wp_footer(); ?>

<!-- JS Global Compulsory -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/hs-header/dist/hs-header.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/hs-go-to/dist/hs-go-to.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/hs-unfold/dist/hs-unfold.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/hs-mega-menu/dist/hs-mega-menu.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/hs-show-animation/dist/hs-show-animation.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/hs-sticky-block/dist/hs-sticky-block.min.js"></script>

<!-- JS Front -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/hs.core.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/hs.validation.js"></script>

<!-- JS Plugins Init. -->
<script>
	$(document).on('ready', function () {
		// initialization of header
		var header = new HSHeader($('#header')).init();

		// initialization of mega menu
		var megaMenu = new HSMegaMenu($('.js-mega-menu'), {
			desktop: {
				position: 'left'
			}
		}).init();

		// initialization of unfold
		var unfold = new HSUnfold('.js-hs-unfold-invoker').init();

		// initialization of show animations
		$('.js-animation-link').each(function () {
			var showAnimation = new HSShowAnimation($(this)).init();
		});

		// initialization of form validation
		$('.js-validate').each(function() {
			$.HSCore.components.HSValidation.init($(this), {
				rules: {
					confirmPassword: {
						equalTo: '#signupPassword'
					}
				}
			});
		});

		// initialization of sticky blocks
		$('.js-sticky-block').each(function () {
			var stickyBlock = new HSStickyBlock($(this)).init();
		});

		// initialization of go to
		$('.js-go-to').each(function () {
			var goTo = new HSGoTo($(this)).init();
		});
	});
</script>
<!-- JS Plugins Init. -->
<script>
    $(document).on('ready', function () {
      // initialization of header
      var header = new HSHeader($('#header')).init();

      // initialization of mega menu
      var megaMenu = new HSMegaMenu($('.js-mega-menu'), {
        desktop: {
          position: 'left'
        }
      }).init();

      // initialization of unfold
      var unfold = new HSUnfold('.js-hs-unfold-invoker').init();

      // initialization of show animations
      $('.js-animation-link').each(function () {
        var showAnimation = new HSShowAnimation($(this)).init();
      });

      // initialization of form validation
      $('.js-validate').each(function() {
        $.HSCore.components.HSValidation.init($(this), {
          rules: {
            confirmPassword: {
              equalTo: '#signupPassword'
            }
          }
        });
      });

      // initialization of sticky blocks
      $('.js-sticky-block').each(function () {
        var stickyBlock = new HSStickyBlock($(this)).init();
      });

      // initialization of go to
      $('.js-go-to').each(function () {
        var goTo = new HSGoTo($(this)).init();
      });
    });
  </script>

  <!-- IE Support -->
  <script>
    if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/polifills.js"><\/script>');
  </script>
</body>
</html>
