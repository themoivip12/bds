<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php $metas = fw_get_db_post_option( get_the_ID() ); ?>
	<!-- Hero Section -->
	<div class="bg-navy">
		<div class="container space-2 space-bottom-lg-3">
		</div>
	</div>
	<!-- End Hero Section -->
	<div class="container space-2 space-lg-0">
		<div class="row">
			<div class="col-lg-4 mt-n11 space-bottom-2">
				<!-- Sidebar Content -->
				<div class="card bg-white">
					<div class="card-header text-center py-5">
						<div class="max-w-27rem mx-auto">
							<?php if(isset($metas['logo']['url']) && $metas['logo']['url']):?>
								<img class="img-fluid" src="<?php echo $metas['logo']['url'];?>" alt="<?php the_title(); ?>" />
							<?php else : ?>
								<img class="img-fluid" src="https://placehold.it/480x220" alt="<?php the_title(); ?>" />
							<?php endif; ?>
						</div>
					</div>

					<div class="card-body">
						<?php if(isset($metas['description']) && $metas['description']):?>
						<div class="pb-2 mb-4">
							<?php echo $metas['description'];?>
						</div>
						<?php endif; ?>
						<div class="border-bottom pb-2 mb-4">
							<dl class="row font-size-1">
								<dt class="col-sm-4 text-dark">Giải pháp</dt>
								<dd class="col-sm-8 text-body"><?php the_terms( get_the_ID(), 'deals-type', '', ', ' ); ?></dd>
							</dl>
							<!-- <dl class="row font-size-1">
								<dt class="col-sm-4 text-dark">Email</dt>
								<dd class="col-sm-8 text-body"><?php echo (isset($metas['email']) && $metas['email']) ? $metas['email'] : 'Đang cập nhật';?></dd>
							</dl>
							<dl class="row font-size-1">
								<dt class="col-sm-4 text-dark">Hotline</dt>
								<dd class="col-sm-8 text-body"><?php echo (isset($metas['phone']) && $metas['phone']) ? $metas['phone'] : 'Đang cập nhật';?></dd>
							</dl> -->
							<dl class="row font-size-1">
								<dt class="col-sm-4 text-dark">Website</dt>
								<dd class="col-sm-8 text-body"><?php echo (isset($metas['url']) && $metas['url'] ) ? '<a class="d-block text-truncate" href="' . $metas['url'] . '">' . $metas['url'] . '</a>' : 'Đang cập nhật';?></dd>
							</dl>
						</div>
						<div class="font-size-1 text-primary d-flex align-items-center"><span class="badge badge-primary mr-2">Ưu đãi</span> <?php echo (isset($metas['uudai_tomtat']) && $metas['uudai_tomtat']) ? $metas['uudai_tomtat'] : 'Đang cập nhật';?></div>
					</div>
				</div>
				<!-- End Sidebar Content -->
			</div>

			<div class="col-lg-8 space-lg-2">
				<div class="pl-lg-4">
					<h1 class="h3 mb-5 mt-lg-n5"><?php the_title(); ?> hân hạnh đồng hành cùng #VRW hỗ trợ các doanh nghiệp chuyển đổi số vượt đại dịch Covid-19</h1>
					
				</div>
			</div>
		</div>
	</div>

<section id="danhsach" class="border-top space-2">
	<div class="container">
		<?php
		$args = array(
			'post_type' => 'deals',
			'post_status' => 'publish',
			'orderby' => 'rand',
			'posts_per_page' => 24
		);
		$the_query = new WP_Query($args);
		?>
		<?php if ( $the_query->have_posts() ) : ?>
			<div class="row">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="col-md-6 col-xl-4 mb-3 mb-md-5">
						<?php get_template_part( 'template-parts/content-deal-card' ); ?>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
		<!-- End Hero -->
		<div class="row justify-content-center">
			<div class="col-md-4">
				<a href="<?php echo home_url() . '/uu-dai/'; ?>" class="btn btn-primary btn-block">
					Xem tất cả ưu đãi
				</a>
			</div>
		</div>
	</div>
</section>
<?php endwhile; ?>
<?php get_footer(); ?>
